class BudgetAboutToExpireWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily.hour_of_day(1) }

  def perform
    Demand.with_budgets_to_be_expired_at(Date.today) do |demand|
      notification                   = demand.user.notifications.build
      notification.sender            = demand.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("BUDGET_EXPIRED")
      notification.notifiable        = demand
      notification.save

      NotificationMailer.budget_expired(demand).deliver

      demand.budgets_to_be_expired_at(Date.today).each { |budget|
        budget.expire }
    end
  end

end