class ExpiredDemandWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily }

  def perform
    Demand.to_be_expired_at(Date.today).each do |demand|
      demand.expire

      demand.transporters_with_budgets.each do |transporter|
        notification                   = transporter.user.notifications.build
        notification.sender            = demand.user
        notification.status            = Status::Notification::UNREAD
        notification.notification_type = NotificationType.find("DEMAND_EXPIRED")
        notification.notifiable        = demand
        notification.save
      end

      NotificationMailer.demand_expired(demand).deliver
    end
  end

end