class NoCreditWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily.hour_of_day(11) }

  def perform
    Transporter.with_budgets_to_be_sent.each do |transporter|
      if transporter.last_no_credit_notification_date.nil? ||
        transporter.last_no_credit_notification_date == 3.days
        notification                   = transporter.user.notifications.build
        notification.sender            = transporter.user
        notification.status            = Status::Notification::UNREAD
        notification.notification_type = NotificationType.find("NO_CREDIT")
        notification.notifiable        = transporter
        notification.save

        NotificationMailer.no_credit(transporter.user).deliver
      end
    end
  end

end