class EndOfReceptionPeriodWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily }

  def perform
    Demand.which_reception_period_finishes_at(Date.today).each do |demand|
      demand.finish_budget_reception_period

      notification                   = demand.user.notifications.build
      notification.sender            = demand.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("END_OF_RECEPTION_PERIOD")
      notification.notifiable        = demand
      notification.save

      NotificationMailer.end_of_reception_period(demand).deliver
    end
  end

end