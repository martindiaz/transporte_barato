class DemandAboutToExpireWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily.hour_of_day(12) }

  def perform
    Demand.to_be_expired_at(Date.today + 3.days).each do |demand|
      notification                   = demand.user.notifications.build
      notification.sender            = demand.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("DEMAND_ABOUT_TO_EXPIRE")
      notification.notifiable        = demand
      notification.save

      NotificationMailer.demand_about_to_expire(demand).deliver
    end
  end

end