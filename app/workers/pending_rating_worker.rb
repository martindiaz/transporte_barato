class PendingRatingWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily.hour_of_day(20) }

  def perform
    Demand.to_be_delivered_at(Date.today).each do |demand|
      budget = demand.accepted_budget

      notification                   = demand.user.notifications.build
      notification.sender            = budget.transporter.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("REMEMBER_TO_VALUATE_TRANSPORTER")
      notification.notifiable        = budget.transporter
      notification.save

      NotificationMailer.remember_to_valuate_transporter(demand.client, budget.transporter).deliver

      valoration_notification                   = budget.transporter.user.notifications.build
      valoration_notification.sender            = budget.demand.user
      valoration_notification.status            = Status::Notification::UNREAD
      valoration_notification.notification_type = NotificationType.find("REMEMBER_TO_VALUATE_CLIENT")
      valoration_notification.notifiable        = budget.demand.user
      valoration_notification.save

      NotificationMailer.remember_to_valuate_client(budget.transporter, demand.user).deliver
    end
  end
  
end