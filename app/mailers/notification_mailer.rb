class NotificationMailer < ActionMailer::Base
  layout "notification"
  default from: "info@ahorraenvios.com"

  def new_budget(demand, budget)
    @client = demand.client
    @model  = budget
    @user = @client.user

    if @client.receive_email_alerts
      mail(to: @client.email, subject: "Tienes un nuevo presupuesto: #{demand.description}")
    end
  end

  def new_demand(demand)
    @demand = demand

    users = Transporter.includes(:user).where(
      users: {receive_email_alerts: true} ).who_match_location_and_category(
        demand.category, demand.origin, demand.destiny).reject { |transporter|
          demand.transporter_has_sent_budget?(transporter) }.map(&:user)

    unless users.blank?
      users.each do |user|
        @user = user
        mail(to: user.email, subject: "Nuevo envío para presupuestar: #{demand.description}")
      end
    end
  end

  def updated_demand(demand)
    @model = demand

    users = demand.transporters_with_budgets_to_be_confirmed.select { |transporter|
      transporter.receive_email_alerts }.map(&:user)

    unless users.blank?
      users.each do |user|
        @user = user
        mail(to: user.email, :subject =>  "Envío modificado: #{demand.description}")
      end
    end
  end

  def demand_cancelled(demand)
    @model = demand
    user = demand.user

    users = demand.transporters_with_budgets.select { |transporter|
      transporter.receive_email_alerts }.map(&:user)

    unless users.blank?
      users.each do |user|
        @user = user
        mail(to: user.email, subject: "Envío cancelado: #{demand.description}")
      end
    end
  end

  def demand_expired(demand)
    @model = demand

    users = demand.transporters_with_budgets.select { |transporter|
      transporter.receive_email_alerts }.map(&:user)

    unless users.blank?
      users.each do |user|
        @user = user
        mail(to: user.email, subject: "Envío caducado: #{demand.description}")
      end
    end
  end

  def budget_changed(budget)
    @user = budget.demand.user

    if @user.receive_email_alerts
      @budget = budget
      email = @user.email
      mail(to: email, subject: "Han modificado un presupuesto: #{budget.demand.description}")
    end
  end

  def budget_accepted(model)
    if model.respond_to? :transporter
      @user = model.transporter.user
      subject = "Nuevo presupuesto aceptado!"
    else
      @user = model.client.user
      subject = "Recuerda valorar"
    end

    if @user.receive_email_alerts
      @model = model
      email = @user.email
      mail(to: email, subject: subject)
    end
  end

  def budget_cancelled(budget)
    @user = budget.demand.user
    if @user.receive_email_alerts
      @model = budget
      mail(to: @user.email, subject: "Han cancelado un presupuesto")
    end
  end

  def budget_confirmed(budget)
    @user = budget.demand.user
    if @user.receive_email_alerts
      @model = budget
      mail(to: @user.email, subject: "Han confirmado un presupuesto")
    end
  end

  def budget_discarded(budget)
    @user = budget.user
    if @user.receive_email_alerts
      @model = budget
      mail(to: @user.email, subject: "Han descartado tu presupuesto")
    end
  end

  def budget_expired(demand)
    @user = demand.user
    if @user.receive_email_alerts
      @model = demand
      mail(to: @user.email, subject: "Tienes presupuestos que caducan hoy")
    end
  end

  def another_budget_was_accepted(demand)
    @model = demand

    users = demand.transporters_rejected.select { |transporter|
      transporter.receive_email_alerts &&
        transporter != demand.accepted_budget_transporter }.map(&:user)

    unless users.blank?
      users.each do |user|
        @user = user
        mail(to: user.email, subject: "Aceptaron el presupuesto de otro transportista")
      end
    end
  end

  def remember_to_valuate_client(transporter, client)
    if transporter.receive_email_alerts
      @client = client
      email = transporter.email
      @user = transporter.user
      mail(to: email, subject: "Recuerda valorar al usuario")
    end
  end

  def remember_to_valuate_transporter(client, transporter)
    if client.receive_email_alerts
      @transporter = transporter
      email = client.email
      @user = client.user
      mail(to: email, subject: "Recuerda valorar al transportista")
    end
  end

  def new_message_received(comment, user)
    if user.receive_email_alerts
      @comment = comment
      @user = user
      email = user.email
      mail(to: email, subject: "Has recibido un nuevo mensaje")
    end
  end

  def new_rating_received(rating)
    @user = rating.receiver

    if @user.receive_email_alerts
      @rating = rating
      email = @user.email
      mail(to: email, subject: "Has recibido una valoracion")
    end
  end

  def confirm_your_budget(budget)
    transporter = budget.transporter
    @user = transporter.user

    if transporter.receive_email_alerts
      @budget = budget
      email = transporter.email
      mail(to: email, subject: "Envío modificado: #{budget.demand_description}")
    end
  end

  def username_changed(user)
    @user = user

    if user.receive_email_alerts
      email = user.email
      mail(to: email, subject: "Actualización de nombre de usuario")
    end
  end

  def password_changed(user)
    @user = user

    if user.receive_email_alerts
      email = user.email
      mail(to: email, subject: "Actualización de contraseña")
    end
  end

  def cheaper_budget(budget, transporter)
    @user = transporter.user
    @budget = budget

    if @user.receive_email_alerts
      mail(to: @user.email, subject: "Han presupuestado más barato que tu")
    end
  end

  def demand_about_to_expire(demand)
    @model = demand
    @user  = demand.user

    if @user.receive_email_alerts
      mail(to: user.email, subject: "Próximo vencimiento del envío #{demand.description}")
    end
  end

  def end_of_reception_period(demand)
    @model = demand
    @user  = demand.user

    if @user.receive_email_alerts
      mail(to: user.email, subject: "Ya puedes elegir la mejor oferta para el envío #{demand.description}")
    end
  end

  def no_credit(user)
    @model, @user = user

    if @user.receive_email_alerts
      mail(to: @user.email, subject: "Tienes presupuestos pendientes de entrega")
    end
  end

  def account_activation(email)
    @user = User.where(email: email).first
    mail(to: email, subject: "Activa tu cuenta en AhorraEnvios")
  end
end
