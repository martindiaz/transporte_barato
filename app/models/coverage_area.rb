class CoverageArea < ActiveRecord::Base
  belongs_to :transporter

  has_and_belongs_to_many :categories
  has_many :addresses, as: :addressable

  has_and_belongs_to_many :polygons
  has_and_belongs_to_many :kmls

  attr_accessible :transporter_id, :category_ids, :kml_ids,  :description, :addresses_attributes,
                  :accepts_notifications_from_state_to_area, :accepts_notifications_from_city_to_area,
                  :accepts_notifications_from_area_to_city,  :accepts_notifications_from_area_to_state
  accepts_nested_attributes_for :addresses

  validates :categories, :addresses, :description, presence: true

  def has_category?(category)
    categories.include?(category)
  end

  def contains_location?(origin, destiny)
    transporter_address = self.transporter.address
    result = false

    if transporter_address
      if accepts_notifications_from_state_to_area
        result ||= transporter_address.contains_state?(origin) && self.contains?(destiny)
      end

      if accepts_notifications_from_city_to_area
        result ||=  (transporter_address.contains_city?(origin) || transporter_address.contains_administrative_area?(origin)) && self.contains?(destiny)
      end

      if accepts_notifications_from_area_to_state
        result ||= self.contains?(origin) && transporter_address.contains_state?(destiny)
      end

      if accepts_notifications_from_area_to_city
        result ||= self.contains?(origin) && (transporter_address.contains_city?(destiny) || transporter_address.contains_administrative_area?(destiny))
      end
    end

    result
  end

  def contains?(location)
    self.addresses.each do |address|
      if address.administrative_area
        if address.contains_state?(location) && address.contains_administrative_area?(location)
          return true
        else
          return false
        end
      end

      if address.city
        if address.contains_state?(location) && address.contains_city?(location)
          return true
        else
          return false
        end
      end

      if address.state
        if address.contains_state?(location)
          return true
        else
          return false
        end
      end
    end
    false
  end

end
