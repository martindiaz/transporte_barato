class User < ActiveRecord::Base
  rolify

  belongs_to :role, polymorphic: true, dependent: :destroy

  has_many :comments, :as => :commentable
  has_many :notifications
  has_many :credits

  has_one :billing_address

  devise :database_authenticatable, :registerable, :recoverable,
         :rememberable, :trackable, :validatable, :omniauthable,
         :confirmable, :authentication_keys => [:login]

  attr_accessible :login, :username, :role_id, :role_type, :name, :surname, :email, :password,
                  :password_confirmation, :remember_me, :provider, :uid, :phone_number,
                  :accept_use_conditions, :receive_email_alerts, :must_change_password

  validates :accept_use_conditions, presence: true, on: :create
  validates :username, :email, uniqueness: true, presence: true
  validates :name, :surname, presence: true, on: :update, if: :has_signed_in?

  has_many :received_ratings, class_name: 'Rating', foreign_key: "receiver_id", conditions: "points is not null"
  has_many :given_ratings, class_name: 'Rating', foreign_key: "owner_id", conditions: "points is not null"

  # Virtual attribute for authenticating by either username or email
  # This is in addition to a real persisted field like 'username'
  attr_accessor :login

  def has_signed_in?
    self.sign_in_count > 0
  end

  def pending_rating
    Rating.where(:owner_id => self).pending
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup

    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { value: login.downcase }]).first
    else
      where(conditions).first
    end
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first

    unless user
      user = User.new(provider:auth.provider,
                      uid:auth.uid,
                      email:auth.info.email,
                      name: auth.info.first_name,
                      surname: auth.info.last_name,
                      username: auth.info.nickname)
    end

    user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def client?
    self.role.kind_of? Client
  end

  def transporter?
    self.role.kind_of? Transporter
  end

  def complete?
    !name.blank?     &&
    !surname.blank?  &&
    !username.blank?
  end

  def full_name
    "#{self.name} #{self.surname}"
  end

  def total_credits
    self.credits.inject(0){|total, credit_history| total + credit_history.amount }
  end

  # Number of stars to show
  def total_rating
    return 0 if self.received_ratings.count == 0

    case (self.received_ratings.sum(:points) / self.received_ratings.count.to_f).to_f.round(2)
    when -1..-0.67
      0
    when -0.66..-0.34
      1
    when -0.33..-0.01
      2
    when 0..0.33
      3
    when 0.34..0.66
      4
    when 0.67..1
      5
    end
  end

  {
    positive: 1,
    neutral:  0,
    negative: -1
  }.each do |key, value|
    define_method("#{key}_ratings".to_sym) do
      received_ratings.select { |rating| rating.points == value  }
    end
  end

  def has_credit?
    self.total_credits > 0
  end

  def must_change_password?
    self.must_change_password
  end

  def total_comments
    self.comments.size
  end

  def unread_notifications
    self.notifications.unread
  end

end
