class Group < ActiveRecord::Base
  attr_accessible :description, :order, :icon_ids

  validates :description, presence: true

  has_many :icons, inverse_of: :group
end
