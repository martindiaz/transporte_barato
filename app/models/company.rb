class Company < ActiveRecord::Base
  belongs_to :transporter
  has_one    :image,   as: :imageable,   dependent: :destroy, class_name: Image
  has_one    :address, as: :addressable, dependent: :destroy, class_name: Address
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :payment_methods
  has_and_belongs_to_many :payment_conditions
  has_and_belongs_to_many :extra_services

  attr_accessible :company_type, :name, :number_of_drivers, :number_of_employees,
                  :transport_license_number, :year_of_creation, :image_attributes,
                  :phone_number, :website, :vat_registered, :address_attributes,
                  :category_ids, :license_and_insurance, :additional_information,
                  :payment_method_ids, :payment_condition_ids, :extra_service_ids

  accepts_nested_attributes_for :image

  delegate :state, :city, :country, to: :address

  validates :name, :address, :phone_number, presence: true

  before_validation :smart_add_url_protocol

  def complete?
    !address.nil?        &&
    !name.blank?         &&
    !phone_number.blank?
  end

  def has_image?
    !image.image.to_s.empty? if image
  end

  protected

  def smart_add_url_protocol
    return if self.website.blank?

    unless self.website[/^http:\/\//] || self.website[/^https:\/\//]
      self.website = 'http://' + self.website
    end
  end
end
