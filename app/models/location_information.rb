class LocationInformation < ActiveRecord::Base
  has_many :demands

  attr_accessible :delivery_date_from, :delivery_date_to, :has_place_to_park, :date_range

  validates :delivery_date_from, presence: true
end
