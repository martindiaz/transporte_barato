class BillingAddress < ActiveRecord::Base
  attr_accessible :city, :country, :first_name, :last_name, :state, :street_line, :zip

  belongs_to :user

  validates :city, :country, :first_name, :last_name, :state, :street_line, :zip, :user_id, presence: true
end
