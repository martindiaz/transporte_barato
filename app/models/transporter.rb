class Transporter < ActiveRecord::Base
  resourcify

  has_one  :user, as: :role, dependent: :destroy, inverse_of: :role
  has_one  :company,  dependent: :destroy
  has_one  :home_banner, dependent: :destroy
  has_many :vehicles, dependent: :destroy
  has_many :budgets,  dependent: :destroy
  has_many :coverage_areas, dependent: :destroy

  has_and_belongs_to_many :favorites, class_name: 'Demand', uniq: true

  attr_accessible :user_attributes
  accepts_nested_attributes_for :user, allow_destroy: true

  delegate :email,
           :add_role,
           :name,
           :surname,
           :receive_email_alerts,
           :username,
           :comments,
           :received_ratings,
           :total_rating,
           :positive_ratings,
           :neutral_ratings,
           :negative_ratings,
           :credits,
           :has_credit?,
           :total_comments,
           to: :user

  delegate :active, :accepted, to: :budgets, prefix: true
  delegate :address, :categories, to: :company
  delegate :name, to: :company, prefix: true

  def self.top_transporters
    all(limit: 10)
  end

  def self.who_match_location_and_category(category, origin, destiny)
    all.map do |transporter|
      if transporter.match_location_and_category?(category, origin, destiny)
        transporter
      end
    end.compact
  end

  def self.with_budgets_to_be_sent
    all.select do |transporter|
      transporter.budgets_to_be_sent.any?
    end
  end

  def has_a_complete_profile?
    user.complete? && company && company.complete?
  end

  def budgeted_demands(status)
    # TODO make a query for this
    Set.new(budgets.send(status).map { |budget| budget.demand }.sort{|a, b| b.created_at <=> a.created_at }).to_a
  end

  def match_location_and_category?(category, origin, destiny)
    match = false
    coverage_areas.each do |coverage_area|
      unless match
        if coverage_area.has_category?(category)
          if coverage_area.contains_location?(origin, destiny)
            match = true
          end
        end
      end
    end
    match
  end

  def accepted_budgets
    self.budgets.accepted
  end

  def set_budgets_to_status_without_credit
    self.budgets.sent.each            { |budget| budget.sent_without_credit }
    self.budgets.updated.each         { |budget| budget.modify(save: true) }
    self.budgets.to_be_confirmed.each { |budget| budget.to_be_confirmed }
    self.budgets.confirmed.each       { |budget| budget.confirm }
  end

  def budgets_to_be_sent
    self.budgets.sent_without_credit_and_not_sent
  end

  def set_budgets_to_sent
    self.budgets_to_be_sent.each { |budget| budget.sent }
  end

  def has_a_cheaper_budget?(demand, budget)
    demand.budgets_by_transporter(self).select { |current_budget|
      current_budget.is_cheaper_than?(budget) }.any?
  end

  def budgets_with_new_client_comments
    self.budgets.select { |budget|
      budget.has_new_client_comments? }
  end

  def last_no_credit_notification_date
    if last = user.notifications.no_credit.last
      last.created_at.to_date
    end
  end

  def self.csv(options={})
    CSV.generate(options) do |csv|
      csv << ['Fecha', 'Nombre', 'Apellido', 'Razon social', 'Email', 'Telefono', 'Cant. de presupuestos']

      all.each do |transporter|
        csv << [ (I18n.l transporter.created_at.to_date),
                transporter.name,
                transporter.surname,
                (transporter.company.blank? ? '' : transporter.company.name),
                transporter.user.email,
                (transporter.company.blank? ? '' : transporter.company.phone_number),
                transporter.budgets.count ]
      end
    end
  end
end
