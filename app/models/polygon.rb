class Polygon < ActiveRecord::Base
  has_many :points, dependent: :destroy

  attr_accessible :description, :points

  accepts_nested_attributes_for :points

  validates :points, presence: true

  def calculate_area
    members = []

    points.each_with_index do |point, index|
      next_point = (points[index + 1]) ? points[index + 1] : points.first

      actual_x, actual_y = point.latitude, point.longitude
      next_x, next_y     = next_point.latitude, next_point.longitude

      members << (actual_x * next_y) - (actual_y * next_x)
    end

    ((members.inject(:+)) / 2.0).abs
  end

end
