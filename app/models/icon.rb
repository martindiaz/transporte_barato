class Icon < ActiveRecord::Base
  attr_accessible :depth, :description, :group_id, :high, :image_attributes,
                  :order, :unit, :volume, :weight, :width,
                  :template_ids

  belongs_to :group, inverse_of: :icons
  has_one    :image, as: :imageable, dependent: :destroy, class_name: Image
  has_many   :icons_templates
  has_many   :templates, :through => :icons_templates, inverse_of: :icons

  validates :description, :group, :presence => true

  accepts_nested_attributes_for :image

  def to_s
    self.description
  end

end
