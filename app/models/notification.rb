class Notification < ActiveRecord::Base
  belongs_to :user
  belongs_to :sender, class_name: "User", foreign_key: "sender_id"
  belongs_to :notifiable, polymorphic: true
  belongs_to :notification_type

  attr_accessible :status, :user_id, :sender_attributes, :prominent, :notification_type_id

  delegate :description, :partial, to: :notification_type

  {
    read:    Status::Notification::READ,
    unread:  Status::Notification::UNREAD,
    deleted: Status::Notification::DELETED
  }.each do |key, value|
    define_singleton_method(key) { where(status: value).order("created_at DESC") }
    define_method("#{key}?".to_sym) { status == value }
    define_method(key) do
      self.status = value
      self.save 
    end
  end

  def prominent?
    self.prominent == true
  end

  def self.prominent
    where prominent: true
  end

  def self.no_credit
    joins("LEFT OUTER JOIN notification_types ON notification_types.name = 'NO_CREDIT'")
  end

end