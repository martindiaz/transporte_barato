class Budget < ActiveRecord::Base
  include Status::Budget

  attr_accessible :demand_id, :price, :transporter_id, :status, :expiration_date,
    :accepts_notification_for_cheaper_budgets

  belongs_to :transporter
  belongs_to :demand
  has_many   :comments,      :as => :commentable
  has_many   :notifications, :as => :notifiable
  has_many   :credits

  validates :price, presence: true, numericality: true, numericality: { greater_than_or_equal_to: 0 }
  validates :expiration_date, presence: true

  delegate :user, to: :transporter
  delegate :has_credit?, to: :transporter, prefix: true
  delegate :description, to: :demand, prefix: true

  def set_status
    if transporter.has_credit?
      self.status = SENT
    else
      self.status = NOT_SENT
    end
  end

  def accept
    self.status = ACCEPTED
    save(validate: false)
  end

  def discard
    self.status = DISCARDED
    save(validate: false)
  end

  def receive
    self.status = RECEIVED
    save(validate: false)
  end

  def modify(params = {})
    if transporter.has_credit?
      self.status = UPDATED
    else
      self.status = UPDATED_WITHOUT_CREDIT
    end

    save(validate: false) if params[:save]
  end

  def to_be_confirmed
    if transporter.has_credit?
      self.status = TO_BE_CONFIRMED
    else
      self.status = TO_BE_CONFIRMED_WITHOUT_CREDIT
    end

    save(validate: false)
  end

  def confirm
    if transporter.has_credit? & self.to_be_confirmed?
      self.status = CONFIRMED
    else
      self.status = CONFIRMED_WITHOUT_CREDIT
    end

    save(validate: false)
  end

  def cancel
    if self.not_sent?
      self.status = NOT_SENT_AND_CANCELLED
    else
      self.status = CANCELLED
    end

    save(validate: false)
  end

  def another_was_accepted
    self.status = ANOTHER_WAS_ACCEPTED
    save(validate: false)
  end

  def demand_was_cancelled
    self.status = CANCELLED_DEMAND
    save(validate: false)
  end

  def sent_without_credit
    self.status = SENT_WITHOUT_CREDIT
    save(validate: false)
  end

  def to_be_confirmed_without_credit
    self.status = TO_BE_CONFIRMED_WITHOUT_CREDIT
    save(validate: false)
  end

  def not_sent_to_be_confirmed
    self.status = NOT_SENT_TO_BE_CONFIRMED
    save(validate: false)
  end

  def expired_demand
    self.status = EXPIRED_DEMAND
    save(validate: false)
  end

  def expire
    self.status = EXPIRED
    save(validate: false)
  end

  def not_sent_end_of_period
    self.status = NOT_SENT_END_OF_PERIOD
    save(validate: false)
  end

  def sent
    self.status = SENT
    save(validate: false)
  end

  def price
    self.read_attribute(:price).to_i
  end

  def total_credits
    self.credits.inject(0) { |sum, c| c.amount + sum }.abs
  end

  def new_comments(the_user)
    if the_user.transporter?
      self.new_client_comments
    else
      self.new_transporter_comments
    end
  end

  def new_client_comments
    self.comments.select { |comment| comment.new_by_client? }
  end

  def new_transporter_comments
    self.comments.select { |comment| comment.new_by_transporter? }
  end

  def to_be_confirmed?
    self.status == TO_BE_CONFIRMED
  end

  def not_sent?
    self.status == NOT_SENT || self.status == NOT_SENT_TO_BE_CONFIRMED
  end

  def sent?
    self.status == SENT
  end

  def updated?
    self.status == UPDATED
  end

  def confirmed?
    self.status == CONFIRMED
  end

  def new?
    self.sent? || self.not_sent?
  end

  def cancelled?
    self.status == CANCELLED
  end

  def expired?
    self.status == EXPIRED
  end

  def accepted?
    self.status == ACCEPTED
  end

  def was_discarded?
    self.status == DISCARDED
  end

  def another_was_accepted?
    self.status == ANOTHER_WAS_ACCEPTED
  end

  def has_new_comments?(the_user)
    if the_user.transporter?
      self.has_new_client_comments?
    else
      self.has_new_transporter_comments?
    end
  end

  def has_new_client_comments?
    self.new_client_comments.any?
  end

  def has_new_transporter_comments?
    self.new_transporter_comments.any?
  end

  def is_cheaper_than?(other)
    self.price < other.price
  end

  def accepts_notification_for_cheaper_budgets?
    self.accepts_notification_for_cheaper_budgets
  end

  def self.active_statuses
    [
      SENT, RECEIVED, UPDATED, TO_BE_CONFIRMED,
      CONFIRMED, NOT_SENT, SENT_WITHOUT_CREDIT,
      UPDATED_WITHOUT_CREDIT, TO_BE_CONFIRMED_WITHOUT_CREDIT,
      CONFIRMED_WITHOUT_CREDIT, NOT_SENT_TO_BE_CONFIRMED,
      CANCELLED, DISCARDED, EXPIRED
    ]
  end

  def self.accepted_statuses
    [ACCEPTED]
  end

  def self.discarded_statuses
    [
      DISCARDED, ANOTHER_WAS_ACCEPTED,
      EXPIRED, CANCELLED_DEMAND,
      EXPIRED_DEMAND, NOT_SENT_AND_CANCELLED,
      NOT_SENT_END_OF_PERIOD, CANCELLED
    ]
  end

  def self.sent
    where status: SENT
  end

  def self.not_sent
    where status: [NOT_SENT, NOT_SENT_TO_BE_CONFIRMED]
  end

  def self.sent_without_credit
    where status: SENT_WITHOUT_CREDIT
  end

  def self.sent_without_credit_and_not_sent
    where status: [SENT_WITHOUT_CREDIT, NOT_SENT]
  end

  def self.updated
    where status: UPDATED
  end

  def self.confirmed
    where status: CONFIRMED
  end

  def self.to_be_confirmed
    where status: TO_BE_CONFIRMED
  end

  def self.to_be_expired_at(date)
    active.where(expiration_date: date)
  end

  {
    active:    active_statuses,
    accepted:  accepted_statuses,
    discarded: discarded_statuses
  }.each do |key, value|
    define_method("#{key}?".to_sym) { value.include?(status)  }
    define_singleton_method(key)    { where status: value     }
  end

  def self.csv(options={})
    CSV.generate(options) do |csv|
      csv << ['Fecha', 'Importe', 'Estado', 'Transportista', 'Usuario']

      all.each do |budget|
        csv << [ (I18n.l budget.created_at.to_date),
                  budget.price,
                  budget.status,
                  budget.user.username,
                  budget.demand.user.username ]
      end
    end
  end

end
