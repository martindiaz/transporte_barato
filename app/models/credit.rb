class Credit < ActiveRecord::Base
  attr_accessible :amount, :budget_id, :movement_type_id, :user_id, :payment_type

  belongs_to :movement_type
  belongs_to :user
  belongs_to :budget

  validates :amount, :user_id, presence: true

  validates :amount, numericality: true
  validates :budget_id, :uniqueness => { :scope => :movement_type_id}, :if => :is_a_budget?

  after_save :check_total_credit

  delegate :has_credit?, :role, to: :user, prefix: true

  def is_a_budget?
    !self.budget.nil?
  end

  def check_total_credit
    if self.user_has_credit?
      self.user.role.set_budgets_to_sent
    else
      self.user.role.set_budgets_to_status_without_credit
    end
  end

  def buy_movement?
    self.movement_type.name == "BUY_CREDITS"
  end
end
