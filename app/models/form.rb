class Form < ActiveRecord::Base
  attr_accessible :data
  serialize :data, ActiveRecord::Coders::Hstore

  has_and_belongs_to_many :categories

end
