class Point < ActiveRecord::Base
  belongs_to :polygon

  attr_accessible :latitude, :longitude, :polygon_id

  def is_inside_polygon?(polygon, epsilon=2)
    points = polygon.points
    sum    = 0.0

    points.each_with_index do |current_point, index|
      next_point = (points[index + 1]) ? points[index + 1] : points.first
      if cross(current_point, next_point) < 0
        sum -= angle(current_point, next_point)
      else
        sum += angle(current_point, next_point)
      end
    end

    ((sum - 2 * Math::PI).abs < epsilon) || ((sum + 2 * Math::PI).abs < epsilon)
  end

  private

  def cross(current_point, next_point)
    (next_point.latitude - current_point.latitude)   *
    (self.longitude - current_point.longitude)       *
    (next_point.longitude - current_point.longitude) *
    (self.latitude - current_point.latitude)
  end

  def angle(current_point, next_point)
    ux = current_point.latitude  - self.latitude
    uy = current_point.longitude - self.longitude

    vx = next_point.latitude  - self.latitude
    vy = next_point.longitude - self.longitude

    k = (ux * vx + uy * vy) / Math::sqrt((ux * ux + uy * uy) * (vx * vx + vy * vy))

    Math::acos((k < 1) ? k : 1)
  end

end
