class IconsTemplate < ActiveRecord::Base
  attr_accessible :amount, :icon_id, :template_id

  belongs_to :icon
  belongs_to :template

  validates :icon, :template, :presence => true

end
