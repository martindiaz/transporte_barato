class Kml < ActiveRecord::Base
  attr_accessible :body, :name

  validates :name, uniqueness: true

  def self.provinces
    find_by_name("provinces")
  end

  def self.ba_cities
    find_by_name("ba_cities")
  end

  def self.gba_zones
    find_by_name("gba_zones")
  end
end
