class Rating < ActiveRecord::Base
  attr_accessible :comment, :points, :budget_id, :owner_id, :receiver_id

  belongs_to :budget

  has_one :demand, :through => :budget

  belongs_to :owner,    class_name: "User"
  belongs_to :receiver, class_name: "User"

  validates :points, :comment, presence: true
  validates :points, numericality: { greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }

  scope :pending, where(:points => nil)
end
