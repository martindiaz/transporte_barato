class Demand < ActiveRecord::Base
  include Status::Demand
  include Provider

  # => TODO: :destiny & :origin Addresses must belong to destiny_information & origin_information

  resourcify

  attr_accessible :category, :category_id, :client_id, :delivery_date, :description, :destiny, :origin,
                  :origin_attributes, :destiny_attributes, :origin_information_attributes,
                  :destiny_information_attributes, :status, :expiration_date, :data, :icons,
                  :distance, :extra_service_ids, :detail, :height, :height_unit, :width, :width_unit, :depth, :depth_unit,
                  :weight, :weight_unit, :image_ids, :budget_hosting_expiration_date, :email, :provider

  serialize :data, ActiveRecord::Coders::Hstore
  serialize :icons, ActiveRecord::Coders::Hstore

  has_many :budgets, dependent: :destroy, order: 'price'
  has_many :notifications, as: :notifiable
  has_many :images, as: :imageable, class_name: Image
  has_many :comments, as: :commentable

  belongs_to :client
  belongs_to :category

  belongs_to :destiny, class_name: 'Address', foreign_key: 'destiny_id'
  belongs_to :origin,  class_name: 'Address', foreign_key: 'origin_id'

  belongs_to :destiny_information, class_name: 'LocationInformation', foreign_key: 'destiny_information_id'
  belongs_to :origin_information,  class_name: 'LocationInformation', foreign_key: 'origin_information_id'

  has_and_belongs_to_many :extra_services

  # Validations for HOME & STANDARD providers
  validates :category, presence: true, if: :active_or_first_step?
  validates :description, presence: true, if: :active_or_third_step?
  validates :destiny_information, :origin_information,
    :destiny, :origin, :budget_hosting_expiration_date,
      presence: true, if: :active_or_second_step_from_home_or_standard?
  validate :delivery_dates, if: :active_or_second_step_from_home_or_standard?
  validate :expiration_date_greater_than_delivery_date, if: :active_or_second_step_from_home_or_standard?

  # Validations for LANDING provider
  validates :destiny, :origin, :email, :description, presence: true, if: :from_landing_and_first_step?
  validates :email, email: true, if: :from_landing_and_first_step?
  validates :destiny_information, :origin_information, presence: true, if: :from_landing_and_second_step?
  validate :delivery_dates, if: :from_landing_and_second_step?

  delegate :zone, to: :address

  def delivery_dates
    if self.destiny_information &&
      self.destiny_information.delivery_date_from &&
        self.origin_information &&
          self.origin_information.delivery_date_from
      if self.destiny_information.delivery_date_from < self.origin_information.delivery_date_from
        errors.add(:la_fecha_de_entrega, "no puede ser menor a la de retiro")
      end
    end
  end

  def expiration_date_greater_than_delivery_date
    if self.origin_information &&
      self.origin_information.delivery_date_from &&
        self.budget_hosting_expiration_date
      if self.budget_hosting_expiration_date > self.origin_information.delivery_date_from
        errors.add(:expiration_date, "no puede ser mayor a la fecha de retiro")
      end
    end
  end

  accepts_nested_attributes_for :origin, :destiny, :destiny_information, :origin_information

  delegate :user, to: :client
  delegate :transporter, to: :accepted_budget, prefix: true

  PRICE_MATRIX = [
    [3, 3, 4, 4, 5, 5, 6, 6],
    [3, 4, 4, 4, 5, 6, 6, 7],
    [4, 4, 4, 5, 5, 6, 7, 8],
    [4, 4, 5, 5, 6, 7, 7, 8],
    [4, 5, 5, 6, 7, 7, 8, 9],
    [5, 5, 6, 7, 7, 8, 9, 10],
    [5, 6, 7, 7, 8, 9, 10, 11],
    [6, 7, 7, 8, 9, 10, 11, 12],
  ]

  def self.accepted_budgets
    all.map { |demand| demand.accepted_budget }
  end

  def self.to_be_delivered_at(date)
    where(delivery_date: date, status: ACTIVE)
  end

  def self.to_be_expired_at(date)
    where(delivery_date: date, status: ACTIVE)
  end

  def self.with_budgets_to_be_expired_at(date)
    all.select { |demand|
      demand.budgets_to_be_expired_at(date).any?  }
  end

  def self.which_reception_period_finishes_at(date)
    where(budget_hosting_expiration_date: date, status: ACTIVE)
  end

  def activate
    self.status = ACTIVE
    self.save(validate: false)
  end

  def has_email?
    !!self.email
  end

  def accomplish
    self.status = ACCOMPLISHED

    self.save(validate: false)

    # CREAR RATINGS
    rate = Rating.new(owner_id: self.client.user.id, receiver_id: self.accepted_budget.transporter.user.id, budget_id: self.accepted_budget.id, points: nil)
    rate.save(validate: false)
    rate = Rating.new(owner_id: self.accepted_budget.transporter.user.id, receiver_id: self.client.user.id, budget_id: self.accepted_budget.id, points: nil)
    rate.save(validate: false)
  end

  def discard
    self.status = DISCARDED
    self.save(validate: false)
  end

  def active_budgets_for_change
    self.budgets.active.reject { |budget|
      budget.cancelled? || budget.expired?  }
  end

  def active_budgets
    self.budgets.active.reject { |budget| budget.not_sent? }
  end

  def not_sent_budgets
    self.budgets.not_sent
  end

  def accepted_budget
    self.budgets.where(status: Status::Budget::ACCEPTED).first
  end

  def parsed_data
    results = {}

    array_result = self.data.collect do | key, value |
      if value =~ /{*.}/i
        {key => JSON.parse(value.gsub('=>', ':'))}
      else
        {key => value}
      end
    end

    array_result.each {|a| results.merge! a }
    results
  end

  {
    active:       ACTIVE,
    accomplished: ACCOMPLISHED,
    discarded:    DISCARDED
  }.each do |key, value|
    define_method("#{key}?".to_sym) { status == value }
    define_singleton_method(key) { where status: value }
  end

  {
    home:     HOME,
    landing:  LANDING,
    standard: STANDARD
  }.each do |key, value|
    define_method("from_#{key}?".to_sym) { provider == value }
    define_singleton_method("from_#{key}".to_sym) { where provider: value }
  end

  def have_dimensions?
    !self.width.blank? || !self.depth.blank? || !self.height.blank? || !self.weight.blank?
  end

  def expire
    self.expiration_date = Date.today
    self.status = EXPIRED
    self.save(validate: false)
  end

  def expired?
    self.status == EXPIRED
  end

  def finish_budget_reception_period
    self.not_sent_budgets.each do |budget|
      budget.not_sent_end_of_period
    end
  end

  def parsed_icons
    results = {}

    self.icons.each do |id, qty|
      icon = Icon.find(id)
      results[icon] ||= 0
      results[icon] += qty.to_i
    end

    results
  end

  def volumen

    total = if self.height && self.width && self.depth

      ((Unit("#{self.height} #{self.height_unit}") * Unit("#{self.width} #{self.width_unit}") * Unit("#{self.depth} #{self.depth_unit}")) >> "m^3").scalar.to_f

    else

      total = 0.0

      self.parsed_icons.each do |icon, qty|

        icon.volume ||= 0
        total += icon.volume * qty

      end

      total

    end

    total.round(2) if total

  end

  def total_weight
    if self.weight
      (Unit("#{self.weight} #{self.weight_unit}") >> "tn").scalar.to_f
    else
      total = 0.0

      self.parsed_icons.each do |icon, qty|

        total += (icon.weight || 0) * qty

      end

      (Unit("#{total} kg") >> "tn").scalar.to_f
    end
  end

  def price

    coeficient = self.volumen > self.total_weight ? self.volumen : self.total_weight

    y_axis = if coeficient < 1
      0
    elsif coeficient < 3
      1
    elsif coeficient < 5
      2
    elsif coeficient < 10
      3
    elsif coeficient < 20
      4
    elsif coeficient < 30
      5
    elsif coeficient < 60
      6
    else
      7
    end

    self.distance ||= "0"

    distance_in_number = self.distance.gsub(".", "").to_f

    x_axis = if distance_in_number < 3
      0
    elsif distance_in_number < 15
      1
    elsif distance_in_number < 50
      2
    elsif distance_in_number < 100
      3
    elsif distance_in_number < 500
      4
    elsif distance_in_number < 1000
      5
    elsif distance_in_number < 1500
      6
    else
      7
    end

    self.category.base_price + PRICE_MATRIX[x_axis][y_axis]

  end

  def transporters_with_budgets
    transporters = Set.new
    self.budgets.each { |budget| transporters << budget.transporter }
    transporters
  end

  def transporters_with_budgets_to_be_confirmed
    transporters = Set.new
    self.budgets.to_be_confirmed.each do |budget|
      transporters << budget.transporter
    end
    transporters
  end

  def transporters_rejected
    transporters = Set.new
    self.budgets.each do |budget|
      if budget.another_was_accepted?
        transporters << budget.transporter
      end
    end
    transporters
  end

  def transporter_has_sent_budget?(transporter)
    transporters_with_budgets.include?(transporter)
  end

  def has_new_comments?(the_user)
    self.budgets.select { |budget|
      budget.has_new_comments?(the_user) }.any?
  end

  def has_new_budgets?
    new_budgets.any?
  end

  def new_budgets
    self.budgets.select { |budget|
      budget.sent? }
  end

  def budgets_by_transporter(transporter)
    self.budgets.select { |budget|
      budget.transporter == transporter }
  end

  def budgets_to_be_expired_at(date)
    budgets.to_be_expired_at(date)
  end

  def first_step?
    self.status == FIRST_STEP
  end

  def active_or_first_step?
    active? || first_step?
  end

  def second_step?
    self.status == SECOND_STEP
  end

  def active_or_second_step_from_home_or_standard?
    active? || (second_step? && (from_home? || from_standard?))
  end

  def third_step?
    self.status == THIRD_STEP
  end

  def active_or_third_step?
    active? || third_step?
  end

  def from_landing_and_first_step?
    from_landing? && first_step?
  end

  def from_landing_and_second_step?
    from_landing? && second_step?
  end

  def has_zone?
    !!self.address_zone
  end

  def self.csv(options={})
    CSV.generate(options) do |csv|
      csv << ['Fecha', 'Usuario', 'Categoria', 'Subcategoria', 'Titulo', 'Estado', 'Cant. de presupuestos']

      all.each do |demand|
        csv << [ (I18n.l demand.created_at.to_date),
                  (demand.client.blank? ? '' : demand.user.username),
                  demand.category.parent.name,
                  demand.category.name,
                  demand.description,
                  demand.status,
                  demand.budgets.size ]
      end
    end
  end
end
