class Comment < ActiveRecord::Base
  attr_accessible :message, :user_id, :question_id

  belongs_to :user
  belongs_to :commentable, polymorphic: true
  belongs_to :question, class_name: "Comment", foreign_key: "question_id"

  has_one :answer, class_name: "Comment", foreign_key: "question_id"

  has_many :notifications, as: :notifiable

  validates :message, :user_id, presence: true

  def read
    self.status = Status::Comment::READ
    self.save(validate: false)
  end

  def new_by_client?
    self.unread? && self.user.client?
  end

  def new_by_transporter?
    self.unread? && self.user.transporter?
  end

  def read?
    self.status == Status::Comment::READ
  end

  def unread?
    self.status == Status::Comment::UNREAD
  end

  def commentable_is_demand?
    self.commentable.kind_of? Demand
  end

  def commentable_is_budget?
    self.commentable.kind_of? Budget
  end

  def commentator_is_transporter?
    self.user.role.kind_of? Transporter
  end

  def commentator_is_client?
    self.user.role.kind_of? Client
  end
end
