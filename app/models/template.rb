class Template < ActiveRecord::Base
  attr_accessible :description, :icon_ids, :category_ids

  has_many :icons_templates
  has_many :icons, :through => :icons_templates, inverse_of: :templates

  has_and_belongs_to_many :categories
end
