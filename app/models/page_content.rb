class PageContent < ActiveRecord::Base
  attr_accessible :body, :name

  validate :name, :body, presence: true
end
