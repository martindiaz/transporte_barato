class Image < ActiveRecord::Base
  belongs_to :imageable, :polymorphic => true

  mount_uploader :image, ImageUploader

  attr_accessible :image, :image_cache, :remove_image

  ImageUploader.versions.keys.each {|key| delegate key, to: :image }

  def to_s
    self.image.url
  end
end
