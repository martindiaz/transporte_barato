class MovementType < ActiveRecord::Base
  attr_accessible :name, :description

  has_many :credits
end
