# encoding: UTF-8

class Category < ActiveRecord::Base
  attr_accessible :name, :ancestry, :template_id, :base_price, :order, :image_attributes

  has_one :image, as: :imageable, dependent: :destroy, class_name: Image

  has_and_belongs_to_many :templates
  has_and_belongs_to_many :forms
  has_and_belongs_to_many :companies

  has_ancestry

  accepts_nested_attributes_for :image

  def to_s
    self.name
  end

  def ancestry=(value)
    value = nil if value.blank?

    super
  end

  def normalized_name
    self.name.downcase.gsub('/', ' ').split.join('_').gsub(/é/, 'e').gsub(/ó/, 'o').gsub(/á/, 'a').gsub(/í/, 'i').gsub(/ú/, 'u')
  end
end
