class Vehicle < ActiveRecord::Base
  resourcify

  belongs_to :transporter
  has_many :images, as: :imageable, class_name: Image

  attr_accessible :description, :name, :image_ids

end
