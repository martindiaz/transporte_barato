class Client < ActiveRecord::Base
  resourcify

  has_one  :user, as: :role, dependent: :destroy
  has_many :demands, dependent: :destroy

  attr_accessible :user_attributes
  accepts_nested_attributes_for :user, allow_destroy: true

  delegate :email,
           :add_role,
           :name,
           :surname,
           :receive_email_alerts,
           :username,
           :comments,
           :received_ratings,
           :total_rating,
           :positive_ratings,
           :neutral_ratings,
           :negative_ratings,
           to: :user

  delegate :active, :discarded, to: :demands, prefix: true

  def has_a_complete_profile?
    true
  end

  def accepted_budgets
    self.demands.map { |demand| demand.accepted_budget }.compact
  end

  def demands_with_new_transporter_comments(the_user)
    self.demands.select { |demand|
      demand.has_new_comments?(the_user) }
  end

  def has_news?
    news_count > 0
  end

  def news_count
    new_budgets_count + demands_with_new_transporter_comments(self.user).count
  end

  def new_budgets_count
    self.demands.collect { |demand|
      demand.new_budgets.count }.inject(&:+) || 0
  end

  def self.csv(options={})
    CSV.generate(options) do |csv|
      csv << ['Fecha', 'Nombre', 'Apellido', 'Email', 'Cant. de envios']

      all.each do |client|
        csv << [(I18n.l client.created_at.to_date),
                client.name,
                client.surname,
                client.email,
                client.demands.where(status: ["ACTIVE", "ACCOMPLISHED", "DISCARDED", "EXPIRED"]).count ]
      end
    end
  end
end
