class Zone < ActiveRecord::Base
  attr_accessible :administrative_areas, :cities, :name
  has_many :addresses

  class << self
    def with_city(city)
      where("? = ANY (cities)", city)
    end

    def with_administrative_area(administrative_area)
      where("? = ANY (administrative_areas)", administrative_area)
    end
  end
end
