class HomeBanner < ActiveRecord::Base
  attr_accessible :image_attributes, :transporter_id

  has_one :image, as: :imageable, class_name: Image, dependent: :destroy
  belongs_to :transporter

  accepts_nested_attributes_for :image

  delegate :username, :company_name,
    :total_rating, :received_ratings, :total_comments, to: :transporter, prefix: true

  validate :transporter_id, presence: true
end
