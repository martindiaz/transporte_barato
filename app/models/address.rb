class Address < ActiveRecord::Base
  belongs_to :addressable, polymorphic: true
  belongs_to :zone

  attr_accessible :apartment, :building, :floor, :street_line1, :street_line2,
                  :zipcode, :city, :country, :state, :longitude, :latitude, :neighborhood,
                  :administrative_area

  validates :street_line2, presence: true

  geocoded_by :full_address

  delegate :name, to: :zone, prefix: true

  after_save :assign_zone

  def full_address
    [self.street_line1, self.neighborhood, self.city, self.state].delete_if{|text| text.blank? }.to_sentence(last_word_connector: ', ', two_words_connector: ', ')
  end

  def to_s(format=:full)
    case format
    when :full
      self.full_address
    when :short
      [self.street_line1, self.neighborhood, self.city].delete_if{|text| text.blank? }.to_sentence(last_word_connector: ', ', two_words_connector: ', ')
    else
      raise "Inexistent format"
    end
  end

  def address_point
    Point.new(longitude: self.longitude, latitude: self.latitude)
  end

  def contains_state? address
    address.state == self.state
  end

  def contains_administrative_area? address
    address.administrative_area == self.administrative_area
  end

  def contains_city? address
    address.city == self.city
  end

  def contains_zone? address
    address.zone_name == self.zone_name
  end

  def assign_zone
    zones = Zone.with_administrative_area(self.administrative_area).
      with_city(self.city)

    if zones.any?
      zone_id = (zones.count == 1) ? zones.first.id : nil
    end

    self.update_column :zone_id, zone_id
  end

end
