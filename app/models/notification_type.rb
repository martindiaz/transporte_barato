class NotificationType < ActiveRecord::Base
  attr_accessible :description, :name, :partial
end
