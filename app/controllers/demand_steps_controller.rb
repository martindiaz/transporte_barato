class DemandStepsController < ApplicationController
  include Wicked::Wizard

  skip_before_filter :authenticate_user!

  expose(:demand) do
    if session[:demand_id]
      Demand.find(session[:demand_id])
    end
  end

  steps :required, :additional

  def show
    @category = demand.category

    case step
    when :required
      demand.origin_information ||= LocationInformation.new
      demand.destiny_information ||= LocationInformation.new
    when :additional
      @groups = []

      if !@category.templates.blank?
        @groups = @category.templates.first.icons.collect{ |icon| icon.group }.uniq
      end

      if @category.parent
        if !@category.parent.templates.blank?
          @groups << @category.parent.templates.first.icons.collect{ |icon| icon.group }.uniq
        end
      end

      @groups.compact!
    else
    end

    render_wizard
  end

  def update
    case step
    when :required
      demand.status = Status::Demand::SECOND_STEP
      demand.origin_information  ||= LocationInformation.new
      demand.destiny_information ||= LocationInformation.new

      demand.expiration_date = Date.parse(params[:demand][:budget_hosting_expiration_date]) +
        15.days unless params[:demand][:budget_hosting_expiration_date].blank?

    when :additional
      demand.status = Status::Demand::THIRD_STEP

      params[:images] ||= []
      params[:images] << demand.image_ids
      demand.image_ids = params[:images].flatten
    end

    if demand.update_attributes(params[:demand])
      render_wizard demand
    else
      @category = demand.category
      @groups = []

      if !@category.templates.blank?
        @groups = @category.templates.first.icons.collect{ |icon| icon.group }.uniq
      end

      if @category.parent
        if !@category.parent.templates.blank?
          @groups << @category.parent.templates.first.icons.collect{ |icon| icon.group }.uniq
        end
      end

      @groups.compact!

      render_wizard
    end
  end

  def finish_wizard_path
    demand.status = Status::Demand::ACTIVE
    demand.save
    new_user_session_path
  end

end
