module UsersController

  # Initializes the controller, authenticate the user & exposes values
  # @param [Symbol] model to be initialized
  def init_user(model)
    before_filter :authenticate_user!, :only => [:show, :edit]

    expose(model.to_s.pluralize.to_sym)
    expose(model)
  end

  # Creates a new blank User or a new User based on facebook data
  # @return [User] the instance
  def create_user_instance(session)
    if session["devise.facebook_data"].nil?
      User.new
    else
      User.find_for_facebook_oauth(session["devise.facebook_data"], nil)
    end
  end

end
