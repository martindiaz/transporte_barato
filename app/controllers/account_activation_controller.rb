class AccountActivationController < ApplicationController

  skip_before_filter :authenticate_user!

  expose(:user)   { User.where(confirmation_token: params[:id]).first }
  expose(:demand) { user.role.demands.last }

  def show
    if user.has_signed_in?
      demand.activate
      sign_in(user.class, user)
    end
  end

  def update
    if user.update_attributes(params[:user])
      demand.activate
      sign_in(user.class, user)
      redirect_to root_url
    else
      render :show
    end
  end

end
