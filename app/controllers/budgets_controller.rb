class BudgetsController < ApplicationController
  before_filter :sanitize_numbers, only: [:create, :update]

  expose(:transporter) { current_user.role }
  expose(:demand) do
    if params[:demand_id]
      Demand.find(params[:demand_id])
    else
      budget.demand
    end
  end

  expose(:budgets) do
    if params[:demand_id]
      demand.budgets
    else
      transporter.budgets
    end
  end

  expose(:budget) do
    if params[:action] == "accept" || params[:action] == "discard"
      demand.budgets.find(params[:id])
    elsif params[:id]
      transporter.budgets.find(params[:id])
    else
      demand.budgets.build(params[:budget])
    end
  end

  expose(:comment) { budget.comments.build }

  expose(:active_budget_demands) do
    active = transporter.budgeted_demands :active
    Kaminari.paginate_array(active).page(params[:page]).per(10)
  end
  expose(:accepted_budget_demands) do
    accepted = transporter.budgeted_demands :accepted
    Kaminari.paginate_array(accepted).page(params[:page]).per(10)
  end
  expose(:discarded_budget_demands) do
    discarded = transporter.budgeted_demands :discarded
    Kaminari.paginate_array(discarded).page(params[:page]).per(10)
  end

  # GET /budgets/new
  # GET /budgets/new.json
  def new
    unless transporter.has_a_complete_profile?
      redirect_to edit_transporter_url(transporter), flash: {error: 'Debe completar su perfil para poder crear un presupuesto.'}
    end
  end

  # POST /budgets
  # POST /budgets.json
  def create
    if params[:comment] && !params[:comment][:message].blank?
      comment.message = params[:comment][:message]
      comment.user_id = transporter.user.id
      comment.save
    end

    budget.transporter = transporter
    budget.set_status

    if budget.save
      redirect_to transporter_budgets_url(transporter), flash: {success: 'El presupuesto fue creado exitosamente.'}
    else
      redirect_to demand_url(budget.demand), flash: {error: 'El presupuesto no fue fue creado.'}
    end
  end

  # PUT /budgets/1
  # PUT /budgets/1.json
  def update
    if params[:comment] && !params[:comment][:message].blank?
      comment.message = params[:comment][:message]
      comment.user_id = transporter.user.id
      comment.save
    end

    budget.modify(save: false)

    if budget.update_attributes(params[:budget])
      redirect_to transporter_budgets_url(transporter), flash: {success: 'El presupuesto fue actualizado exitosamente.'}
    else
      render :edit, flash: {error: 'El presupuesto no fue fue creado.'}
    end
  end

  # DELETE /budgets/1
  # DELETE /budgets/1.json
  def destroy
    budget.destroy
    redirect_to transporter_budgets_path(transporter)
  end

  def accept
    budget.accept
    redirect_to accomplish_demand_url(budget.demand)
  end

  def discard
    budget.discard
    redirect_to demands_url
  end

  def receive
    budget = Budget.find(params[:id])
    budget.receive
    render nothing: true
  end

  def confirm
    budget.confirm
    redirect_to transporter_budgets_path(transporter)
  end

  def cancel
    budget.cancel
    redirect_to transporter_budgets_path(transporter)
  end

  def expire
    if current_user.transporter?
      budget.expire
      render text: "El presupuesto #{budget.id} ha caducado."
    else
      render text: "Para caducar un presupuesto es necesario iniciar sesion como transportista."
    end
  end

  private

  def sanitize_numbers
    params[:budget][:price].gsub!(".", "") if params[:budget] and params[:budget][:price]
  end
end
