class VehiclesController < ApplicationController

  expose(:transporter) { current_user.role }
  expose(:vehicles)    { transporter.vehicles }
  expose(:vehicle) do
    if params[:id]
      transporter.vehicles.find(params[:id])
    else
      transporter.vehicles.build(params[:vehicle])
    end
  end

  # POST /vehicles
  # POST /vehicles.json
  def create
    vehicle.image_ids = params[:images]

    if vehicle.save
      redirect_to edit_transporter_path(current_user.role), flash: {success: 'El vehiculo fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /vehicles/1
  # PUT /vehicles/1.json
  def update
    if params[:images]
      params[:images] << vehicle.image_ids
      vehicle.image_ids = params[:images].flatten
    end

    if vehicle.update_attributes(params[:vehicle])
      redirect_to edit_transporter_path(current_user.role), flash: {success: 'El vehiculo fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy
    vehicle.images.each {|image| image.delete }
    vehicle.image_ids = []
    vehicle.destroy

    redirect_to edit_transporter_path(current_user.role)
  end
end
