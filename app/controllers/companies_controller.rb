class CompaniesController < ApplicationController

  expose(:transporter) { current_user.role }
  expose(:company) do
    if id = params[:id]
      transporter.company
    else
      Company.new(params[:company])
    end
  end
  expose(:address) { company.address ||= company.build_address }

  # GET /companies
  # GET /companies.json
  def index
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  # GET /companies/new.json
  def new
    company.image ||= company.build_image
  end

  # GET /companies/1/edit
  def edit
    company.image ||= company.build_image
  end

  # POST /companies
  # POST /companies.json
  def create
    address.attributes  = params[:address]
    company.transporter = transporter
    company.address     = address

    if company.save && address.save(validate: false)
      redirect_to edit_transporter_url(transporter), flash: { success: 'Los datos fueron actualizados correctamente.' }
    else
      redirect_to edit_transporter_url(transporter), flash: { error: 'Los datos no fueron actualizados.' }
    end
  end

  # PUT /companies/1
  # PUT /companies/1.json
  def update
    params[:company][:category_ids]          ||= []
    params[:company][:payment_method_ids]    ||= []
    params[:company][:payment_condition_ids] ||= []
    params[:company][:extra_service_ids]     ||= []

    address.attributes = params[:address]

    if address.save(validate: false) &&
      company.update_attributes(params[:company])
      redirect_to edit_transporter_url(transporter), flash: { success: 'Los datos fueron actualizados correctamente.' }
    else
      redirect_to edit_transporter_url(transporter), flash: { error: 'Los datos no fueron actualizados.' }
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    company.destroy
    redirect_to transporter
  end
end