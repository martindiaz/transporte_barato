class BillingAddressesController < ApplicationController
  def create
    @billing_address = BillingAddress.new(params[:billing_address])
    @billing_address.user = current_user

    if @billing_address.save
      redirect_to credits_path, notice: "Datos de facturación guardados exitosamente"
    else
      redirect_to credits_path, notice: "Datos de facturación no pudieron ser guardados"
    end
  end

  def update
    @billing_address = current_user.billing_address

    if @billing_address.update_attributes(params[:billing_address])
      redirect_to credits_path, notice: "Datos de facturación guardados exitosamente"
    else
      redirect_to credits_path, notice: "Datos de facturación no pudieron ser guardados"
    end
  end

end
