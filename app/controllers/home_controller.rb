class HomeController < ApplicationController

  skip_before_filter :authenticate_user!, :only => :index

  expose(:categories)       { Category.roots }
  expose(:cities) do
    ["Buenos Aires", "Capital Federal", "CABA", "Zona Norte",
    "Zona Oeste", "Zona Sur", "Gran Buenos Aires", "Cordoba",
    "Rosario", "Mendoza", "La Plata", "Mar del Plata",
    "Santa Fe", "Salta", "San Juan","Resistencia",
    "Santiago del Estero"]
  end
  expose(:categories_for_links) do
    ["Mudanzas", "Mudanza de oficina", "Mudanza de comercio",
    "Mudanza de vivienda", "Transporte de muebles", "Transporte de plantas",
    "Transporte de artículos frágiles", "Transporte de vehiculos",
    "Transporte de motos", "Transporte de coches", "Transporte de lanchas",
    "Transporte de repuestos", "Transporte de piano", "Transporte de maquinaria", "Transporte de animales",
    "Transporte de mascotas", "Transporte de perros", "Transporte de gatos",
    "Transporte de vacas", "Transporte de caballos", "Transporte de granos",
    "Transporte de liquidos", "Transporte de alimentos", "Transporte de textiles",
    "Transporte de pasajeros", "Transporte de cargas", "Transporte de containers",
    "Transporte de palés", "Transporte de cajas"]
  end

  expose(:home_banners) { HomeBanner.all }
  expose(:stats) { PageContent.where(name: "Datos Estadisticos").first }
  expose(:demand) { Demand.new }
  expose(:provider) { Provider::HOME }

  def index
    if current_user && current_user.has_role?(:admin)
      redirect_to admin_root_url
    elsif current_user
      redirect_to edit_current_user_url
    end
  end

end
