class CreditsController < ApplicationController
  expose(:credits) { current_user.credits.where("payment_type is not ?", nil) }
  expose(:credit)
  expose(:pdf_file) { "tarifario.pdf" }

  def index
    @billing_address = current_user.billing_address ||= current_user.build_billing_address
  end

  def show
  end

  def new
  end

  def create
    credit.movement_type = MovementType.find('BUY_CREDITS')
    credit.user = current_user
    credit.status = 'Entregado'

    if credit.save
      redirect_to credits_path, notice: 'La compra se ha realizado con exito'
    else
      render "index"
    end
  end
end
