class ClientsController < ApplicationController
  extend UsersController

  skip_before_filter :authenticate_user!, :only => :index

  init_user :client

  expose(:user)              { client.user || self.class.create_user_instance(session) }
  expose(:active_demands)    { client.demands_active                                   }
  expose(:all_demands)       { client.demands                                          }
  expose(:discarded_demands) { client.demands_discarded                                }
  expose(:accepted_budgets)  { client.accepted_budgets                                 }
  expose(:positive_ratings)  { client.positive_ratings                                 }
  expose(:neutral_ratings)   { client.neutral_ratings                                  }
  expose(:negative_ratings)  { client.negative_ratings                                 }

  # POST /clients
  # POST /clients.json
  def create
    user        = User.new(params[:user])
    user.role   = client
    user.add_role :user
    client.user = user

    if user.save && client.save
      sign_in(user.class, user)
      redirect_to edit_client_url(client), flash: { success: 'El cliente fue creado exitosamente.' }
    else
      render :new
    end
  end

  # PUT /clients/1
  # PUT /clients/1.json
  def update
    # Refactor!
    current_password = params[:user].delete(:current_password)
    update_method = if current_password == "undefined"
      :update_attributes
    else
      params[:user][:current_password] = current_password
      :update_with_password
    end

    if user.send(update_method, params[:user]) && client.update_attributes(params[:client])
      user.must_change_password = false
      user.save validate: false
      sign_in(user.class, user, bypass: true)
      redirect_to edit_client_url(client), flash: {success: 'Su perfil fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    client.destroy
    redirect_to clients_url
  end

  def change_password
  end
end
