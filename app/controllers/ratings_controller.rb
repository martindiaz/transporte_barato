class RatingsController < ApplicationController
  def index
    @pendings = current_user.pending_rating
    @received = current_user.received_ratings
    @given    = current_user.given_ratings
  end

  def update
    @rating = Rating.find(params[:id])

    if @rating.update_attributes(params[:rating])
      redirect_to ratings_path, notice: 'Valorizacion creada correctamente'
    else
      redirect_to ratings_path, notice: 'La valorizacion contiene datos incorrectos'
    end
  end
end
