class RegistrationsController < Devise::RegistrationsController

  def create

    @user = if session["devise.facebook_data"].nil?
      User.new(params[:user])
    else
      User.find_for_facebook_oauth(session["devise.facebook_data"], nil)
    end

    if params[:commit] == "client"
      @client = Client.new
      @user.role   = @client
      @user.add_role :user
      @user.receive_email_alerts = true
      @client.user = @user
    elsif params[:commit] == "transporter"
      @transporter = Transporter.new
      @user.role = @transporter
      @user.add_role :transporter
      @user.receive_email_alerts = true
      @transporter.user = @user
    end

    if !session["devise.facebook_data"].blank?
      @user.accept_use_conditions = true
      @user.save(validate: false)
      @user.role.save(validate: false)

      if @user.has_role? :transporter
        credit = @user.credits.build
        credit.amount = 10000
        credit.movement_type = MovementType.find('BUY_CREDITS')
        credit.status = 'Entregado'
        credit.save

        company = @user.role.build_company
        company.save validate: false
      end

      sign_in(@user.class, @user)
      assign_user_to_demand
      redirect_to edit_current_user_url
    else
      if @user.save && @user.role.save(validate: false)
        if @user.has_role? :transporter
          credit = @user.credits.build
          credit.amount = 10000
          credit.movement_type = MovementType.find('BUY_CREDITS')
          credit.status = 'Entregado'
          credit.save

          company = @user.role.build_company
          company.save validate: false
        end

        sign_in(@user.class, @user)
        assign_user_to_demand
        redirect_to edit_current_user_url
      else
        @user.errors[:email].uniq! unless @user.errors[:email].blank?

        render 'devise/sessions/new'
      end
    end

  end

end
