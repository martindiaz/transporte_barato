class DemandsController < ApplicationController
  include Provider

  skip_before_filter :authenticate_user!, only: [:search, :new, :create, :update]

  before_filter :is_client?, only: [:new], if: :current_user_logged_in?

  expose(:user) { current_user.nil? ? nil : current_user.role }
  expose(:demand) do
    if id = params[:id]
      Demand.find(id)
    elsif params[:demand].respond_to?(:each)
      if category_id = params[:demand][:category]
        params[:demand][:category] = Category.find category_id
      end

      if user
        user.demands.build(params[:demand])
      else
        Demand.new(params[:demand])
      end
    else
      if user
        user.demands.build
      else
        Demand.new
      end
    end
  end

  expose(:demands) do
    results = if current_user.client?
      user.demands
    else
      Demand.all
    end

    results = results.order('created_at DESC')
  end

  expose(:budget) do
    demand.budgets.build(params[:budget])
  end
  expose(:comment) { budget.comments.build }

  expose(:demand_search) do
    if params[:search]
      params[:search].each do |k, v|
        if k =~ /date/ && !v.empty? && !v.nil?
          params[:search][k] = Date.parse(v)
        end

        if k =~ /category/ && !v.empty? && !v.nil?
          params[:search][k].concat v.map { |category_id|
            Category.find(category_id).children.map { |children| children.id }
          }.flatten
        end
      end
    end

    Demand.active.search(params[:search], search_key: :log_search)
  end

  expose(:result) do
    r = demand_search.result

    if params[:search] && vehicle_types = params[:search][:vehicle_types]
      types = vehicle_types.map { |v| "'#{v}'" }.join(",")
      r = r.where("data -> 'vehicle_type' IN (#{types})")
    end

    r.where("expiration_date > ? AND client_id IS NOT NULL", Date.today)
      .order("created_at DESC").page(params[:page]).per(10)
  end

  expose(:active_demands) do
    if demands.respond_to?(:active)
      if params[:status] == "active"
        demands.active.page(params[:page]).per(10)
      else
        demands.active.page(1).per(10)
      end
    else
      Demand.active.page(params[:page]).per(10)
    end
  end

  expose(:accomplished_demands) do
    if demands.respond_to?(:accomplished)
      if params[:status] == "accomplished"
        demands.accomplished.page(params[:page]).per(10)
      else
        demands.accomplished.page(1).per(10)
      end
    else
      Demand.accomplished.page(params[:page]).per(10)
    end
  end

  expose(:discarded_demands) do
    if demands.respond_to?(:discarded)
      if params[:status] == "discarded"
        demands.discarded.page(params[:page]).per(10)
      else
        demands.discarded.page(1).per(10)
      end
    else
      Demand.discarded.page(params[:page]).per(10)
    end
  end
  expose(:categories)              { Category.roots }
  expose(:vehicle_characteristics) { VehicleCaracteristic.all }
  expose(:extra_services)
  expose(:active_budgets)          { demand.blank? ? [] : demand.active_budgets }
  expose(:provider)                { Provider::STANDARD }

  # GET /demands
  # GET /demands.json
  def index
    if params[:budget] && params[:demand]
      a_supposed_sent_budget = Budget.find(params[:budget])
      if a_supposed_sent_budget.sent?
        credit = Credit.new(amount: (a_supposed_sent_budget.demand.price * -1))
        credit.movement_type = MovementType.find('BUDGET_VIEWED')
        credit.budget = a_supposed_sent_budget
        credit.user = a_supposed_sent_budget.user
        credit.save
        a_supposed_sent_budget.receive
      end
    end
  end

  # GET /demands/1
  # GET /demands/1.json
  def show
    @category = demand.category
    @groups = []

    if !@category.templates.empty?
      @groups = @category.templates.first.icons.collect{ |icon| icon.group }.uniq
    end

    if @category.parent
      if !@category.parent.templates.empty?
        @groups << @category.parent.templates.first.icons.collect{ |icon| icon.group }.uniq
      end
    end

    @groups.compact!
  end

  # GET /demands/new
  # GET /demands/new.json
  def new
    demand.build_origin
    demand.build_destiny
    demand.build_destiny_information
    demand.build_origin_information

    @category = Category.where(name: 'Mudanzas').first.children.first
    @groups = []

    if !@category.templates.empty?
      @groups = @category.templates.first.icons.collect{ |icon| icon.group }.uniq
    end

    if @category.parent
      if !@category.parent.templates.empty?
        @groups << @category.parent.templates.first.icons.collect{ |icon| icon.group }.uniq
      end
    end

    @groups.compact!
  end

  # GET /demands/1/edit
  def edit
    @category = demand.category
    @groups = []

    if !@category.templates.empty?
      @groups = @category.templates.first.icons.collect{ |icon| icon.group }.uniq
    end

    if @category.parent
      if !@category.parent.templates.empty?
        @groups << @category.parent.templates.first.icons.collect{ |icon| icon.group }.uniq
      end
    end

    @groups.compact!

    session[:demand_id] = demand.id

    redirect_to demand_steps_url
  end

  # POST /demands
  # POST /demands.json
  def create
    demand.status = Status::Demand::FIRST_STEP unless params[:demand][:status]
    demand.image_ids = params[:images]

    unless params[:demand][:budget_hosting_expiration_date].blank?
      demand.expiration_date = Date.parse(params[:demand][:budget_hosting_expiration_date]) + 15.days
    end

    if demand.from_landing? and demand.category.ancestry
      demand.description = demand.category.name
    end

    if demand.save
      session[:demand_id] = demand.id

      if demand.from_landing?
        if demand.category.name == 'Fletes'
          redirect_to landings_fletes_dates_url
        else
          redirect_to landings_mudanzas_dates_url
        end
      else
        redirect_to demand_steps_url
      end

    else
      @category = demand.category
      @groups = []

      if !@category.templates.blank?
        @groups = @category.templates.first.icons.collect{ |icon| icon.group }.uniq
      end

      if @category.parent
        if !@category.parent.templates.blank?
          @groups << @category.parent.templates.first.icons.collect{ |icon| icon.group }.uniq
        end
      end

      @groups.compact!

      if demand.from_landing?
        if demand.category.name == "Fletes"
          redirect_to landings_fletes_places_url,
            flash: {
              demand_errors: demand.errors,
              demand: demand }
        else
          redirect_to landings_mudanzas_places_url,
            flash: {
              demand_errors: demand.errors,
              demand: demand }
        end
      else
        render :new
      end
    end
  end

  # PUT /demands/1
  # PUT /demands/1.json
  def update
    params[:images] ||= []
    params[:images] << demand.image_ids
    demand.image_ids = params[:images].flatten

    if !params[:demand][:budget_hosting_expiration_date].blank?
      demand.expiration_date = Date.parse(params[:demand][:budget_hosting_expiration_date]) +
        15.days
    elsif !params[:demand][:origin_information_attributes][:delivery_date_from].blank?
      demand.budget_hosting_expiration_date = Date.parse(
        params[:demand][:origin_information_attributes][:delivery_date_from])
      demand.expiration_date = Date.parse(
        params[:demand][:origin_information_attributes][:delivery_date_from]) + 15.days
    end

    session[:demand_id] = demand.id

    if demand.update_attributes(params[:demand])
      if demand.from_landing?
        if demand.category.name == 'Fletes'
          redirect_to landings_fletes_thanks_url
        else
          redirect_to landings_mudanzas_thanks_url
        end
      else
        redirect_to demand_steps_url
      end
    else
      @category = demand.category
      @groups = []

      if !@category.templates.blank?
        @groups = @category.templates.first.icons.collect{ |icon| icon.group }.uniq
      end

      if @category.parent
        if !@category.parent.templates.blank?
          @groups << @category.parent.templates.first.icons.collect{ |icon| icon.group }.uniq
        end
      end

      @groups.compact!

      if demand.from_landing?
        if demand.category.name == "Fletes"
          redirect_to landings_fletes_dates_url,
            flash: {
              demand_errors: demand.errors,
              demand: demand }
        else
          redirect_to landings_mudanzas_dates_url,
            flash: {
              demand_errors: demand.errors,
              demand: demand }
        end
      else
        redirect_to demand_steps_url(demand_id: demand.id)
      end
    end
  end

  # DELETE /demands/1
  # DELETE /demands/1.json
  def destroy
    demand.destroy
    redirect_to demands_url
  end

  def accomplish
    demand.accomplish
    redirect_to demands_url
  end

  def discard
    demand.discard
    redirect_to demands_url
  end

  def search
  end

  def expire
    if current_user.client?
      demand.expire
      render text: "El envio '#{demand.description}' ha expirado"
    else
      render text: "Para realizar la accion es necesario iniciar sesion como usuario."
    end
  end

  def finish_budget_reception_period
    if current_user.client?
      demand.finish_budget_reception_period
      render text: "El periodo de recepcion del envio '#{demand.description}' ha finalizado"
    else
      render text: "Para realizar la accion es necesario iniciar sesion como usuario."
    end
  end

end
