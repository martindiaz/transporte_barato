class CategoriesController < ApplicationController

  skip_before_filter :authenticate_user!, only: [:show, :children]

  expose(:demand) { Demand.new }

  respond_to :html, :json

  layout false

  def show
    @category = Category.find(params[:id])

    @groups = []

    if !@category.templates.empty?
      @groups = @category.templates.first.icons.collect{ |icon| icon.group }.uniq
    end

    if @category.parent
      if !@category.parent.templates.empty?
        @groups << @category.parent.templates.first.icons.collect{ |icon| icon.group }.uniq
      end
    end

    @groups.compact!

    render "categories/#{"#{@category.parent.normalized_name}/" if @category.parent}#{@category.normalized_name}"
  end

  def children
    render json: Category.find(params[:category_id]).children.order('categories.order')
  end

end
