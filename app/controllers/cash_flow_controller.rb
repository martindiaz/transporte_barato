class CashFlowController < ApplicationController

  def create
    credit = nil

    if params[:budget_id]
      budget = Budget.find(params[:budget_id])
      credit = Credit.new(amount: (budget.demand.price * -1))
      credit.movement_type = MovementType.find('BUDGET_VIEWED')
      credit.budget = budget
      credit.user = credit.budget.user
      credit.save
    end

    respond_to do |format|
      format.json { render json: credit }
    end
  end
end
