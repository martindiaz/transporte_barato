class FavoritesController < ApplicationController
  def index
    @demands = current_user.role.favorites.active
  end

  def create
    transporter = current_user.role
    demand = Demand.find(params[:demand_id])

    if transporter.favorites.exists? id: params[:demand_id]
      transporter.favorites.delete demand
      redirect_to request.referer
    else
      transporter.favorites << demand
      redirect_to request.referer
    end
  end
end
