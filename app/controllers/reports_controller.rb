class ReportsController < ApplicationController

  expose(:transporter) { current_user.role }
  expose(:budgets) do
    budgets_list = unless params[:report][:from].empty? || params[:report][:to].empty?
      from, to = Date.parse(params[:report][:from]), Date.parse(params[:report][:to])
      from = from.to_datetime
      to = to.to_datetime + 23.hour + 59.minute + 59.second
      transporter.budgets.where(created_at: from..to)
    else
      transporter.budgets
    end

    budgets_list.order('created_at DESC')

  end

  expose(:from) { params[:report][:from] if params[:report] }
  expose(:to)   { params[:report][:to]   if params[:report] }

  expose(:total_budgets_amount) do
    budgets.inject(0) { |total, budget| total + budget.price }
  end

  expose(:total_accepts_budgets_amount) do
    budgets.inject(0) do |total, budget|
      total + (budget.accepted? ? budget.price : 0 )
    end
  end


  expose(:total_discount_amount) do
    budgets.inject(0) do |total, budget|
      total + budget.total_credits
    end
  end

  def index
    if params[:historical_cost]
      redirect_to transporter_reports_historical_cost_url(transporter, params)
    elsif params[:budgets]
      redirect_to transporter_reports_budget_situation_url(transporter, params)
    end
  end

  def historical_cost
  end

  def budget_situation
  end
end
