class PolygonsController < ApplicationController
  expose(:polygons)
  expose(:polygon)
  expose(:points) { polygon.points }
  
  # GET /polygons
  # GET /polygons.json
  def index
  end

  # GET /polygons/1
  # GET /polygons/1.json
  def show
  end

  # GET /polygons/new
  # GET /polygons/new.json
  def new
  end

  # GET /polygons/1/edit
  def edit
  end

  # POST /polygons
  # POST /polygons.json
  def create
    if points_params = params[:polygon][:points]
      new_points = []
      points_params.each { |k, v| new_points << Point.new(v) }
      params[:polygon][:points] = new_points
    end
    
    polygon = Polygon.new(params[:polygon])
    
    if polygon.save
      redirect_to polygon, notice: 'El poligono fue creado exitosamente.'
    else
      render :new
    end
  end

  # PUT /polygons/1
  # PUT /polygons/1.json
  def update
    if points_params = params[:polygon][:points]
      new_points = []
      points_params.each { |k, v| new_points << Point.new(v) }
      params[:polygon][:points] = new_points
    end

    if polygon.update_attributes(params[:polygon])
      redirect_to polygon, notice: 'El poligono fue actualizado exitosamente.'
    else
      render :edit
    end
  end

  # DELETE /polygons/1
  # DELETE /polygons/1.json
  def destroy
    polygon.destroy
    redirect_to polygons_url
  end
end
