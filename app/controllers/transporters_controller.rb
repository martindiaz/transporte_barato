class TransportersController < ApplicationController
  extend UsersController

  skip_before_filter :authenticate_user!, :only => :index

  init_user :transporter

  expose(:user)               { transporter.user || self.class.create_user_instance(session) }
  expose(:demands)            { Demand.available_for(transporter)                            }
  expose(:company)            { transporter.company || Company.new                           }
  expose(:address)            { company.address ||= company.build_address                    }
  expose(:categories)         { Category.roots                                               }
  expose(:payment_methods)    { PaymentMethod.all                                            }
  expose(:payment_conditions) { PaymentCondition.all                                         }
  expose(:comments)           { transporter.comments                                         }
  expose(:extra_services)     { ExtraService.all                                             }
  expose(:positive_ratings)   { transporter.positive_ratings                                 }
  expose(:neutral_ratings)    { transporter.neutral_ratings                                  }
  expose(:negative_ratings)   { transporter.negative_ratings                                 }
  expose(:coverage_areas)     { transporter.coverage_areas                                   }
  expose(:vehicles)           { transporter.vehicles                                         }

  # GET /transporters/new
  # GET /transporters/new.json
  def new
    company.image ||= company.build_image
  end

  # GET /transporters/1/edit
  def edit
    company.image ||= company.build_image
  end

  # POST /transporters
  # POST /transporters.json
  def create
    user      = User.new(params[:user])
    user.role = transporter
    user.add_role :transporter
    transporter.user = user

    if user.save && transporter.save
      sign_in(user.class, user)
      redirect_to edit_transporter_url(transporter), flash: {success: 'El transportista fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /transporters/1
  # PUT /transporters/1.json
  def update
    # Refactor!
    current_password = params[:user].delete(:current_password)
    update_method = if current_password == "undefined"
      :update_attributes
    else
      params[:user][:current_password] = current_password
      :update_with_password
    end

    if user.send(update_method, params[:user]) && transporter.update_attributes(params[:transporter])
      user.must_change_password = false
      user.save validate: false
      sign_in(user.class, user, bypass: true)
      redirect_to edit_transporter_url(transporter), flash: {success: 'El transportista fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /transporters/1
  # DELETE /transporters/1.json
  def destroy
    transporter.destroy
    redirect_to transporters_url
  end

  def change_password
  end
end
