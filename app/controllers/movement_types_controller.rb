class MovementTypesController < ApplicationController
  # GET /movement_types
  # GET /movement_types.json
  def index
    @movement_types = MovementType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @movement_types }
    end
  end

  # GET /movement_types/1
  # GET /movement_types/1.json
  def show
    @movement_type = MovementType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @movement_type }
    end
  end

  # GET /movement_types/new
  # GET /movement_types/new.json
  def new
    @movement_type = MovementType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @movement_type }
    end
  end

  # GET /movement_types/1/edit
  def edit
    @movement_type = MovementType.find(params[:id])
  end

  # POST /movement_types
  # POST /movement_types.json
  def create
    @movement_type = MovementType.new(params[:movement_type])

    respond_to do |format|
      if @movement_type.save
        format.html { redirect_to @movement_type, notice: 'Movement type was successfully created.' }
        format.json { render json: @movement_type, status: :created, location: @movement_type }
      else
        format.html { render action: "new" }
        format.json { render json: @movement_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /movement_types/1
  # PUT /movement_types/1.json
  def update
    @movement_type = MovementType.find(params[:id])

    respond_to do |format|
      if @movement_type.update_attributes(params[:movement_type])
        format.html { redirect_to @movement_type, notice: 'Movement type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @movement_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movement_types/1
  # DELETE /movement_types/1.json
  def destroy
    @movement_type = MovementType.find(params[:id])
    @movement_type.destroy

    respond_to do |format|
      format.html { redirect_to movement_types_url }
      format.json { head :no_content }
    end
  end
end
