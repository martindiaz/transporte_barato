class MeliController < ApplicationController
  def show
    client = Meli.new
    render json: client.item(params[:id])
  end
end
