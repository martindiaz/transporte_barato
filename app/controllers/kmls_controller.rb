class KmlsController < ApplicationController
  # GET /kmls/1
  # GET /kmls/1.json
  def show
    @kml = Kml.find(params[:id])

    respond_to do |format|
      format.kml { render text: @kml.body }
    end
  end

end
