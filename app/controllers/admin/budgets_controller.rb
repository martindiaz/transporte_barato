class Admin::BudgetsController < AdminController

  expose(:budgets) {
    budgets = Budget.page(params[:page]).includes(:transporter => :user)

    if params[:budget]
      budgets = budgets.where('budgets.price = ?', params[:budget][:price]) unless params[:budget][:price].blank?
      budgets = budgets.where('budgets.status = ?', params[:budget][:status]) unless params[:budget][:status].blank?
    end

    if params[:user]
      budgets = budgets.where('users.username ILIKE ?', params[:user][:username]) unless params[:user][:username].blank?
    end

    if params[:dates]
      budgets = budgets.where('date(budgets.created_at) >= to_date(?, \'DD/MM/YYYY\')', params[:dates][:start]) unless params[:dates][:start].blank?
      budgets = budgets.where('date(budgets.created_at) <= to_date(?, \'DD/MM/YYYY\')', params[:dates][:end]) unless params[:dates][:end].blank?
    end

    budgets
  }

  def index
    if params[:commit] == "Exportar"
      send_data budgets.csv, filename: "export_#{Time.now.to_s}.csv"
    end
  end

end
