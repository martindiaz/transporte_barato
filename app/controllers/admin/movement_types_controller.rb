class Admin::MovementTypesController < AdminController
  expose(:movement_types)
  expose(:movement_type)

  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/new
  # GET /admin/extra_services/new.json
  def new
  end

  # GET /admin/extra_services/1/edit
  def edit
  end

  # POST /admin/extra_services
  # POST /admin/extra_services.json
  def create
    if movement_type.save
      redirect_to admin_movement_types_path, flash: {success: 'El tipo de movimiento fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if movement_type.update_attributes(params[:movement_type])
      redirect_to admin_movement_types_path, flash: {success: 'El tipo de movimiento fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    movement_type.destroy
    redirect_to admin_movement_types_path, flash: {success: 'El tipo de movimiento fue eliminado exitosamente.'}
  end
end
