class Admin::GroupsController < AdminController
  expose(:groups)
  expose(:group)

  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/new
  # GET /admin/extra_services/new.json
  def new
  end

  # GET /admin/extra_services/1/edit
  def edit
  end

  # POST /admin/extra_services
  # POST /admin/extra_services.json
  def create
    if group.save
      redirect_to admin_groups_path, flash: {success: 'El grupo fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if group.update_attributes(params[:group])
      redirect_to admin_groups_path, flash: {success: 'El grupo fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    group.destroy
    redirect_to admin_groups_path, flash: {success: 'El grupo fue eliminado exitosamente.'}
  end
end
