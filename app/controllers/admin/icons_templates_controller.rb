class Admin::IconsTemplatesController < AdminController
  expose(:icons_templates)
  expose(:icons_template)
  
  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/new
  # GET /admin/extra_services/new.json
  def new
  end

  # GET /admin/extra_services/1/edit
  def edit
  end

  # POST /admin/extra_services
  # POST /admin/extra_services.json
  def create
    if icons_template.save
      redirect_to admin_icons_templates_path, flash: {success: 'El Icono de Plantilla fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if icons_template.update_attributes(params[:icons_template])
      redirect_to admin_icons_templates_path, flash: {success: 'El icono de la plantilla fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    icons_template.destroy
    redirect_to admin_icons_templates_path, flash: {success: 'El icono de la plantilla fue eliminado exitosamente.'}
  end
end
