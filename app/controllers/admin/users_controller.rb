class Admin::UsersController < AdminController
  expose(:clients) {
    clients = Client.includes(:user).page(params[:page])

    if params[:user]
      clients = clients.where('users.email ILIKE ?', "%#{params[:user][:email]}%") unless params[:user][:email].blank?
      clients = clients.where('users.name ILIKE ?', "%#{params[:user][:name]}%") unless params[:user][:name].blank?
      clients = clients.where('users.surname ILIKE ?', "%#{params[:user][:surname]}%") unless params[:user][:surname].blank?
    end

    if params[:dates]
      clients = clients.where('date(users.created_at) >= to_date(?, \'DD/MM/YYYY\')', params[:dates][:start]) unless params[:dates][:start].blank?
      clients = clients.where('date(users.created_at) <= to_date(?, \'DD/MM/YYYY\')', params[:dates][:end]) unless params[:dates][:end].blank?
    end

    clients
  }
  expose(:client)

  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
    if params[:commit] == "Exportar"
      send_data clients.csv, filename: "export_#{Time.now.to_s}.csv"
    end
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/1/edit
  def edit
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if client.update_attributes(params[:client])
      redirect_to admin_users_path, flash: {success: 'El usuario fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    client.destroy
    redirect_to admin_users_path, flash: {success: 'El usuario fue eliminado exitosamente.'}
  end
end
