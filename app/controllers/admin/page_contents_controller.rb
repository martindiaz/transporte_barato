class Admin::PageContentsController < AdminController

  expose(:page_contents)
  expose(:page_content)

  # GET /admin/page_contents
  # GET /admin/page_contents.json
  def index
  end

  # GET /admin/page_contents/1
  # GET /admin/page_contents/1.json
  def show
  end

  # GET /admin/page_contents/new
  # GET /admin/page_contents/new.json
  def new
  end

  # GET /admin/page_contents/1/edit
  def edit
  end

  # POST /admin/page_contents
  # POST /admin/page_contents.json
  def create
    if page_content.save
      redirect_to admin_page_contents_path, flash: {success: 'El contenido fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /admin/page_contents/1
  # PUT /admin/page_contents/1.json
  def update
    if page_content.update_attributes(params[:page_content])
      redirect_to admin_page_contents_path, flash: {success: 'El contenido fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /admin/page_contents/1
  # DELETE /admin/page_contents/1.json
  def destroy
    page_content.destroy
    redirect_to admin_page_contents_path, flash: {success: 'El contenido fue eliminado exitosamente.'}
  end

end
