class Admin::DemandsController < AdminController

  expose(:demands){
    #TODO implement ransack!!!

    demands = Demand.where(status: ["ACTIVE", "ACCOMPLISHED", "DISCARDED", "EXPIRED"]).page(params[:page])
    demands = demands.includes(:category)

    if params[:demand]
      demands = demands.where('demands.description ILIKE ?', "%#{params[:demand][:description]}%") unless params[:demand][:description].blank?
      demands = demands.where('demands.status = ?', params[:demand][:status]) unless params[:demand][:status].blank?
      demands = demands.where('categories.ancestry = ?', params[:demand][:category_id]) unless params[:demand][:category_id].blank?
    end

    if params[:dates]
      demands = demands.where('date(demands.created_at) >= to_date(?, \'DD/MM/YYYY\')', params[:dates][:start]) unless params[:dates][:start].blank?
      demands = demands.where('date(demands.created_at) <= to_date(?, \'DD/MM/YYYY\')', params[:dates][:end]) unless params[:dates][:end].blank?
    end

    demands

  }

  def index
    if params[:commit] == "Exportar"
      send_data demands.csv, filename: "export_#{Time.now.to_s}.csv"
    end
  end

end
