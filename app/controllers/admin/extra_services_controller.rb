class Admin::ExtraServicesController < AdminController
  expose(:extra_services)
  expose(:extra_service)

  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/new
  # GET /admin/extra_services/new.json
  def new
  end

  # GET /admin/extra_services/1/edit
  def edit
  end

  # POST /admin/extra_services
  # POST /admin/extra_services.json
  def create
    if extra_service.save
      redirect_to admin_extra_services_path, flash: {success: 'El servicio extra fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if extra_service.update_attributes(params[:extra_service])
      redirect_to admin_extra_services_path, flash: {success: 'El servicio extra fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    extra_service.destroy
    redirect_to admin_extra_services_path, flash: {success: 'El servicio extra fue eliminado exitosamente.'}
  end
end
