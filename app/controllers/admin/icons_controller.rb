class Admin::IconsController < AdminController
  expose(:icons)
  expose(:icon)

  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/new
  # GET /admin/extra_services/new.json
  def new
    icon.build_image
  end

  # GET /admin/extra_services/1/edit
  def edit
    icon.build_image unless icon.image
  end

  # POST /admin/extra_services
  # POST /admin/extra_services.json
  def create
    if icon.save
      redirect_to admin_icons_path, flash: {success: 'El icono fue creado exitosamente.'}
    else
      icon.build_image unless icon.image
      render :new
    end
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if icon.update_attributes(params[:icon])
      redirect_to admin_icons_path, flash: {success: 'El icono fue actualizado exitosamente.'}
    else
      icon.build_image unless icon.image
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    icon.destroy
    redirect_to admin_icons_path, flash: {success: 'El icono fue eliminado exitosamente.'}
  end
end
