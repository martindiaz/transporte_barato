class Admin::TransportersController < AdminController
  def index
    @transporters = Transporter.includes(:user, :company).order('users.email').page(params[:page])

    if params[:company]
      @transporters = @transporters.where('companies.name ILIKE ?', "%#{params[:company][:name]}%")unless params[:company][:name].blank?
      @transporters = @transporters.where('companies.phone_number ILIKE ?', "%#{params[:company][:phone_number]}%")unless params[:company][:phone_number].blank?
    end

    if params[:user]
      @transporters = @transporters.where('users.email ILIKE ?', "%#{params[:user][:email]}%")unless params[:user][:email].blank?
    end

    if params[:dates]
      @transporters = @transporters.where('date(transporters.created_at) >= to_date(?, \'DD/MM/YYYY\')', params[:dates][:start]) unless params[:dates][:start].blank?
      @transporters = @transporters.where('date(transporters.created_at) <= to_date(?, \'DD/MM/YYYY\')', params[:dates][:end]) unless params[:dates][:end].blank?
    end

    respond_to do |format|
      format.html {
        if params[:commit] == "Exportar"
          send_data @transporters.csv, filename: "export_#{Time.now.to_s}.csv"
        end
      }
      format.json {
        render json: @transporters.to_json(only: :id, include: {
          company: { only: :name } }) }
    end
  end

  def show
    session[:admin_id] = current_user.id
    sign_in_and_redirect Transporter.find(params[:id]).user, :event => :authentication
  end
end
