class Admin::DashboardController < AdminController
  before_filter :is_admin?

  def index
    @number_of_users = Client.count
    @number_of_transporters = Transporter.count
    @number_of_demands = Demand.where(status: ["ACTIVE", "ACCOMPLISHED", "DISCARDED", "EXPIRED"]).count
    @number_of_budgets = Budget.count
  end

  private

  def is_admin?
    redirect_to root_url unless current_user.has_role? :admin
  end
end
