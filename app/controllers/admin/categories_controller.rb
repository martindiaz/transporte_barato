class Admin::CategoriesController < AdminController
  expose(:categories)
  expose(:category)

  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/new
  # GET /admin/extra_services/new.json
  def new
    category.build_image
  end

  # GET /admin/extra_services/1/edit
  def edit
    category.build_image unless category.build_image
  end

  # POST /admin/extra_services
  # POST /admin/extra_services.json
  def create
    if category.save
      redirect_to admin_categories_path, flash: {success: 'El categoria fue creado exitosamente.'}
    else
      category.build_image unless category.build_image
      render :new
    end
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if category.update_attributes(params[:category])
      redirect_to admin_categories_path, flash: {success: 'El categoria fue actualizado exitosamente.'}
    else
      category.build_image unless category.build_image
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    category.destroy
    redirect_to admin_categories_path, flash: {success: 'El categoria fue eliminado exitosamente.'}
  end
end
