class Admin::VehicleCaracteristicsController < AdminController
  expose(:vehicle_caracteristic)
  expose(:vehicle_caracteristics)

  def index
  end

  def show
  end

  def edit
  end

  def new
  end

  def create
    if vehicle_caracteristic.save
      redirect_to admin_vehicle_caracteristics_path, flash: {success: 'La caracteristica de vehiculo fue creada exitosamente.'}
    else
      render :new
    end
  end

  def update
    if vehicle_caracteristic.update_attributes(params[:vehicle_caracteristic])
      redirect_to admin_vehicle_caracteristics_path, flash: {success: 'La caracteristica de vehiculo fue actualizada exitosamente.'}
    else
      render :edit
    end
  end

  def destroy
    vehicle_caracteristic.destroy
    redirect_to admin_vehicle_caracteristics_path, flash: {success: 'La caracteristica de vehiculo fue eliminada exitosamente.'}
  end
end
