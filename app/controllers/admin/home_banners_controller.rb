class Admin::HomeBannersController < AdminController

  expose(:transporters)

  # GET /home_banners
  # GET /home_banners.json
  def index
    @home_banners = HomeBanner.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @home_banners }
    end
  end

  # GET /home_banners/1
  # GET /home_banners/1.json
  def show
    @home_banner = HomeBanner.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @home_banner }
    end
  end

  # GET /home_banners/new
  # GET /home_banners/new.json
  def new
    @home_banner = HomeBanner.new
    @home_banner.build_image

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @home_banner }
    end
  end

  # GET /home_banners/1/edit
  def edit
    @home_banner = HomeBanner.find(params[:id])
    @home_banner.build_image unless @home_banner.image 
  end

  # POST /home_banners
  # POST /home_banners.json
  def create
    @home_banner = HomeBanner.new(params[:home_banner])

    respond_to do |format|
      if @home_banner.image && @home_banner.save
        format.html { redirect_to admin_home_banners_url, notice: 'Home banner was successfully created.' }
        format.json { render json: @home_banner, status: :created, location: @home_banner }
      else
        format.html { render action: "new" }
        format.json { render json: @home_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /home_banners/1
  # PUT /home_banners/1.json
  def update
    @home_banner = HomeBanner.find(params[:id])

    respond_to do |format|
      if @home_banner.update_attributes(params[:home_banner])
        format.html { redirect_to admin_home_banners_url, notice: 'Home banner was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @home_banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /home_banners/1
  # DELETE /home_banners/1.json
  def destroy
    @home_banner = HomeBanner.find(params[:id])
    @home_banner.destroy

    respond_to do |format|
      format.html { redirect_to admin_home_banners_url }
      format.json { head :no_content }
    end
  end
end
