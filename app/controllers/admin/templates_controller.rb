class Admin::TemplatesController < AdminController
  expose(:templates)
  expose(:template)

  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/new
  # GET /admin/extra_services/new.json
  def new
  end

  # GET /admin/extra_services/1/edit
  def edit
  end

  # POST /admin/extra_services
  # POST /admin/extra_services.json
  def create
    if template.save
      redirect_to admin_templates_path, flash: {success: 'El template fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if template.update_attributes(params[:template])
      redirect_to admin_templates_path, flash: {success: 'El template fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    template.destroy
    redirect_to admin_templates_path, flash: {success: 'El template  fue eliminado exitosamente.'}
  end
end
