class Admin::PaymentConditionsController < AdminController
  expose(:payment_conditions)
  expose(:payment_condition)

  # GET /admin/extra_services
  # GET /admin/extra_services.json
  def index
  end

  # GET /admin/extra_services/1
  # GET /admin/extra_services/1.json
  def show
  end

  # GET /admin/extra_services/new
  # GET /admin/extra_services/new.json
  def new
  end

  # GET /admin/extra_services/1/edit
  def edit
  end

  # POST /admin/extra_services
  # POST /admin/extra_services.json
  def create
    if payment_condition.save
      redirect_to admin_payment_conditions_path, flash: {success: 'El servicio extra fue creado exitosamente.'}
    else
      render :new
    end
  end

  # PUT /admin/extra_services/1
  # PUT /admin/extra_services/1.json
  def update
    if payment_condition.update_attributes(params[:payment_condition])
      redirect_to admin_payment_conditions_path, flash: {success: 'El servicio extra fue actualizado exitosamente.'}
    else
      render :edit
    end
  end

  # DELETE /admin/extra_services/1
  # DELETE /admin/extra_services/1.json
  def destroy
    payment_condition.destroy
    redirect_to admin_payment_conditions_path, flash: {success: 'El servicio extra fue eliminado exitosamente.'}
  end
end
