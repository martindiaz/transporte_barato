class CoverageAreasController < ApplicationController
  expose(:transporter)    { current_user.role }
  expose(:coverage_areas) { transporter.coverage_areas }
  expose(:categories)     { Category.roots }
  expose(:company)        { transporter.company }
  expose(:provinces_kml)  { Kml.provinces }
  expose(:ba_cities_kml)  { Kml.ba_cities }
  expose(:gba_zones_kml)  { Kml.gba_zones }

  expose(:coverage_area) do
    if params[:id]
      transporter.coverage_areas.find(params[:id])
    elsif flash[:coverage_area]
      flash[:coverage_area]
    else
      transporter.coverage_areas.build
    end
  end

  def create
    coverage_area = current_user.role.coverage_areas.build(params[:coverage_area])

    if coverage_area.save
      redirect_to edit_transporter_url(transporter),
        flash: {success: 'Ámbito de cobertura creado exitosamente'}
    else
      redirect_to new_transporter_coverage_area_url,
        flash: { error: 'El ámbito de cobertura no fue creado',
          coverage_area_errors: coverage_area.errors,
          coverage_area: coverage_area }
    end
  end

  def update
    params[:coverage_area][:category_ids] ||= [] if params[:coverage_area]
    coverage_area.addresses.each { |a| a.destroy } if params[:coverage_area][:addresses_attributes]

    if coverage_area.update_attributes(params[:coverage_area])
      redirect_to edit_transporter_url(transporter),
        flash: {success: 'Ámbito de cobertura modificado exitosamente'}
    else
      redirect_to edit_transporter_coverage_area_url(coverage_area),
        flash: {error: 'El ámbito de cobertura no fue actualizado'}
    end
  end

  def destroy
    coverage_area.destroy
    redirect_to transporter_coverage_areas_url(current_user.role), notice: 'Ambito de cobertura eliminado correctamente'
  end

end
