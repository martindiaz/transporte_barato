class ImagesController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:create]

  layout false

  def index
    @images = Image.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @images.map{|image| image.to_jq_upload } }
    end
  end

  def create
    if params[:url]
      @image = Image.new
      @image.remote_image_url = params[:url]
      @image.imageable = Demand.find(params[:demand_id])
      @image.save
    else
      params[:image] ||= params[:image]
      @image = Image.create(params[:image])
    end

  end

end
