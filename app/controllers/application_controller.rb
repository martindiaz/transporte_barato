class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :authenticate_user!
  before_filter :must_change_password?
  before_filter :user_confirmed?

  expose(:analytics) { PageContent.where(name: "Google Analytics").first }

  rescue_from CanCan::AccessDenied do |e|
    redirect_to main_app.root_url, flash: {alert: e.message}
  end

  protected

  def edit_current_user_url
    if params[:action] == "create" && params[:controller] == "registrations" # HACK
      (current_user.client?) ? edit_client_url(current_user.role) : edit_transporter_url(current_user.role)
    else
      (current_user.client?) ? demands_url : transporter_budgets_url(current_user.role)
    end
  end

  def current_user_logged_in?
    !!current_user
  end

  def assign_user_to_demand
    if current_user && session[:demand_id]
      if current_user.client?
        demand = Demand.find session[:demand_id]

        unless demand.client
          demand.client = current_user.role

          demand.save
        end
      else
        flash[:notice] = "Ha ingresado como transportista el envio no pudo crearse"
      end
    end

    session.delete(:demand_id)
  end

  private

  def must_change_password?
    if current_user && current_user.must_change_password? &&
      (params[:action] != "change_password" &&
        !(params[:action] == "destroy" && params[:controller] == "devise/sessions") &&
          !(params[:action] == "update" &&
            (params[:controller] == "clients") || params[:controller] == "transporters"))
      if current_user.client?
        redirect_to client_change_password_url(current_user.role)
      elsif current_user.transporter?
        redirect_to transporter_change_password_url(current_user.role)
      end
    end
  end

  def user_confirmed?
    flash[:warning] = 'Debe confirmar su correo electronico.' if current_user and !current_user.confirmed?
  end

  def is_client?
    redirect_to root_url unless current_user.client?
  end

  def after_sign_in_path_for(resource)
    url = current_user.has_role?(:admin) ? :admin_root_url : :edit_current_user_url
    request.env['omniauth.origin'] || stored_location_for(resource) || send(url)
  end

end
