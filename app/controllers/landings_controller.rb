class LandingsController < ApplicationController

  include Wicked::Wizard
  include Provider

  layout 'landing'

  skip_before_filter :authenticate_user!

  steps :home, :places, :dates, :thanks

  expose(:provider)   { Provider::LANDING }
  expose(:demand) do
    if step == :places
      Demand.new
    elsif session[:demand_id]
      Demand.find(session[:demand_id])
    else
      Demand.new
    end
  end

  def index
    if params[:step] == "places" || params[:step] == "dates"
      jump_to params[:step].to_sym
    else
      jump_to :home
    end

    render_wizard
  end

  def show
    @category = demand.category

    case step
    when :places
      demand.origin ||= Address.new
      demand.destiny ||= Address.new
    when :dates
      demand.origin_information ||= LocationInformation.new
      demand.destiny_information ||= LocationInformation.new
    when :thanks
      email = demand.email
      NotificationMailer.account_activation(email).deliver
    end

    render_wizard
  end


  def update
    case step
    when :places
      demand.status = Status::Demand::SECOND_STEP
      demand.origin_information ||= LocationInformation.new
      demand.destiny_information ||= LocationInformation.new
    when :dates
      demand.status = Status::Demand::THIRD_STEP
      demand.budget_hosting_expiration_date = Date.today + 15.days
      demand.expiration_date = Date.today + 30.days
    end

    if demand.update_attributes(params[:demand])
      render_wizard demand
    else
      render_wizard
    end
  end

end
