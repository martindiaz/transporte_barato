class Landings::TransportesController < LandingsController

  expose(:category) { Category.where(name: 'Fletes').first  }

  def places
    session.delete :demand_id

    demand.status = Status::Demand::FIRST_STEP
    demand.origin ||= Address.new
    demand.destiny ||= Address.new
  end

  def dates
    demand.status = Status::Demand::SECOND_STEP
    demand.origin_information ||= LocationInformation.new
    demand.destiny_information ||= LocationInformation.new
  end

  def thanks
    email = demand.email
    NotificationMailer.account_activation(email).deliver
  end

end
