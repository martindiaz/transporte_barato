class NotificationsController < ApplicationController

  expose(:notification)
  expose(:notifications) { current_user.notifications.order("created_at DESC") }

  expose(:all_notifications) do
    all = notifications.reject { |n| n.deleted? }
    if params[:tab] == "all"
      Kaminari.paginate_array(all).page(params[:page]).per(10)
    else
      Kaminari.paginate_array(all).page(1).per(10)
    end
  end
  expose(:not_read_notifications) do
    not_read = notifications.unread.reject { |n| n.deleted? }
    if params[:tab] == "not_read"
      Kaminari.paginate_array(not_read).page(params[:page]).per(10)
    else
      Kaminari.paginate_array(not_read).page(1).per(10)
    end
  end
  expose(:prominent_notifications) do
    prominent = notifications.prominent.reject { |n| n.deleted? }
    if params[:tab] == "prominent"
      Kaminari.paginate_array(prominent).page(params[:page]).per(10)
    else
      Kaminari.paginate_array(prominent).page(1).per(10)
    end
  end
  expose(:deleted_notifications) do
    if params[:tab] == "deleted"
      notifications.deleted.page(params[:page]).per(10)
    else
      notifications.deleted.page(1).per(10)
    end
  end

  expose(:next_notification) do
    if notification.unread?
      index = not_read_notifications.index(notification) + 1
      not_read_notifications[index]
    elsif notification.prominent?
      index = prominent_notifications.index(notification) + 1
      prominent_notifications[index]
    elsif notification.deleted?
      index = deleted_notifications.index(notification) + 1
      deleted_notifications[index]
    elsif notification.read?
      index = all_notifications.index(notification) + 1
      all_notifications[index]
    end
  end

  expose(:last_notification) do
    if notification.unread?
      index = not_read_notifications.index(notification) - 1
      not_read_notifications[index]
    elsif notification.prominent?
      index = prominent_notifications.index(notification) - 1
      prominent_notifications[index]
    elsif notification.deleted?
      index = deleted_notifications.index(notification) - 1
      deleted_notifications[index]
    elsif notification.read?
      index = all_notifications.index(notification) - 1
      all_notifications[index]
    end
  end

  def show
    notification.read if notification.unread?
  end

  def highlight
    notification.prominent = params[:prominent]
    notification.save
    render nothing: true
  end

  def delete
    unless notification.deleted?
      notification.deleted
      render nothing: true
    else
      notification.destroy
      render partial: "deleted"
    end
  end

  def all
    render partial: "all", url: { page: params[:page], tab: "all" }
  end

  def prominent
    render partial: "prominent", url: { page: params[:page], tab: "prominent" }
  end

  def deleted
    render partial: "deleted", url: { page: params[:page], tab: "deleted" }
  end

  def unread
    render partial: "unread", url: { page: params[:page], tab: "not_read" }
  end
end
