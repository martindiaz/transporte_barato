UTIL = {
  exec: function( controller, action ) {
    var ns = TB,
    action = ( action === undefined ) ? "init" : action;

    if ( controller !== "" && ns[controller] && typeof ns[controller][action] == "function" ) {
      ns[controller][action]();
    }
  },

  init: function() {
    var body = document.body,
    controller = body.getAttribute( "data-controller" ),
    action = body.getAttribute( "data-action" );

    TB.init();
    UTIL.exec( controller );
    UTIL.exec( controller, action );
  }
};

$( document ).ready( UTIL.init );
