// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs

//= require jquery.ui.core
//= require jquery.ui.widget
//= require jquery.ui.mouse
//= require jquery.ui.position

//= require jquery.ui.draggable
//= require jquery.ui.droppable
//= require jquery.ui.resizable
//= require jquery.ui.selectable
//= require jquery.ui.sortable

//= require jquery.ui.accordion
//= require jquery.ui.autocomplete
//= require jquery.ui.button
//= require jquery.ui.datepicker
//= require jquery.ui.datepicker-es
//= require jquery.ui.dialog
//= require jquery.ui.menu
//= require jquery.ui.progressbar
//= require jquery.ui.slider
//= require jquery.ui.spinner
//= require jquery.ui.tabs
//= require jquery.ui.tooltip
//= require jquery.colorbox

//= require jquery-hover-effect

//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl

//= require underscore

//= require markup_based_js_execution
//= require init.js
//= require_tree ./polygons
//= require_tree ./coverage_areas
//= require_tree ./demands
//= require_tree ./clients
//= require_tree ./home
//= require_tree ./pages
//= require_tree ./ratings
//= require_tree ./notifications
//= require_tree ./budgets
//= require_tree ./transporters
//= require_tree ./vehicles
//= require_tree ./favorites
//= require_tree ./reports
//= require_tree ./admin/home_banners
//= require_tree ./demand_steps
//= require_tree ./fletes
//= require_tree ./mudanzas
//= require_tree ./transportes
//= require_tree .
//= require twitter/bootstrap

