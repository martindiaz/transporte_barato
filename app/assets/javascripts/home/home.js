TB.home.index = function () {
  $('.current').removeClass('current');
  $('#home-link').addClass('current');
  $('.carousel').carousel();

  $('#categories').change(function() {
    $("#demand_category").find('option').remove().end();
    category_id = this.selectedOptions[0].value;

    $.getJSON("categories/" + category_id + "/children", function(data) {
      $.each(data, function(key, val) {
        $("#demand_category")
          .append('<option value="'+val.id+'">'+val.name+'</option>');
      });
    });
  });

  // Prevent the enter key submit the form
  $('#demand_origin_attributes_street_line2, #demand_destiny_attributes_street_line2').keydown(function(e) {
    if (e.which == 13) {
      e.preventDefault();
      $(this).focus();
    }
  });
  var origin_locality = document.getElementById('demand_origin_attributes_street_line2');
  var destiny_locality = document.getElementById('demand_destiny_attributes_street_line2');

  var options = {
    types: ['geocode'],
    componentRestrictions: {country: 'ar'}
  };

  //Creates the autocomplete objects
  origin_locality_autocomplete = new google.maps.places.Autocomplete(origin_locality, options);
  destiny_locality_autocomplete = new google.maps.places.Autocomplete(destiny_locality, options);

  //Added event listener to the autocmpletes
  google.maps.event.addListener(origin_locality_autocomplete, 'place_changed', function() {
    extract_address_values(this.getPlace(), 'origin');
  });

  google.maps.event.addListener(destiny_locality_autocomplete, 'place_changed', function() {
    extract_address_values(this.getPlace(), 'destiny');
  });
};

function extract_address_values(place, direction)
{
  //Clean fields
  $("#demand_" + direction + "_attributes_neighborhood").val('');
  $("#demand_" + direction + "_attributes_street_line1").val('');
  $("#demand_" + direction + "_attributes_city").val('');
  $("#demand_" + direction + "_attributes_administrative_area").val('');
  $("#demand_" + direction + "_attributes_state").val('');
  $("#demand_" + direction + "_attributes_country").val('');

  place.address_components.forEach(function(value, index){
    if (value.types.indexOf("neighborhood") != -1)
    {
      //Barrio
      $("#demand_" + direction + "_attributes_neighborhood").val(value.long_name);
    }
    else if (value.types.indexOf("street_number") != -1)
    {
      //Altura
      $("#demand_" + direction + "_attributes_street_line1").val(value.long_name);
    }
    else if (value.types.indexOf("route") != -1)
    {
      //Calle
      $("#demand_" + direction + "_attributes_street_line1").val(value.long_name + ' ' + $("#demand_" + direction + "_attributes_street_line1").val());
    }
    else if (value.types.indexOf("locality") != -1)
    {
      $("#demand_" + direction + "_attributes_city").val(value.long_name);

      if (value.long_name == "Buenos Aires") {
        $("#demand_" + direction + "_attributes_state").val(value.long_name);
        $("#demand_" + direction + "_attributes_administrative_area").val("Capital Federal");
      }
    }
    else if (value.types.indexOf("administrative_area_level_2") != -1)
    {
      $("#demand_" + direction + "_attributes_administrative_area").val(value.long_name);
    }
    else if (value.types.indexOf("administrative_area_level_1") != -1)
    {
      if (value.long_name != "Ciudad Autónoma de Buenos Aires") {
        $("#demand_" + direction + "_attributes_state").val(value.long_name);
      }
    }
    else if (value.types.indexOf("country") != -1)
    {
      //Pais
      $("#demand_" + direction + "_attributes_country").val(value.long_name);
    }

    $("#demand_" + direction + "_attributes_latitude").val(place.geometry.location.lat());
    $("#demand_" + direction + "_attributes_longitude").val(place.geometry.location.lng());
  });
}
