TB.demands.show = function (){
  var destiny_latlng = new google.maps.LatLng(document.getElementById("destiny_lat").value, document.getElementById("destiny_long").value);
  var origin_latlng = new google.maps.LatLng(document.getElementById("origin_lat").value, document.getElementById("origin_long").value);

  var myOptions = {
     zoom: 12,
     center: origin_latlng,
     mapTypeId: google.maps.MapTypeId.ROADMAP
   };

  var map = new google.maps.Map(document.getElementById("address-map"), myOptions);

  var destiny_marker = new google.maps.Marker({
     position: destiny_latlng,
     map: map,
     title:"Destino"
  });

  var origin_marker = new google.maps.Marker({
     position: origin_latlng,
     map: map,
     title:"Origen"
  });

  var request = {
   origin:origin_latlng,
   destination:destiny_latlng,
   travelMode: google.maps.TravelMode.DRIVING
  };

  var directionsService = new google.maps.DirectionsService();
  var directionsDisplay = new google.maps.DirectionsRenderer();

  directionsService.route(request, function(result, status) {
   if (status == google.maps.DirectionsStatus.OK) {
     directionsDisplay.setDirections(result);
     $('.distance')[0].textContent = "Distancia: " + result.routes[0].legs[0].distance.text;
   }
  });

  directionsDisplay.setMap(map);

  $("#budget-form").validationEngine('attach', {promptPosition : "inline", scroll: false});
  $("#demand-comment-form").validationEngine('attach', {scroll: false});
};

TB.demands.index = function () {
  $('.current').removeClass('current');
  $('#demands-link').addClass('current');

  $('.collapse .budget-info.sent').on('show', function () {
    var budget_id = this.id.substr(7, this.id.lenght);
    $.post("/budgets/" + budget_id + "/cash_flow.json", { movement_type: 'BUDGET_VIEWED' } );

    if (this.classList.contains("sent")) {
      $(this).removeClass("sent");
      $.post("/budgets/" + budget_id + "/receive");
    }
  });

};

TB.demands.search = function () {

  $(".search-hidden").each(function(value, index){
    this.value = "";
  });

  var origin  = document.getElementById('origin_location');
  var destiny = document.getElementById('destiny_location');

  //Autocomplete options, restrict the search only for Argentina
  var options = {
    types: ['geocode'],
    componentRestrictions: {country: 'ar'}
  };

  //Creates the autocomplete objects
  origin_autocomplete  = new google.maps.places.Autocomplete(origin, options);
  destiny_autocomplete = new google.maps.places.Autocomplete(destiny, options);

  origin_place  = null;
  destiny_place = null;

  //Added event listener to the autocmpletes
  google.maps.event.addListener(origin_autocomplete, 'place_changed', function() {
    origin_place = this.getPlace();
    origin_place.address_components.forEach(function(value, index){
      if (value.types.indexOf("neighborhood") != -1) {
        $("#search_origin_neighborhood_cont").val(value.long_name);
      }
      else if (value.types.indexOf("administrative_area_level_1") != -1) {
        $("#search_origin_state_cont").val(value.long_name);
      }
      else if (value.types.indexOf("locality") != -1) {
        $("#search_origin_city_cont").val(value.long_name);

        if (value.long_name) {
          $("#search_origin_state_cont").val(value.long_name);
        }
      }
      else if (value.types.indexOf("country") != -1) {
        $("#search_origin_country_cont").val(value.long_name);
      }
    });

    if ($("#search_origin_state_cont").val() == $("#search_origin_city_cont").val()) {
      $("#search_origin_city_cont").val("");
    }
  });

  google.maps.event.addListener(destiny_autocomplete, 'place_changed', function() {
    destiny_place = this.getPlace();
    destiny_place.address_components.forEach(function(value, index){
      if (value.types.indexOf("neighborhood") != -1) {
        $("#search_destiny_neighborhood_cont").val(value.long_name);
      }
      else if (value.types.indexOf("administrative_area_level_1") != -1) {
        $("#search_destiny_state_cont").val(value.long_name);
      }
      else if (value.types.indexOf("locality") != -1) {
        $("#search_destiny_city_cont").val(value.long_name);

        if (value.long_name) {
          $("#search_destiny_state_cont").val(value.long_name);
        }
      }
      else if (value.types.indexOf("country") != -1) {
        $("#search_destiny_country_cont").val(value.long_name);
      }
    });

    if ($("#search_destiny_state_cont").val() == $("#search_destiny_city_cont").val()) {
      $("#search_destiny_city_cont").val("");
    }
  });

  $('.current').removeClass('current');
  $('#demands-link').addClass('current');

  $("#delete-filters").click(function(){
	  localStorage.clear();
    $("input").each(function(index, element){
      if(element.type == "checkbox")
        element.checked = false;
      else if (element.type == "text")
        element.value = "";
    });

    $('form.search-form').submit();
  });

  $("#apply-filters").click(function(){

    var searchOriginValues = "";
    var searchDestinyValues = "";

    $(".search-origin-hidden").each(function(value, index){
       searchOriginValues = searchOriginValues + this.value;
    });

    $(".search-destiny-hidden").each(function(value, index){
       searchDestinyValues = searchDestinyValues + this.value;
    });

    if (searchOriginValues == "") {
      $("#search_origin_country_or_origin_city_or_origin_state_cont").val($("#origin_location").val());
    }

    if (searchDestinyValues == "") {
      $("#search_destiny_country_or_destiny_city_or_destiny_state_cont").val($("#destiny_location").val());
    }

    $('form.search-form').submit();
  });

  $("input[type=text],select,textarea").change(function(){
    localStorage[this.id] = this.value;
  });
  $("input[type=checkbox]").change(function(){
    localStorage[this.id] = this.checked;
  });

  if(localStorage) {
    for (var i = 0; i < localStorage.length; i++){
      elementId = localStorage.key(i);
      value     = localStorage.getItem(elementId);
      element   = $("#" + elementId);

      if(element.attr("type") == "checkbox")
        if(value == "true")
          element.attr("checked", true);
        else
          element.attr("checked", false);
      else if(element.attr("type") == "text")
        element.attr("value", value);
    }
  }
};

function createIconHidden(id, qty) {
  return $('<input>', {
    type  : 'hidden',
    name  : 'demand[icons][' + id + ']',
    id    : 'demand_icons_' + id,
    value : qty
  });
}

function initializeIconEvents() {
  $('.add-icon').on('click', function(evt){
    // Id del icono
    var id = this.parentNode.parentElement.parentNode.parentNode.id;
    var object_id = id.substr(5, id.lenght);

    // QTY of icons
    var qty = parseInt($('#' + id + ' .qty').text()) + 1;
    $('#' + id + ' .qty').text(qty);

    if ($('#demand_icons_' + object_id).size() == 0) {
      $('#icon-form').append(createIconHidden(object_id, qty));
    }
    else {
      $('#demand_icons_' + object_id).val(qty);
    }

    return false;
  });

  $('.remove-icon').on('click', function(evt){
    var id = this.parentNode.parentElement.parentNode.parentNode.id;
    var object_id = id.substr(5, id.lenght);

    actual_value = parseInt($('#' + id + ' .qty').text());

    if (actual_value != 0) {
      $('#' + id + ' .qty').text(parseInt($('#' + id + ' .qty').text()) - 1);
      $('#demand_icons_' + object_id).val($('#' + id + ' .qty').text());
    }

    return false;
  });
}

function extract_address_values(place, direction)
{
  //Clean fields
  $("#demand_" + direction + "_attributes_neighborhood").val('');
  $("#demand_" + direction + "_attributes_street_line1").val('');
  $("#demand_" + direction + "_attributes_city").val('');
  $("#demand_" + direction + "_attributes_administrative_area").val('');
  $("#demand_" + direction + "_attributes_state").val('');
  $("#demand_" + direction + "_attributes_country").val('');

  place.address_components.forEach(function(value, index){
    if (value.types.indexOf("neighborhood") != -1)
    {
      //Barrio
      $("#demand_" + direction + "_attributes_neighborhood").val(value.long_name);
    }
    else if (value.types.indexOf("street_number") != -1)
    {
      //Altura
      $("#demand_" + direction + "_attributes_street_line1").val(value.long_name);
    }
    else if (value.types.indexOf("route") != -1)
    {
      //Calle
      $("#demand_" + direction + "_attributes_street_line1").val(value.long_name + ' ' + $("#demand_" + direction + "_attributes_street_line1").val());
    }
    else if (value.types.indexOf("locality") != -1)
    {
      $("#demand_" + direction + "_attributes_city").val(value.long_name);

      if (value.long_name == "Buenos Aires") {
        $("#demand_" + direction + "_attributes_state").val(value.long_name);
        $("#demand_" + direction + "_attributes_administrative_area").val("Capital Federal");
      }
    }
    else if (value.types.indexOf("administrative_area_level_2") != -1)
    {
      $("#demand_" + direction + "_attributes_administrative_area").val(value.long_name);
    }
    else if (value.types.indexOf("administrative_area_level_1") != -1)
    {
      if (value.long_name != "Ciudad Autónoma de Buenos Aires") {
        $("#demand_" + direction + "_attributes_state").val(value.long_name);
      }
    }
    else if (value.types.indexOf("country") != -1)
    {
      //Pais
      $("#demand_" + direction + "_attributes_country").val(value.long_name);
    }

    $("#demand_" + direction + "_attributes_latitude").val(place.geometry.location.lat());
    $("#demand_" + direction + "_attributes_longitude").val(place.geometry.location.lng());
  });
}


function initializeDemandForm(){
  $('#meli').on('change', function(data){
    $('#meli-box').slideToggle();
  });

  $('#meli-button').on("click", function(){
    $.get('/meli/' + $('#meli_item_id').val())
    .done(function(data){
      $('#demand_description').val(data.title);
      $('#demand_detail').val(data.subtitle);

      $.each(data.pictures, function(index, value){

        $.post( "/images", {url: value.url}, function( data ) {

        });

      });

      $('#meli-button').button('reset');
    });
    $('#meli-button').button('loading');
  });

  $("#demand-form").validationEngine('attach', {promptPosition : "inline", scroll: false});
  $('.current').removeClass('current');
  $('#new-demand-link').addClass('current');

  $('.date-range-select').change(function(data){
    var str = this.options[this.selectedIndex].text;

    idx = 0;
    if(this.id.match(/destiny/))
    {
      idx = 1;
    }

    if(str == "Retirar entre los días")
    {
      $($('.date-to')[idx]).show();
    }
    else
    {
      $($('.date-to')[idx]).hide();
    }
  });

  //Categories
  $('.category-trigger').on("ajax:success", function(evt, data, status, xhr){
    $("#demand-form").validationEngine('attach', {promptPosition : "inline", scroll: false});
    $('.sub-categories').html(data);

    $('.category-trigger.orange').addClass('white');
    $('.category-trigger.orange').removeClass('orange');
    $(this).addClass('orange');
    $(this).removeClass('white');

    if ($('.categories .active a[data-toggle="tab"]').text().trim() == 'Mudanzas'){
      $('#demand_description').val("Mudanza " + $(this).text());
    }
    else{
      $('#demand_description').val('');
    }

    initializeIconEvents();

    //Image Hover
    $('ul.da-thumbs > li').hoverdir();

  });

  $('.category-trigger').bind("ajax:error", function(evt, data, status, xhr){
    alert("Categoria " + this.text + " no implementada");
  });

  // Prevent the enter key submit the form
  $('#demand_origin_attributes_street_line2, #demand_destiny_attributes_street_line2').keydown(function(e) {
    if (e.which == 13) {
      e.preventDefault();
      $(this).focus();
    }
  });

  var mapOptions = {
    //Center on Argentina
    center: new google.maps.LatLng(-37.201728,-59.84107),
    zoom: 4,
    types: 'geocode',
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById('genius-map'), mapOptions);

  var origin_locality = document.getElementById('demand_origin_attributes_street_line2');
  var destiny_locality = document.getElementById('demand_destiny_attributes_street_line2');

  //Autocomplete options, restrict the search only for Argentina
  var options = {
    types: ['geocode'],
    componentRestrictions: {country: 'ar'}
  };

  //Creates the autocomplete objects
  origin_locality_autocomplete = new google.maps.places.Autocomplete(origin_locality, options);
  destiny_locality_autocomplete = new google.maps.places.Autocomplete(destiny_locality, options);

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    map: map
  });

  origin = null;
  destiny = null;

  //Added event listener to the autocmpletes
  google.maps.event.addListener(origin_locality_autocomplete, 'place_changed', function() {
    origin = this.getPlace();
    show_place_in_map(this, origin_locality);
  });

  google.maps.event.addListener(destiny_locality_autocomplete, 'place_changed', function() {
    destiny = this.getPlace();
    show_place_in_map(this, destiny_locality);
  });


  // DO the magic!
  function show_place_in_map(autocomplete, input){
    infowindow.close();
    marker.setVisible(false);

    var place = autocomplete.getPlace();
    if (!place.geometry) {
      // Inform the user that the place was not found and return.
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }

    var image = {
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    };

    marker.setIcon(image);
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    // Calculate the route if we have the origin and destiny
    if ((typeof destiny_latlng != 'undefined' && typeof origin_latlng != 'undefined') || (destiny != null && origin != null))
    {
      var origin_lat;
      var origin_lng;
      var destiny_lat;
      var destiny_lng;

      if (origin != null){
        extract_address_values(origin, 'origin');

        origin_lat = origin.geometry.location.lat();
        origin_lng = origin.geometry.location.lng();
      }
      else{
        origin_lat = origin_latlng.lat();
        origin_lng = origin_latlng.lng();
      }

      if (destiny != null){
        extract_address_values(destiny, 'destiny');
        destiny_lng = destiny.geometry.location.lng();
        destiny_lat = destiny.geometry.location.lat();
      }
      else{
        destiny_lat = destiny_latlng.lat();
        destiny_lng = destiny_latlng.lng();
      }

      calculate_route(origin_lat, origin_lng, destiny_lat, destiny_lng);
    }
  }

  $("#demand_budget_hosting_expiration_date").change(function() {
    date_array = _.map(this.value.split("/"), function(element) {
      return parseInt(element);
    });

    expiration_date = new Date(date_array[2], date_array[1] - 1, date_array[0] + 15)

    $("#expiration_notification").
      text("Su envío caducará el día " + expiration_date.getDate() + "/"
        + (expiration_date.getMonth() + 1) + "/" + expiration_date.getFullYear());
  });
}

TB.demands.new = function () {
  $('#categories').change(function() {
    $("#demand_category").find('option').remove().end();
    category_id = this.selectedOptions[0].value;

    $.getJSON("/categories/" + category_id + "/children", function(data) {
      $.each(data, function(key, val) {
        $("#demand_category")
          .append('<option value="'+val.id+'">'+val.name+'</option>');
      });
    });
  });

  // Prevent the enter key submit the form
  $('#demand_origin_attributes_street_line2, #demand_destiny_attributes_street_line2').keydown(function(e) {
    if (e.which == 13) {
      e.preventDefault();
      $(this).focus();
    }
  });
  var origin_locality = document.getElementById('demand_origin_attributes_street_line2');
  var destiny_locality = document.getElementById('demand_destiny_attributes_street_line2');

  var options = {
    types: ['geocode'],
    componentRestrictions: {country: 'ar'}
  };

  //Creates the autocomplete objects
  origin_locality_autocomplete = new google.maps.places.Autocomplete(origin_locality, options);
  destiny_locality_autocomplete = new google.maps.places.Autocomplete(destiny_locality, options);

  //Added event listener to the autocmpletes
  google.maps.event.addListener(origin_locality_autocomplete, 'place_changed', function() {
    extract_address_values(this.getPlace(), 'origin');
  });

  google.maps.event.addListener(destiny_locality_autocomplete, 'place_changed', function() {
    extract_address_values(this.getPlace(), 'destiny');
  });

};

TB.demands.create = function() {
  //Image Hover
  $('ul.da-thumbs > li').hoverdir();

  $('.tabs-left a[href="#' + $('#parent_category').val() + '"]').tab('show'); // Select tab by name

  $('.category-trigger.orange').addClass('white');
  $('.category-trigger.orange').removeClass('orange');
  $('#category-' + $('#demand_category_id').val() ).addClass('orange');
  $('#category-' + $('#demand_category_id').val() ).removeClass('white');

  initializeDemandForm();

  var origin_lat = $("#demand_origin_attributes_latitude").val();
  var origin_lng = $("#demand_origin_attributes_longitude").val();

  var destiny_lat = $("#demand_destiny_attributes_latitude").val();
  var destiny_lng = $("#demand_destiny_attributes_longitude").val();

  calculate_route(origin_lat, origin_lng, destiny_lat, destiny_lng);

  $('#demand-form-map-container').slideToggle();
};

TB.demands.edit = function() {
  //Image Hover
  $('ul.da-thumbs > li').hoverdir();

  initializeDemandForm();
  initializeIconEvents();

  var origin_lat = $("#demand_origin_attributes_latitude").val();
  var origin_lng = $("#demand_origin_attributes_longitude").val();

  var destiny_lat = $("#demand_destiny_attributes_latitude").val();
  var destiny_lng = $("#demand_destiny_attributes_longitude").val();

  calculate_route(origin_lat, origin_lng, destiny_lat, destiny_lng);
  $('#demand-form-map-container').slideToggle();
};

TB.demands.update = function() {
  TB.demands.edit();
};

function calculate_route(origin_lat, origin_lng, destiny_lat, destiny_lng) {
  // NAVIGATION STUFF
  if (typeof directionsService == 'undefined') {
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);
  }

  destiny_latlng = new google.maps.LatLng(destiny_lat, destiny_lng);
  origin_latlng = new google.maps.LatLng(origin_lat, origin_lng);

  var request = {
    origin: origin_latlng,
    destination: destiny_latlng,
    travelMode: google.maps.TravelMode.DRIVING
  };

  //Calculate the route
  directionsService.route(request, function(result, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(result);

      // Show the distance
      $("#demand_distance").val(result.routes[0].legs[0].distance.text);
      $('.distance')[0].textContent = "Distancia: " + result.routes[0].legs[0].distance.text;
    }
    else {
      alert("No existe una ruta para la dirección especificada");
    }
  });
}
