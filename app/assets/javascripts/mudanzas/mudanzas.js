// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

TB.mudanzas.show = function (){
  initializeDemandForm();
};

function initializeDemandForm(){
  $("#demand-form").validationEngine('attach', {promptPosition : "inline", scroll: false});
  $('.current').removeClass('current');
  $('#new-demand-link').addClass('current');

  $('.date-range-select').change(function(data){
    var str = this.options[this.selectedIndex].text;

    idx = 0;
    if(this.id.match(/destiny/))
    {
      idx = 1;
    }

    if(str == "Retirar entre los días")
    {
      $($('.date-to')[idx]).show();
    }
    else
    {
      $($('.date-to')[idx]).hide();
    }
  });

  // Prevent the enter key submit the form
  $('#demand_origin_attributes_street_line2, #demand_destiny_attributes_street_line2').keydown(function(e) {
    if (e.which == 13) {
      e.preventDefault();
      $(this).focus();
    }
  });

  var origin_locality = document.getElementById('demand_origin_attributes_street_line2');
  var destiny_locality = document.getElementById('demand_destiny_attributes_street_line2');

  var options = {
    types: ['geocode'],
    componentRestrictions: {country: 'ar'}
  };

  //Creates the autocomplete objects
  origin_locality_autocomplete = new google.maps.places.Autocomplete(origin_locality, options);
  destiny_locality_autocomplete = new google.maps.places.Autocomplete(destiny_locality, options);

  //Added event listener to the autocmpletes
  google.maps.event.addListener(origin_locality_autocomplete, 'place_changed', function() {
    extract_address_values(this.getPlace(), 'origin');
  });

  google.maps.event.addListener(destiny_locality_autocomplete, 'place_changed', function() {
    extract_address_values(this.getPlace(), 'destiny');
  });

  $("#demand_budget_hosting_expiration_date").change(function() {
    date_array = _.map(this.value.split("/"), function(element) {
      return parseInt(element);
    });

    expiration_date = new Date(date_array[2], date_array[1] - 1, date_array[0] + 15)

    $("#expiration_notification").
      text("Su envío caducará el día " + expiration_date.getDate() + "/"
        + (expiration_date.getMonth() + 1) + "/" + expiration_date.getFullYear());
  });

  function extract_address_values(place, direction)
  {
    //Clean fields
    $("#demand_" + direction + "_attributes_neighborhood").val('');
    $("#demand_" + direction + "_attributes_street_line1").val('');
    $("#demand_" + direction + "_attributes_city").val('');
    $("#demand_" + direction + "_attributes_administrative_area").val('');
    $("#demand_" + direction + "_attributes_state").val('');
    $("#demand_" + direction + "_attributes_country").val('');

    place.address_components.forEach(function(value, index){
      if (value.types.indexOf("neighborhood") != -1)
      {
        //Barrio
        $("#demand_" + direction + "_attributes_neighborhood").val(value.long_name);
      }
      else if (value.types.indexOf("street_number") != -1)
      {
        //Altura
        $("#demand_" + direction + "_attributes_street_line1").val(value.long_name);
      }
      else if (value.types.indexOf("route") != -1)
      {
        //Calle
        $("#demand_" + direction + "_attributes_street_line1").val(value.long_name + ' ' + $("#demand_" + direction + "_attributes_street_line1").val());
      }
      else if (value.types.indexOf("locality") != -1)
      {
        $("#demand_" + direction + "_attributes_city").val(value.long_name);

        if (value.long_name == "Buenos Aires") {
          $("#demand_" + direction + "_attributes_state").val(value.long_name);
          $("#demand_" + direction + "_attributes_administrative_area").val("Capital Federal");
        }
      }
      else if (value.types.indexOf("administrative_area_level_2") != -1)
      {
        $("#demand_" + direction + "_attributes_administrative_area").val(value.long_name);
      }
      else if (value.types.indexOf("administrative_area_level_1") != -1)
      {
        if (value.long_name != "Ciudad Autónoma de Buenos Aires") {
          $("#demand_" + direction + "_attributes_state").val(value.long_name);
        }
      }
      else if (value.types.indexOf("country") != -1)
      {
        //Pais
        $("#demand_" + direction + "_attributes_country").val(value.long_name);
      }

      $("#demand_" + direction + "_attributes_latitude").val(place.geometry.location.lat());
      $("#demand_" + direction + "_attributes_longitude").val(place.geometry.location.lng());
    });
  }
}

TB.mudanzas.new = function () {
  initializeDemandForm();
};

TB.mudanzas.create = function() {
  initializeDemandForm();
};

TB.mudanzas.edit = function() {
  initializeDemandForm();
};

TB.mudanzas.update = function() {
  TB.mudanzas.edit();
};

TB.mudanzas.places = function() {
  initializeDemandForm();
};

TB.mudanzas.dates = function() {
  initializeDemandForm();
};

