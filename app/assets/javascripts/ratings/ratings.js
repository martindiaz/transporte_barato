TB.ratings.index = function () {
  $('.current').removeClass('current');
  $('#rating-link').addClass('current');

  $('form .rating-face').on('click', function(event){
    $('.rating-face').removeClass('active');
    $(this).addClass('active');
    $($($(this).parent()[0]).siblings()[0]).val($(this).text());
  });
};
