// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function blah() {
  var myLatLng = new google.maps.LatLng(-37.201728,-59.84107);

  var myOptions = {
    zoom: 5,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    editable: true
  };

  var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

  if ($('#points') != null) {
    var paths = [];

    $('#points > input').each(function (index, value) {
      point     = value.value.split(',');
      latitude  = parseFloat(point[0]);
      longitude = parseFloat(point[1]);

      paths[index] = new google.maps.LatLng(latitude, longitude);
    });

    var shape = new google.maps.Polygon({
      paths: paths,
      strokeColor: '#ff0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#ff0000',
      fillOpacity: 0.35
    });
    shape.setMap(map);
  }

  var drawingManager = new google.maps.drawing.DrawingManager({
    wingMode: google.maps.drawing.OverlayType.MARKER,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: [
        google.maps.drawing.OverlayType.POLYGON,
      ]
    },
    circleOptions: {
      fillColor: '#ffff00',
      fillOpacity: 1,
      strokeWeight: 5,
      clickable: false,
      zIndex: 1,
      editable: true
    }
  });

  google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
    if (event.type == google.maps.drawing.OverlayType.POLYGON) {
      latLngs = event.overlay.latLngs.getArray()[0].getArray();

      $.each(latLngs, function(index, value){
        var point     = value.toString().replace(/\(|\)/g, '').replace(/\s+/g, '').split(',');
        var latitude  = point[0];
        var longitude = point[1];
        var $hiddenLatitude  = createPointHidden(latitude, index, 'latitude');
        var $hiddenLongitude = createPointHidden(longitude, index, 'longitude');

        $('form').append($hiddenLatitude);
        $('form').append($hiddenLongitude);
      });
    }
  });

  drawingManager.setMap(map);

  function createPointHidden(coordenate, index, coordenateDescription) {
    return $('<input>', {
      type  : 'hidden',
      name  : 'polygon[points][' + index +']['+ coordenateDescription  +']',
      id    : 'polygon_points',
      value : coordenate
    });
  }
}

TB.polygons.new = function (){
  blah();
};

TB.polygons.edit = function (){
  blah();
};
