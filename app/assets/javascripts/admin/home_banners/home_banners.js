TB.home_banners.new = init;
TB.home_banners.edit = init;

function init() {
  $("#search-transporters").click(function(){
    $("#search-transporters").prop( "disabled", true );

    $.ajax({
      url: "../transporters",
      data: {
        company: {
          name: $("#transporter_company_name").val()
        }
      },
      type: "GET",
      dataType: "json",
      success: function(json) {
        fillTransportersSelect(json);
      },
      complete: $("#search-transporters").prop( "disabled", false )
    });
  });

  function fillTransportersSelect(transporters) {
    $("#home_banner_transporter_id").find('option').remove().end();

    for (var i = transporters.length - 1; i >= 0; i--){
      $("#home_banner_transporter_id")
        .append('<option value="' + transporters[i].id + '">' + transporters[i].company.name + '</option>')
          .val(transporters[i].name);
    };
  }
}
