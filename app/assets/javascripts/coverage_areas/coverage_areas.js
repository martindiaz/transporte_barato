TB.coverage_areas.new = function (){
  initialize(false);
};

TB.coverage_areas.edit = function (){
  initialize(true);
};

TB.coverage_areas.show = function (){
  initialize(true);
};

var initialize = function (parseOnInit){
  var myLatLng = new google.maps.LatLng(-37.201728,-59.84107);

  var myOptions = {
    zoom: 4,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

  var provincesSelected = false;
  var baCitiesSelected = false;
  var gbaZonesSelected = false;

  provincesParser = new geoXML3.parser({
    map: map,
    suppressInfoWindows: true,
    afterParse: interactions
  });

  baCitiesParser = new geoXML3.parser({
    map: map,
    suppressInfoWindows: true,
    afterParse: interactions
  });

  gbaZonesParser = new geoXML3.parser({
    map: map,
    suppressInfoWindows: true,
    afterParse: interactions
  });

  if (parseOnInit) {
    gbaZonesParser.parse(document.getElementById("gba_zones").href);
    gbaZonesSelected = true;
    baCitiesParser.parse(document.getElementById("ba_cities").href);
    baCitiesSelected = true;
    provincesParser.parse(document.getElementById("provinces").href);
    provincesSelected = true;
  }

  $("#provinces").click(function(){
    if (provincesParser.docs.length > 0) {
      if (provincesSelected && baCitiesSelected && gbaZonesSelected) {
        baCitiesParser.hideDocument(baCitiesParser.docs[0]);
        baCitiesSelected = false;
        gbaZonesParser.hideDocument(gbaZonesParser.docs[0]);
        gbaZonesSelected = false;
      } else if (provincesSelected) {
        provincesParser.hideDocument(provincesParser.docs[0]);
        provincesSelected = false;
      } else {
        if (baCitiesSelected) {
          baCitiesParser.hideDocument(baCitiesParser.docs[0]);
          baCitiesSelected = false;
        } else if (gbaZonesSelected) {
          gbaZonesParser.hideDocument(gbaZonesParser.docs[0]);
          gbaZonesSelected = false;
        }
        provincesParser.showDocument(provincesParser.docs[0]);
        provincesSelected = true;
      }
    } else {
      if (baCitiesSelected) {
        baCitiesParser.hideDocument(baCitiesParser.docs[0]);
        baCitiesSelected = false;
      } else if (gbaZonesSelected) {
        gbaZonesParser.hideDocument(gbaZonesParser.docs[0]);
        gbaZonesSelected = false;
      }
      provincesParser.parse(this.href);
      provincesSelected = true;
    }
    this.on = !this.on;
    return false;
  });

  $("#ba_cities").click(function(){
    if (baCitiesParser.docs.length > 0) {
      if (baCitiesSelected && provincesSelected && gbaZonesSelected) {
        provincesParser.hideDocument(provincesParser.docs[0]);
        provincesSelected = false;
        gbaZonesParser.hideDocument(gbaZonesParser.docs[0]);
        gbaZonesSelected = false;
      } else if (baCitiesSelected) {
        baCitiesParser.hideDocument(baCitiesParser.docs[0]);
        baCitiesSelected = false;
      } else {
        if (provincesSelected) {
          provincesParser.hideDocument(provincesParser.docs[0]);
          provincesSelected = false;
        } else if (gbaZonesSelected) {
          gbaZonesParser.hideDocument(gbaZonesParser.docs[0]);
          gbaZonesSelected = false;
        }
        baCitiesParser.showDocument(baCitiesParser.docs[0]);
        baCitiesSelected = true;
      }
    } else {
      if (provincesSelected) {
        provincesParser.hideDocument(provincesParser.docs[0]);
        provincesSelected = false;
      } else if (gbaZonesSelected) {
        gbaZonesParser.hideDocument(gbaZonesParser.docs[0]);
        gbaZonesSelected = false;
      }
      baCitiesParser.parse(this.href);
      baCitiesSelected = true;
    }
    this.on = !this.on;
    return false;
  });

  $("#gba_zones").click(function(){
    if (gbaZonesParser.docs.length > 0) {
      if (baCitiesSelected && provincesSelected && gbaZonesSelected) {
        provincesParser.hideDocument(provincesParser.docs[0]);
        provincesSelected = false;
        baCitiesParser.hideDocument(baCitiesParser.docs[0]);
        baCitiesSelected = false;
      } else if (gbaZonesSelected) {
        gbaZonesParser.hideDocument(gbaZonesParser.docs[0]);
        gbaZonesSelected = false;
      } else {
        if (provincesSelected) {
          provincesParser.hideDocument(provincesParser.docs[0]);
          provincesSelected = false;
        } else if (baCitiesSelected) {
          baCitiesParser.hideDocument(baCitiesParser.docs[0]);
          baCitiesSelected = false;
        }
        gbaZonesParser.showDocument(gbaZonesParser.docs[0]);
        gbaZonesSelected = true;
      }
    } else {
      if (provincesSelected) {
        provincesParser.hideDocument(provincesParser.docs[0]);
        provincesSelected = false;
      } else if (baCitiesSelected) {
        baCitiesParser.hideDocument(baCitiesParser.docs[0]);
        baCitiesSelected = false;
      }
      gbaZonesParser.parse(this.href);
      gbaZonesSelected = true;
    }
    this.on = !this.on;
    return false;
  });


  function interactions(doc) {
    if(doc[0]) {
      $.each(doc[0].gpolygons, function(index, value) {
        google.maps.event.addListener(this, "click", function() {
          if (!this.selected) {
            if (doc[0].styles["estilo-provincia"]) {
              selectProvinceArea(this);
            } else {
              selectBACityArea(this);
            }
          } else {
            unselectArea(this);
          }
        });

        var infowindow = new google.maps.InfoWindow({
          content: this.title,
          position: new google.maps.LatLng(
            this.latLngs.b[0].b[0].lat(), this.latLngs.b[0].b[0].lng())
        });

        google.maps.event.addListener(this, "mouseover", function(event) {
          infowindow.open(this.map);
        });

        google.maps.event.addListener(this, "mouseout", function() {
          infowindow.close();
        });
      });

      $.each($("#preselected-areas").children(), function(index, area){
        $.each(doc[0].gpolygons, function(y, gpolygon) {
          if(area.value == gpolygon.title)
            if (doc[0].styles["estilo-provincia"]) {
              selectProvinceArea(gpolygon);
            } else {
              selectBACityArea(gpolygon);
            }
        });
      });
    }
  }

  function selectProvinceArea(area) {
    area["selected"] = true;
    area.setOptions({fillOpacity: 0.75, strokeColor: "#FFFFFF", zIndex: 1});
    $("#selected-areas").append(createCountryHidden("Argentina"));
    $("#selected-areas").append(createProvinceHidden(area.title));
  }
  
  function selectBACityArea(area) {
    area["selected"] = true;
    area.setOptions({fillOpacity: 0.75, strokeColor: "#FFFFFF", zIndex: 1});
    $("#selected-areas").append(createCountryHidden("Argentina"));
    $("#selected-areas").append(createProvinceHidden("Buenos Aires"));
    $("#selected-areas").append(createCityHidden(area.title));
  }

  function unselectArea(area) {
    area["selected"] = false;
    area.setOptions({fillOpacity: 0.48828125, strokeColor: "#FF0000", zIndex: 1});
    $.each($("#selected-areas").children(), function(index, value){
      if(value.value == area.title){
        previous = $(value).prev();

        if (previous.prev().is("input")) {
          previous.prev().remove();
          previous.remove();
        } else {
          previous.remove();
        }

        $(value).remove();
      }
    });
  }

  function createCountryHidden(country) {
    return $('<input>', {
      type  : 'hidden',
      name  : 'coverage_area[addresses_attributes][][country]',
      value : country
    });
  }

  function createProvinceHidden(province) {
    return $('<input>', {
      type  : 'hidden',
      name  : 'coverage_area[addresses_attributes][][state]',
      value : province
    });
  }
  
  function createCityHidden(administrative_area) {
    return $('<input>', {
      type  : 'hidden',
      name  : 'coverage_area[addresses_attributes][][administrative_area]',
      value : administrative_area
    });
  }
};
