TB.notifications.show = initNotifications;
TB.notifications.index = initNotifications;

function initNotifications() {
  $('.current').removeClass('current');
  $('#notifications-link').addClass('current');

  addClickListenerToStars();

  function addClickListenerToStars() {
    $(".star").click(function() {
      notificationId = $(this).attr("id");

      var page = loadPageVar("page");
      var tab = loadPageVar("tab");

      if($(this).hasClass("star-off")) {
        $(this).removeClass("star-off");
        $(this).addClass("star-on");
        highlightNotification(notificationId, true, page, tab);
      }
      else if ($(this).hasClass("star-on")) {
        $(this).removeClass("star-on");
        $(this).addClass("star-off");
        highlightNotification(notificationId, false, page, tab);
      }
    });
  }

  function highlightNotification(notificationId, status, page, tab) {
    $.ajax({
      url: "/notifications/" + notificationId + "/highlight",
      type: "POST",
      data: {
        prominent: status
      }
    }).done(function(data){
      $(".dynamic-all").load("/notifications/all?tab=" + tab + "&page=" + page, function(){
        $(".dynamic-prominent").load("/notifications/prominent?tab=" + tab + "&page=" + page, function(){
          $(".dynamic-unread").load("/notifications/unread?tab=" + tab + "&page=" + page, function(){
            $(".dynamic-deleted").load("/notifications/deleted?tab=" + tab + "&page=" + page, addClickListenerToStars);
          });
        });
      });
    });
  }

  $("#delete").click(function(){
    if(window.confirm("Esta seguro?")) {
      $("input:checked").each(function(index, element){
        notificationId = element.id;
        deleteNotification(notificationId);
      });
    }
  });

  function deleteNotification(notificationId) {
    var page = loadPageVar("page");
    var tab = loadPageVar("tab");

    $.ajax({
      url: "/notifications/" + notificationId + "/delete",
      type: "POST",
      data: {
        notification: {
          id: notificationId
        }
      }
    }).done(function(data) {
      $(".dynamic-deleted").load("/notifications/deleted?tab=" + tab + "&page=" + page);
      $(".dynamic-prominent").load("/notifications/prominent?tab=" + tab + "&page=" + page);
      $(".dynamic-unread").load("/notifications/unread?tab=" + tab + "&page=" + page);
      $("#" + notificationId).remove();
    });
  }

  function loadPageVar(sVar) {
    return unescape(
      window.location.search.replace(
        new RegExp(
          "^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"
        ),
      "$1")
    );
  }
}
