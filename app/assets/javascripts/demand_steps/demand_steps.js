// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

TB.demand_steps.show = function (){
  initializeDemandForm();
  initializeIconEvents();
  $('ul.da-thumbs > li').hoverdir();
};

function createIconHidden(id, qty) {
  return $('<input>', {
    type  : 'hidden',
    name  : 'demand[icons][' + id + ']',
    id    : 'demand_icons_' + id,
    value : qty
  });
}

function initializeIconEvents() {
  $('.add-icon').on('click', function(evt){
    // Id del icono
    var id = this.parentNode.parentElement.parentNode.parentNode.id;
    var object_id = id.substr(5, id.lenght);

    // QTY of icons
    var qty = parseInt($('#' + id + ' .qty').text()) + 1;
    $('#' + id + ' .qty').text(qty);

    if ($('#demand_icons_' + object_id).size() == 0) {
      $('#icon-form').append(createIconHidden(object_id, qty));
    }
    else {
      $('#demand_icons_' + object_id).val(qty);
    }

    return false;
  });
}

function initializeDemandForm(){
  $('#meli').on('change', function(data){
    $('#meli-box').slideToggle();
  });

  $('#meli-button').on("click", function(){
    $.get('/meli/' + $('#meli_item_id').val())
    .done(function(data){
      $('#demand_description').val(data.title);
      $('#demand_detail').val(data.subtitle);

      $.each(data.pictures, function(index, value){

        $.post( "/images", {url: value.url, demand_id: document.getElementById('demand_id').value }, function( data ) {

        });

      });

      $('#meli-button').button('reset');
    });
    $('#meli-button').button('loading');
  });

  $("#demand-form").validationEngine('attach', {promptPosition : "inline", scroll: false});
  $('.current').removeClass('current');
  $('#new-demand-link').addClass('current');

  $('.date-range-select').change(function(data){
    var str = this.options[this.selectedIndex].text;

    idx = 0;
    if(this.id.match(/destiny/))
    {
      idx = 1;
    }

    if(str == "Retirar entre los días")
    {
      $($('.date-to')[idx]).show();
    }
    else
    {
      $($('.date-to')[idx]).hide();
    }
  });

  // Prevent the enter key submit the form
  $('#demand_origin_attributes_street_line2, #demand_destiny_attributes_street_line2').keydown(function(e) {
    if (e.which == 13) {
      e.preventDefault();
      $(this).focus();
    }
  });

  var origin_locality = document.getElementById('demand_origin_attributes_street_line2');
  var destiny_locality = document.getElementById('demand_destiny_attributes_street_line2');

  var options = {
    types: ['geocode'],
    componentRestrictions: {country: 'ar'}
  };

  //Creates the autocomplete objects
  origin_locality_autocomplete = new google.maps.places.Autocomplete(origin_locality, options);
  destiny_locality_autocomplete = new google.maps.places.Autocomplete(destiny_locality, options);

  $("#demand_budget_hosting_expiration_date").change(function() {
    date_array = _.map(this.value.split("/"), function(element) {
      return parseInt(element);
    });

    expiration_date = new Date(date_array[2], date_array[1] - 1, date_array[0] + 15)

    $("#expiration_notification").
      text("Su envío caducará el día " + expiration_date.getDate() + "/"
        + (expiration_date.getMonth() + 1) + "/" + expiration_date.getFullYear());
  });
}

TB.demand_steps.new = function () {
  initializeDemandForm();
};

TB.demand_steps.create = function() {
  initializeDemandForm();
};

TB.demand_steps.edit = function() {
  initializeDemandForm();
};

TB.demand_steps.update = function() {
  TB.demand_steps.edit();
};

