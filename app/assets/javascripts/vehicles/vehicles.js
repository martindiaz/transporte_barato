TB.vehicles.new = function () {

  $('.new_image').fileupload({
      dataType: 'script'
  });

  $('.vehicle-actions input').on('click', function(event){

    $('#vehicle-form').submit();

    return false;
  });

};

TB.vehicles.edit = function () {

  TB.vehicles.new();

};
