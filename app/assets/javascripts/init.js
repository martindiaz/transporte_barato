TB = new Object();
TB.polygons = new Object;
TB.coverage_areas = new Object;
TB.demands = new Object;
TB.clients = new Object;
TB.home = new Object;
TB.pages = new Object;
TB.ratings = new Object;
TB.notifications = new Object;
TB.budgets = new Object;
TB.transporters = new Object;
TB.credits = new Object;
TB.vehicles = new Object;
TB.favorites = new Object;
TB.reports = new Object;
TB.home_banners = new Object;
TB.demand_steps = new Object;
TB.fletes = new Object;
TB.mudanzas = new Object;
TB.transportes = new Object;


TB.init = function (){
  // Your js code for all pages here
  $("a[rel=popover]").popover();
  $(".tooltip").tooltip();
  $("a[rel=tooltip]").tooltip();

  $( "#datepicker, .datepicker" ).datepicker({
    dateFormat: 'dd/mm/yy',
    showOn: "button",
    buttonImage: "/images/calendar.png",
    buttonImageOnly: true,
    regional: 'es'
  });

  $('.collapse').on('hide', function (event) {
    $('#button-' + this.id).removeClass('active');

    if (this.previousElementSibling.className.search("tb-accordion-budget") > -1) {
      this.previousElementSibling.className = this.previousElementSibling.className.replace(/\btb-budget-active\b/,'');
    }

    event.stopPropagation();
  });

  $('.collapse').on('show', function (event) {
    $('#button-' + this.id).addClass('active');

    if (this.previousElementSibling.className.search("tb-accordion-budget") > -1) {
      if (this.previousElementSibling.className.search("tb-budget-new") > -1) {
        this.previousElementSibling.className = this.previousElementSibling.className.replace(/\btb-budget-new\b/,'');
      }

      if (this.previousElementSibling.className.search("tb-budget-message") > -1) {
        this.previousElementSibling.className = this.previousElementSibling.className.replace(/\btb-budget-message\b/,'');

        $(".tb-accordion-iconbox").remove();
        var comments = this.getElementsByClassName("comment");
        $.each(comments, function(index, value) {
          if (value.id != "") {
            $.ajax({
              url: "/comments/" + value.id + "/read",
              type: "GET"
            });
          }
        });
      }

      this.previousElementSibling.className = this.previousElementSibling.className + ' tb-budget-active';
    }

    event.stopPropagation();
  });

  $('#multiple-select').multiSelect();

  $('.accordion-button').on('click', function(){
    $(this).children().toggleClass('icon-minus');
    $(this).children().toggleClass('icon-plus');
    $($(this).parents()[1]).next().slideToggle();
  });

  $("#registration-form").validationEngine('attach', {promptPosition : "inline", scroll: false});
  $("#sign-in-form").validationEngine('attach', {promptPosition : "inline", scroll: false});

  $('#image-form').fileupload({
      url: '/images',
      type: 'POST',
      dataType: 'script'
  });

  $('.colorbox-gallery').colorbox({rel:'colorbox-gallery'});
};
