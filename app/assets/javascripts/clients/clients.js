TB.clients.edit = function () {
  $('.current').removeClass('current');
  $('#profile-link').addClass('current');

  $("#client-form").validationEngine('attach', {promptPosition : "inline", scroll: false});
};

TB.clients.update = function() {
  TB.clients.edit();
}
