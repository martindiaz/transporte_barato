TB.credits.index = function () {
  $('.current').removeClass('current');
  $('#credits-link').addClass('current');

  $("#billing-address-form, #credits-form").validationEngine('attach', {promptPosition : "inline", scroll: false});
};
