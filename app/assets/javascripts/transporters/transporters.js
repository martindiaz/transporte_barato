TB.transporters.edit = function () {
  $('.current').removeClass('current');
  $('#transporter-profile-link').addClass('current');

  var city  = document.getElementById('address_city');
  var state = document.getElementById('address_state');

  $("#city-hidden").val(city.value);
  $("#state-hidden").val(state.value);

  // Prevent the enter key submit the form
  $('#address_state, #address_city').keydown(function(e) {
    if (e.which == 13) {
      e.preventDefault();
      $(this).focus();
    }
  });

  //Autocomplete options, restrict the search only for Argentina
  var cityOptions = {
    types: ['(cities)'],
    componentRestrictions: {
      country: 'ar'
    }
  };

  //Autocomplete options, restrict the search only for Argentina
  var stateOptions = {
    types: ['(regions)'],
    componentRestrictions: {
      country: 'ar'
    }
  };

  //Creates the autocomplete objects
  cityAutocomplete  = new google.maps.places.Autocomplete(city, cityOptions);
  stateAutocomplete = new google.maps.places.Autocomplete(state, stateOptions);

  cityPlace  = null;
  statePlace = null;

  google.maps.event.addListener(cityAutocomplete, 'place_changed', function() {
    cityPlace = this.getPlace();
    cityPlace.address_components.forEach(function(value, index){
      if ($.inArray("administrative_area_level_2", value.types) == 0) {
        $("#administrative_area-hidden").val(value.long_name);
      }
      if ($.inArray("locality", value.types) == 0) {
        $("#city-hidden").val(value.long_name);
      }
    });
  });

  google.maps.event.addListener(stateAutocomplete, 'place_changed', function() {
    statePlace = this.getPlace();
    statePlace.address_components.forEach(function(value, index){
      if ($.inArray("locality", value.types) == 0) {
        $("#state-hidden").val(value.long_name);
      }
    });
  });
};

TB.transporters.update = function() {
  TB.transporters.edit();
}
