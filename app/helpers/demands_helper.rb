module DemandsHelper

  def budget_class_by_status(budget)
    if budget.sent? || budget.updated? || budget.confirmed?
      "sent"
    end
  end

end
