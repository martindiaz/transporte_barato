module ApplicationHelper

  def render_current_user_navigation_bar params
    action     = params[:action]
    controller = params[:controller]

    if current_user &&
      (action != "show" || controller == "notifications" || controller == "coverage_areas") &&
        action != "search" && action != "change_password"
      user_role_type = current_user.role_type.downcase
      render "layouts/common/#{user_role_type}_navigation_bar"
    end
  end

  def stars(number_of_stars, ratings=nil)
    content = ""
    number_of_stars.times { content << content_tag(:span, '&nbsp;'.html_safe, class: 'star star-on') }
    (5 - number_of_stars).times { content << content_tag(:span, '&nbsp;'.html_safe, class: 'star star-off')}

    content << content_tag(:span, "(#{ratings})".html_safe, class: 'reviews-size') if ratings
    content.html_safe
  end

  def accordion_class(param_id, current_id)
    if !param_id.nil?
      if param_id.to_i == current_id
        "in"
      end
    end
  end

  def accordion_style(param_id, current_id)
    if !param_id.nil?
      if param_id.to_i == current_id
        "height:auto;"
      end
    end
  end

  def conditional_html(&block )
    haml_concat Haml::Util::html_safe <<-"HTML".gsub( /^\s+/, '' )
      <!--[if lt IE 7 ]>              <html class="ie6"> <![endif]-->
      <!--[if IE 7 ]>                 <html class="ie7"> <![endif]-->
      <!--[if IE 8 ]>                 <html class="ie8"> <![endif]-->
      <!--[if IE 9 ]>                 <html class="ie9"> <![endif]-->
      <!--[if (gte IE 9)|!(IE)]><!--> <html> <!--<![endif]-->
    HTML
    haml_concat capture( &block ) << Haml::Util::html_safe( "\n</html>" ) if block_given?
  end
end
