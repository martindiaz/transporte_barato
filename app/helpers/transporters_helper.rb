module TransportersHelper

  def build_company_link(transporter)
    if transporter.company
      edit_transporter_company_path(transporter, transporter.company)
    else
      new_transporter_company_path(transporter)
    end
  end

end