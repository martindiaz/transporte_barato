module CoverageAreasHelper

  def icon_ok_or_not(value)
    value ? "icon-ok" : "icon-remove"
  end

  def father_and_children_categories(category)
    if father_id = category.ancestry
      father = Category.find(father_id)
      "#{father.name} / #{category.name}"
    else
      category.name
    end
  end
end
