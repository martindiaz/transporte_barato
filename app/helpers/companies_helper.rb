module CompaniesHelper
  def profile_photo_style(company)
  <<EOS
background: url(#{ company.image.image.small }) no-repeat right top;
height: 64px;
width: 64px;
EOS
  end
end
