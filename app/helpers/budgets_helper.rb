module BudgetsHelper

  def budget_class(budget)
    if has_special_status?(budget, current_user)
      "tb-budget-new"
    elsif budget.has_new_comments?(current_user)
      "tb-budget-message"
    end
  end

  def has_special_status?(budget, user)
    if user.client?
      budget.sent? || budget.updated? || budget.cancelled? || budget.expired?
    elsif user.transporter?
      budget.accepted? || budget.discarded? || budget.to_be_confirmed? ||
      budget.cancelled? || budget.expired? || budget.another_was_accepted?
    end
  end

  def budget_status_label(budget, user)
    case budget.status
    when Status::Budget::SENT then user.transporter? ? "ENVIADO" : "NUEVO"
    when Status::Budget::SENT_WITHOUT_CREDIT then user.transporter? ? "SIN SALDO" : "PENDIENTE"
    when Status::Budget::NOT_SENT then user.transporter? ? "SIN SALDO" : "PENDIENTE"
    when Status::Budget::NOT_SENT_AND_CANCELLED then user.transporter? ? "CANCELADO" : ""
    when Status::Budget::NOT_SENT_TO_BE_CONFIRMED then user.transporter? ? "A CONFIRMAR" : ""
    when Status::Budget::UPDATED_WITHOUT_CREDIT then user.transporter? ? "SIN SALDO" : "PENDIENTE"
    when Status::Budget::SENT_WITHOUT_CREDIT then user.transporter? ? "SIN SALDO" : "PENDIENTE"
    when Status::Budget::RECEIVED then user.transporter? ? "RECIBIDO" : "DISPONIBLE"
    when Status::Budget::UPDATED then user.transporter? ? "MODIFICADO" : "MODIFICADO"
    when Status::Budget::TO_BE_CONFIRMED then user.transporter? ? "A CONFIRMAR" : "PENDIENTE"
    when Status::Budget::TO_BE_CONFIRMED_WITHOUT_CREDIT then user.transporter? ? "A CONFIRMAR" : "PENDIENTE"
    when Status::Budget::CONFIRMED then user.transporter? ? "CONFIRMADO" : "DISPONIBLE"
    when Status::Budget::CONFIRMED_WITHOUT_CREDIT then user.transporter? ? "A CONFIRMAR" : "PENDIENTE"
    when Status::Budget::ACCEPTED then user.transporter? ? "ACEPTADO" : "ACEPTADO"
    when Status::Budget::DISCARDED then user.transporter? ? "DESCARTADO" : "DESCARTADO"
    when Status::Budget::CANCELLED then user.transporter? ? "CANCELADO" : "CANCELADO"
    when Status::Budget::ANOTHER_WAS_ACCEPTED then user.transporter? ? "ACEPTARON OTRO" : "DESCARTADO"
    when Status::Budget::CANCELLED_DEMAND then user.transporter? ? "ENVIO CANCELADO" : "DESCARTADO"
    when Status::Budget::EXPIRED then user.transporter? ? "CADUCADO" : "CADUCADO"
    when Status::Budget::EXPIRED_DEMAND then user.transporter? ? "ENVIO CADUCADO" : "DESCARTADO"
    when Status::Budget::NOT_SENT_END_OF_PERIOD then user.transporter? ? "CANCELADO" : "DESCARTADO"
    end
  end

end
