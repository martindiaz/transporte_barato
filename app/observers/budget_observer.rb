class BudgetObserver < ActiveRecord::Observer

  def after_create(model)
    if model.transporter_has_credit?
      notification                   = model.demand.user.notifications.build
      notification.sender            = model.transporter.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("NEW_BUDGET_RECEIVED")
      notification.notifiable        = model
      notification.save

      NotificationMailer.new_budget(model.demand, model).deliver
    end

    demand = model.demand

    demand.transporters_with_budgets.each do |transporter|
      if model.accepts_notification_for_cheaper_budgets? &&
        transporter != model.transporter &&
        !transporter.has_a_cheaper_budget?(demand, model)
        cheaper_budget_notification                   = transporter.user.notifications.build
        cheaper_budget_notification.sender            = model.demand.user
        cheaper_budget_notification.status            = Status::Notification::UNREAD
        cheaper_budget_notification.notification_type = NotificationType.find("CHEAPER_BUDGET")
        cheaper_budget_notification.notifiable        = model
        cheaper_budget_notification.save

        NotificationMailer.cheaper_budget(model, transporter).deliver
      end
    end
  end

  def after_update(model)
    if model.active? && model.updated?
      notification                   = model.demand.user.notifications.build
      notification.sender            = model.transporter.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("BUDGET_CHANGED")
      notification.notifiable        = model
      notification.save

      NotificationMailer.budget_changed(model).deliver

      demand = model.demand

      demand.transporters_with_budgets.each do |transporter|
        if transporter != model.transporter && !transporter.has_a_cheaper_budget?(demand, model)
          cheaper_budget_notification                   = transporter.user.notifications.build
          cheaper_budget_notification.sender            = model.demand.user
          cheaper_budget_notification.status            = Status::Notification::UNREAD
          cheaper_budget_notification.notification_type = NotificationType.find("CHEAPER_BUDGET")
          cheaper_budget_notification.notifiable        = model
          cheaper_budget_notification.save

          NotificationMailer.cheaper_budget(model, transporter).deliver
        end
      end
    elsif model.accepted?
      notification                   = model.transporter.user.notifications.build
      notification.sender            = model.demand.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("BUDGET_ACCEPTED")
      notification.notifiable        = model
      notification.save

      NotificationMailer.budget_accepted(model).deliver

#      valoration_notification                   = model.transporter.user.notifications.build
#      valoration_notification.sender            = model.demand.user
#      valoration_notification.status            = Status::Notification::UNREAD
#      valoration_notification.notification_type = NotificationType.find("REMEMBER_TO_VALUATE_CLIENT")
#      valoration_notification.notifiable        = model.demand.user
#      valoration_notification.save
#
#      NotificationMailer.remember_to_valuate_client(model.transporter, model.demand.user).deliver
    elsif model.cancelled?
      notification                   = model.demand.user.notifications.build
      notification.sender            = model.transporter.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("BUDGET_CANCELLED")
      notification.notifiable        = model
      notification.save

      NotificationMailer.budget_cancelled(model).deliver
    elsif model.confirmed?
      notification                   = model.demand.user.notifications.build
      notification.sender            = model.transporter.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("BUDGET_CONFIRMED")
      notification.notifiable        = model
      notification.save

      NotificationMailer.budget_confirmed(model).deliver
    elsif model.was_discarded?
      notification                   = model.transporter.user.notifications.build
      notification.sender            = model.demand.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("BUDGET_DISCARDED")
      notification.notifiable        = model
      notification.save

      NotificationMailer.budget_discarded(model).deliver
    end
  end

end
