class UserObserver < ActiveRecord::Observer

  def after_create(model)
    if model.client?
      model.role.demands.each do |demand|
        Transporter.who_match_location_and_category(
          demand.category, demand.origin, demand.destiny).each do |transporter|
          notification = transporter.user.notifications.build
          notification.sender = model
          notification.status = Status::Notification::UNREAD
          notification.notification_type = NotificationType.find("NEW_DEMAND_TO_BUDGET")
          notification.notifiable = demand
          notification.save
        end

        NotificationMailer.new_demand(demand).deliver
      end
    end
  end

  def after_update(model)
    if model.username_changed?
      NotificationMailer.username_changed(model).deliver
    elsif model.encrypted_password_changed?
      NotificationMailer.password_changed(model).deliver
    end
  end

end
