class CommentObserver < ActiveRecord::Observer

  def after_create(model)
    if model.commentable_is_demand?
      demand = model.commentable

      if model.commentator_is_transporter?
        notification = demand.user.notifications.build
        notification.notification_type = NotificationType.find("NEW_MESSAGE_RECEIVED")
        notification.notifiable = model
        notification.sender     = model.user
        notification.status     = Status::Notification::UNREAD
        notification.save

        NotificationMailer.new_message_received(model, demand.user).deliver
      elsif model.commentator_is_client?
        notification = model.question.user.notifications.build
        notification.notification_type = NotificationType.find("NEW_MESSAGE_RECEIVED")
        notification.notifiable = model
        notification.sender     = model.user
        notification.status     = Status::Notification::UNREAD
        notification.save

        NotificationMailer.new_message_received(model, model.question.user).deliver
      end
    elsif model.commentable_is_budget? && !(model.commentable.new? || model.commentable.updated?)
      budget = model.commentable

      if model.commentator_is_transporter?
        user = budget.demand.user
      elsif model.commentator_is_client?
        user = budget.user
      end

      notification = user.notifications.build
      notification.notification_type = NotificationType.find("NEW_MESSAGE_RECEIVED")
      notification.notifiable = model
      notification.sender     = model.user
      notification.status     = Status::Notification::UNREAD
      notification.save

      NotificationMailer.new_message_received(model, user).deliver
    end
  end

end
