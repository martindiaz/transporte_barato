class RatingObserver < ActiveRecord::Observer

  def after_update(model)
    notification                   = model.receiver.notifications.build
    notification.sender            = model.owner
    notification.status            = Status::Notification::UNREAD
    notification.notification_type = NotificationType.find("NEW_RATING")
    notification.notifiable        = model
    notification.save

    NotificationMailer.new_rating_received(model).deliver
  end

end
