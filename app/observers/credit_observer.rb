class CreditObserver < ActiveRecord::Observer

  def after_create(model)
    if model.buy_movement?
      transporter = model.user.role
      transporter.budgets_to_be_sent.each do |budget|
        budget.sent

        notification                   = budget.demand.user.notifications.build
        notification.sender            = budget.transporter.user
        notification.status            = Status::Notification::UNREAD
        notification.notification_type = NotificationType.find("NEW_BUDGET_RECEIVED")
        notification.notifiable        = budget
        notification.save

        NotificationMailer.new_budget(budget.demand, budget).deliver
      end
    end
  end

end
