class DemandObserver < ActiveRecord::Observer

  def after_create(model)
    if model.has_email?

      if user = User.where(email: model.email).first
        user.send(:generate_confirmation_token)
        user.save(validate: false)

        model.client = user.role
      else
        user = User.new(email: model.email, username: model.email)
        user.skip_confirmation!
        user.send(:generate_confirmation_token)
        user.save(validate: false)

        client = Client.new
        user.role = client
        user.add_role :user
        user.receive_email_alerts = true
        client.user = user
        client.save(validate: false)

        model.client = client
      end

      model.save(validate: false)
    end
  end

  def after_update(model)
    if model.accomplished?
      notification                   = model.client.user.notifications.build
      notification.sender            = model.accepted_budget.transporter.user
      notification.status            = Status::Notification::UNREAD
      notification.notification_type = NotificationType.find("BUDGET_ACCEPTED")
      notification.notifiable        = model
      notification.save

      NotificationMailer.budget_accepted(model).deliver

      # valoration_notification                   = model.client.user.notifications.build
      # valoration_notification.sender            = model.accepted_budget.transporter.user
      # valoration_notification.status            = Status::Notification::UNREAD
      # valoration_notification.notification_type = NotificationType.find("REMEMBER_TO_VALUATE_TRANSPORTER")
      # valoration_notification.notifiable        = model.accepted_budget.transporter
      # valoration_notification.save
      #
      # NotificationMailer.remember_to_valuate_transporter(model.client, model.accepted_budget.transporter).deliver

      model.active_budgets_for_change.each do |budget|
        unless budget.transporter == model.accepted_budget_transporter
          budget.another_was_accepted

          budget_notification = budget.transporter.user.notifications.build
          budget_notification.sender = model.client.user
          budget_notification.status = Status::Notification::UNREAD
          budget_notification.notification_type = NotificationType.find("ANOTHER_BUDGET_WAS_ACCEPTED")
          budget_notification.notifiable = model
          budget_notification.save
        end
      end

      NotificationMailer.another_budget_was_accepted(model).deliver

    elsif model.active?
      model.budgets.each do |budget|
        budget.to_be_confirmed if budget.active?
        budget.not_sent_to_be_confirmed if budget.not_sent?

        transporter = budget.transporter

        notification = transporter.user.notifications.build
        notification.sender = model.client.user
        notification.status = Status::Notification::UNREAD
        notification.notification_type = NotificationType.find("CONFIRM_YOUR_BUDGET")
        notification.notifiable = budget
        notification.save

        NotificationMailer.confirm_your_budget(budget).deliver
      end

      if model.client
        Transporter.who_match_location_and_category(
          model.category,model.origin, model.destiny).each do |transporter|

          unless model.transporter_has_sent_budget?(transporter)
            notification = transporter.user.notifications.build
            notification.sender = model.user
            notification.status = Status::Notification::UNREAD
            notification.notification_type = NotificationType.find("NEW_DEMAND_TO_BUDGET")
            notification.notifiable = model
            notification.save
          end
        end

        NotificationMailer.new_demand(model).deliver
      end

    elsif model.discarded?
      model.budgets.each do |budget|
        budget.demand_was_cancelled
        transporter = budget.transporter

        notification = transporter.user.notifications.build
        notification.sender = model.client.user
        notification.status = Status::Notification::UNREAD
        notification.notification_type = NotificationType.find("DEMAND_CANCELLED")
        notification.notifiable = model
        notification.save
      end

      NotificationMailer.demand_cancelled(model).deliver
    elsif model.expired?
      model.active_budgets.each do |budget|
        budget.expired_demand
        transporter = budget.transporter

        notification = transporter.user.notifications.build
        notification.sender = model.client.user
        notification.status = Status::Notification::UNREAD
        notification.notification_type = NotificationType.find("DEMAND_EXPIRED")
        notification.notifiable = model
        notification.save
      end

      NotificationMailer.demand_expired(model).deliver
    end

  end

end
