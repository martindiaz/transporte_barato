TransporteBarato::Application.routes.draw do

  resources :account_activation, only: [:update, :show]

  match "fletes-landing", to: "landings/fletes#home", via: :get
  match "fletes-publicar-anuncio", to: "landings/fletes#places", via: :get, as: :landings_fletes_places
  match "fletes-exito-mas-info", to: "landings/fletes#dates", via: :get, as: :landings_fletes_dates
  match "fletes-gracias-activar-mail", to: "landings/fletes#thanks", via: :get, as: :landings_fletes_thanks

  match "mudanza-landing", to: "landings/mudanzas#home", via: :get
  match "mudanza-publicar-anuncio", to: "landings/mudanzas#places", via: :get, as: :landings_mudanzas_places
  match "mudanza-exito-mas-info", to: "landings/mudanzas#dates", via: :get, as: :landings_mudanzas_dates
  match "mudanza-gracias-activar-mail", to: "landings/mudanzas#thanks", via: :get, as: :landings_mudanzas_thanks

  match "transporte-landing", to: "landings/transportes#home", via: :get
  match "transporte-publicar-anuncio", to: "landings/transportes#places", via: :get, as: :landings_transporte_places
  match "transporte-exito-mas-info", to: "landings/transportes#dates", via: :get, as: :landings_transporte_dates
  match "transporte-gracias-activar-mail", to: "landings/transportes#thanks", via: :get, as: :landings_transporte_thanks

  namespace :landings do
    resources :fletes
    resources :mudanzas
    resources :transportes
  end

  get "/pages/*id" => 'pages#show', :as => :page, :format => false

  resources :meli, only: [:show]
  resources :kmls, only: [:show]
  resources :points
  resources :categories do
    match :children, via: [:get]
  end

  resources :images do
    collection do
      match :create, via: [:put]
    end
  end

  resources :billing_addresses, only: [:create, :update]

  namespace :admin do
    root to: "dashboard#index"

    resources :dashboard
    resources :extra_services
    resources :movement_types
    resources :payment_conditions
    resources :users
    resources :groups
    resources :icons
    resources :templates
    resources :categories
    resources :icons_templates
    resources :vehicle_caracteristics
    resources :transporters
    resources :home_banners
    resources :page_contents
    resources :demands
    resources :budgets
  end

  root to: "home#index"

  localized(I18n.available_locales) do
    resources :notifications, only: [:index, :show] do
      match :highlight, via: [:post]
      match :delete,    via: [:post]

      collection { get :all }
      collection { get :prominent }
      collection { get :deleted }
      collection { get :unread }
    end

    resources :ratings
    resources :credits
    resources :polygons
    resources :clients do
      match :change_password, via: [:get]
    end
    resources :favorites

    resources :demand_steps

    resources :demands do
      match :search, on: :collection, via: [:get, :post]

      resources :comments, only: [:create]

      member do
        match :accomplish,                     via: [:get, :put]
        match :discard,                        via: [:get, :put]
        match :expire,                         via: [:get, :put]
        match :finish_budget_reception_period, via: [:get, :put]
      end

      resources :budgets do
        member do
          match :accept,  via: [:get, :put]
          match :discard, via: [:get, :put]
        end
      end

    end

    resources :budgets do
      resources :comments, only: [:create]
      resources :cash_flow

      member do
        match :receive,  via: [:post]
        match :expire,   via: [:get, :put]
      end
    end

    resources :transporters do
      match :change_password, via: [:get]
      resources :vehicles
      resources :budgets do
        member do
          match :confirm, via: [:post]
          match :cancel, via: [:post]
        end
      end

      resources :coverage_areas
      resources :companies

      resources :reports, only: [:index]
      get "reports/historical_cost"
      get "reports/budget_situation"
    end

    resources :comments, only: [:create] do
      match :read, via: [:get]
    end

    devise_for :users, :controllers => {
      :registrations => 'registrations',
      :omniauth_callbacks => "users/omniauth_callbacks"
    }
  end

end
