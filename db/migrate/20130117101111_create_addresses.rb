class CreateAddresses < ActiveRecord::Migration
  def self.up
    create_table :addresses do |t|
      t.string :apartment
      t.string :building
      t.string :floor
      t.string :street_line1
      t.string :street_line2
      t.string :zipcode
      t.string :city
      t.string :state
      t.string :country
      t.float  :latitude
      t.float  :longitude

      t.references :addressable, :polymorphic => true

      t.timestamps
    end
  end

  def self.down
    drop_table :addresses
  end
end
