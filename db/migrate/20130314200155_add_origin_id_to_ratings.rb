class AddOriginIdToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :origin_id, :integer
  end
end
