class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.hstore :data

      t.timestamps
    end
  end
end
