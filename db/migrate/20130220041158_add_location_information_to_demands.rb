class AddLocationInformationToDemands < ActiveRecord::Migration
  def change
    add_column :demands, :destiny_information_id, :integer
    add_column :demands, :origin_information_id, :integer
  end
end
