class AddAdministrativeAreaToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :administrative_area, :string
  end
end
