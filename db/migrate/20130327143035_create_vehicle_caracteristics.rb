class CreateVehicleCaracteristics < ActiveRecord::Migration
  def change
    create_table :vehicle_caracteristics do |t|
      t.string :name

      t.timestamps
    end

    ['Reefer', 'High Cube', 'Dry Van', 'Métalicos', 'Open Top', 'Flat Rack', 'Open Side', 'Contenedor cisterna', 'Flexi-Tank'].each do |name|
      VehicleCaracteristic.create(name: name)
    end
  end
end
