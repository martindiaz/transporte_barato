class AddTemplateIdToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :template_id, :integer
  end
end
