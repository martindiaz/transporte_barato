class CreateCompaniesPaymentConditions < ActiveRecord::Migration
  def change
    create_table :companies_payment_conditions do |t|
      t.integer :company_id
      t.integer :payment_condition_id
    end
  end
end
