class AddDistanceToDemands < ActiveRecord::Migration
  def change
    add_column :demands, :distance, :string
  end
end
