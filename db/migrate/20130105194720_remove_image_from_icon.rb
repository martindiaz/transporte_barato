class RemoveImageFromIcon < ActiveRecord::Migration
  def up
    remove_column :icons, :image
  end

  def down
    add_column :icons, :image, :string
  end
end
