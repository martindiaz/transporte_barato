class AddPaymentTypeToCredits < ActiveRecord::Migration
  def change
    add_column :credits, :payment_type, :string
  end
end
