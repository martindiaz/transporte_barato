class CreateExtraServices < ActiveRecord::Migration
  def change
    create_table :extra_services do |t|
      t.text :description

      t.timestamps
    end
  end
end
