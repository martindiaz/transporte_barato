class AddExpirationDateToDemands < ActiveRecord::Migration
  def change
    add_column :demands, :expiration_date, :date
  end
end
