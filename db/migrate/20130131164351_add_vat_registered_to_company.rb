class AddVatRegisteredToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :vat_registered, :boolean
  end
end
