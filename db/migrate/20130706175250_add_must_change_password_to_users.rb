class AddMustChangePasswordToUsers < ActiveRecord::Migration
  def change
    add_column :users, :must_change_password, :boolean, default: false
  end
end
