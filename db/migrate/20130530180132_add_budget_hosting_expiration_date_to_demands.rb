class AddBudgetHostingExpirationDateToDemands < ActiveRecord::Migration
  def change
    add_column :demands, :budget_hosting_expiration_date, :date
  end
end
