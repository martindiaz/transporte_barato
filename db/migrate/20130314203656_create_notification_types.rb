class CreateNotificationTypes < ActiveRecord::Migration
  def change
    create_table :notification_types, { id: false } do |t|
      t.string :name
      t.string :description
      t.string :partial
    end
    execute "ALTER TABLE notification_types ADD PRIMARY KEY (name);"
  end
end
