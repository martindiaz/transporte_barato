class RemoveFieldsToRatings < ActiveRecord::Migration
  def up
    remove_column :ratings, :client_id
    remove_column :ratings, :transporter_id
    remove_column :ratings, :destiny_id
    remove_column :ratings, :origin_id
  end

  def down
    add_column :ratings, :client_id, :integer
    add_column :ratings, :transporter_id, :integer
    add_column :ratings, :destiny_id, :integer
    add_column :ratings, :origin_id, :integer
  end
end

