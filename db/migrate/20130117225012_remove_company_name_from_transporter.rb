class RemoveCompanyNameFromTransporter < ActiveRecord::Migration
  def up
    remove_column :transporters, :company_name
  end

  def down
    add_column :transporters, :company_name, :string
  end
end
