class CreatePaymentConditions < ActiveRecord::Migration
  def change
    create_table :payment_conditions do |t|
      t.string :name

      t.timestamps
    end
  end
end
