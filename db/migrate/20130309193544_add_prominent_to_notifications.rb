class AddProminentToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :prominent, :boolean
  end
end
