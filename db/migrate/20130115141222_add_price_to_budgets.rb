class AddPriceToBudgets < ActiveRecord::Migration
  def change
    add_column :budgets, :price, :decimal
  end
end
