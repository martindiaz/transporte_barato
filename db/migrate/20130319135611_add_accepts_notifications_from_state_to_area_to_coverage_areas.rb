class AddAcceptsNotificationsFromStateToAreaToCoverageAreas < ActiveRecord::Migration
  def change
    add_column :coverage_areas, :accepts_notifications_from_state_to_area, :boolean
  end
end
