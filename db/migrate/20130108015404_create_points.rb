class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.decimal :latitude
      t.decimal :longitude
      t.integer :polygon_id

      t.timestamps
    end
  end
end
