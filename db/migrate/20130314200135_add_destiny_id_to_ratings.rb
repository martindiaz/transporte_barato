class AddDestinyIdToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :destiny_id, :integer
  end
end
