class CreateDemandsTrnsporters < ActiveRecord::Migration
  def change
    create_table :demands_transporters, :id => false do |t|
      t.integer :demand_id
      t.integer :transporter_id
    end

    add_index :demands_transporters, [:demand_id, :transporter_id], :name => 'dt_demand_transporter'
    add_index :demands_transporters, [:transporter_id, :demand_id], :name => 'dt_transporter_demand'
  end
end
