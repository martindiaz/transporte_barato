class CreateNewMovementTypes < ActiveRecord::Migration
  def up
    drop_table :movement_types

    create_table :movement_types, { id: false } do |t|
      t.string :name
      t.string :description
    end
    execute "ALTER TABLE movement_types ADD PRIMARY KEY (name);"

    {
      BUY_CREDITS:   "Compra de creditos",
      BUDGET_VIEWED: "Presupuesto visto por el usuario"
    }.each do |k, v|
      MovementType.create(name: k.to_s, description: v)
    end

    change_column :credits, :movement_type_id, :string
  end

  def down
    change_column :credits, :movement_type_id, :integer
  end
end
