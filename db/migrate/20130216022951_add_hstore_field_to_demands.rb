class AddHstoreFieldToDemands < ActiveRecord::Migration
  def change
    add_column :demands, :data, :hstore

    execute "CREATE INDEX demands_data ON demands USING GIN(data)"
  end
end
