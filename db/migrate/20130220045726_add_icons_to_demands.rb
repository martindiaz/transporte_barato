class AddIconsToDemands < ActiveRecord::Migration
  def change
    add_column :demands, :icons, :hstore

    execute "CREATE INDEX demands_icons ON demands USING GIN(icons)"
  end
end
