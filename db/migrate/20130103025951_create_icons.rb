class CreateIcons < ActiveRecord::Migration
  def change
    create_table :icons do |t|
      t.integer :group_id
      t.string :description
      t.integer :order
      t.string :image
      t.float :volume
      t.integer :unit
      t.float :high
      t.float :width
      t.float :depth
      t.float :weight

      t.timestamps
    end
  end
end
