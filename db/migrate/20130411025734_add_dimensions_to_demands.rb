class AddDimensionsToDemands < ActiveRecord::Migration
  def change
    add_column :demands, :height, :float
    add_column :demands, :height_unit, :string

    add_column :demands, :width, :float
    add_column :demands, :width_unit, :string

    add_column :demands, :depth, :float
    add_column :demands, :depth_unit, :string

    add_column :demands, :weight, :float
    add_column :demands, :weight_unit, :string
  end
end
