class AddBasePriceToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :base_price, :float, default: 0.0
  end
end
