class CreatePolygons < ActiveRecord::Migration
  def change
    create_table :polygons do |t|
      t.string :description

      t.timestamps
    end
  end
end
