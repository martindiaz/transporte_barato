class AddZoneIdToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :zone_id, :integer
  end
end
