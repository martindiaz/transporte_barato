class CreateCoverageAreasKmls < ActiveRecord::Migration
  def self.up
    create_table :coverage_areas_kmls, :id => false do |t|
        t.references :kml
        t.references :coverage_area
    end
    add_index :coverage_areas_kmls, [:kml_id, :coverage_area_id], :name => 'cca_kml_coverage'
    add_index :coverage_areas_kmls, [:coverage_area_id, :kml_id], :name => 'cca_coverage_kml'
  end

  def self.down
    drop_table :coverage_areas_kmls
  end
end
