class AddDescriptionToCoverageAreas < ActiveRecord::Migration
  def change
    add_column :coverage_areas, :description, :string
  end
end
