class CreateHomeBanners < ActiveRecord::Migration
  def change
    create_table :home_banners do |t|
      t.integer :transporter_id

      t.timestamps
    end
  end
end
