class RemovePriceFromBudgets < ActiveRecord::Migration
  def up
    remove_column :budgets, :price
  end

  def down
    add_column :budgets, :price, :string
  end
end
