class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.references :client
      t.references :transporter
      t.integer :points
      t.text :comment

      t.timestamps
    end
  end
end
