class AddStatusToDemand < ActiveRecord::Migration
  def change
    add_column :demands, :status, :string
  end
end
