class RemoveDescriptionFromNotifications < ActiveRecord::Migration
  def up
    remove_column :notifications, :description
  end

  def down
    add_column :notifications, :description, :string
  end
end
