class CreateCategoriesForms < ActiveRecord::Migration
  def change
    create_table :categories_forms, :id => false do |t|
      t.integer :category_id
      t.integer :form_id
    end

    add_index :categories_forms, [:category_id, :form_id], :name => 'cf_category_form'
    add_index :categories_forms, [:form_id, :category_id], :name => 'cf_form_category'
  end
end
