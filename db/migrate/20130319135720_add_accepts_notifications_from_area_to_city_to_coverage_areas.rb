class AddAcceptsNotificationsFromAreaToCityToCoverageAreas < ActiveRecord::Migration
  def change
    add_column :coverage_areas, :accepts_notifications_from_area_to_city, :boolean
  end
end
