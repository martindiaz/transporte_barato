class ChangeDestinyAndOriginOnDemands < ActiveRecord::Migration
  def change
    remove_column :demands, :origin
    add_column :demands, :origin_id, :integer
    remove_column :demands, :destiny
    add_column :demands, :destiny_id, :integer
  end
end
