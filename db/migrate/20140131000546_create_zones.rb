class CreateZones < ActiveRecord::Migration
  def change
    create_table :zones do |t|
      t.string :name
      t.string :administrative_areas, :null => false, :array => true
      t.string :cities, :null => false, :array => true

      t.timestamps
    end
  end
end
