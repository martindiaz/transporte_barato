class RemoveMessageFromBudgets < ActiveRecord::Migration
  def up
    remove_column :budgets, :message
  end

  def down
    add_column :budgets, :message, :text
  end
end
