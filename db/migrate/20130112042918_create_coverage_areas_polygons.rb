class CreateCoverageAreasPolygons < ActiveRecord::Migration
  def self.up
    create_table :coverage_areas_polygons, :id => false do |t|
        t.references :polygon
        t.references :coverage_area
    end
    add_index :coverage_areas_polygons, [:polygon_id, :coverage_area_id], :name => 'cca_polygon_coverage'
    add_index :coverage_areas_polygons, [:coverage_area_id, :polygon_id], :name => 'cca_coverage_polygon'
  end

  def self.down
    drop_table :coverage_areas_polygons
  end
end
