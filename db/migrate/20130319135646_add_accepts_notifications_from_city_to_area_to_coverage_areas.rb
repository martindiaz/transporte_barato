class AddAcceptsNotificationsFromCityToAreaToCoverageAreas < ActiveRecord::Migration
  def change
    add_column :coverage_areas, :accepts_notifications_from_city_to_area, :boolean
  end
end
