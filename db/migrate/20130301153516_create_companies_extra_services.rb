class CreateCompaniesExtraServices < ActiveRecord::Migration
  def change
    create_table :companies_extra_services do |t|
      t.integer :company_id
      t.integer :extra_service_id
    end
  end
end
