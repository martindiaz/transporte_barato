class AddAdditionalInformationToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :additional_information, :text
  end
end
