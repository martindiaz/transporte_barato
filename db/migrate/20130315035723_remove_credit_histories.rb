class RemoveCreditHistories < ActiveRecord::Migration
  def up
    drop_table :credit_histories
  end

  def down
    create_table :credit_histories do |t|
    end
  end
end
