class AddAcceptsNotificationForCheaperBudgetsToBudget < ActiveRecord::Migration
  def change
    add_column :budgets, :accepts_notification_for_cheaper_budgets, :boolean, default: false
  end
end
