class CreateCompaniesPaymentMethods < ActiveRecord::Migration
  def change
    create_table :companies_payment_methods do |t|
      t.integer :company_id
      t.integer :payment_method_id
    end
  end
end
