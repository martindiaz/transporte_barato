class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.integer :year_of_creation
      t.string :transport_license_number
      t.integer :transporter_id
      t.integer :number_of_drivers
      t.integer :number_of_employees
      t.string :company_type

      t.timestamps
    end
  end
end
