class AddOwnerIdToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :owner_id, :integer
  end
end
