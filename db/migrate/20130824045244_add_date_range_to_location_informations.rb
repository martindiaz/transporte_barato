class AddDateRangeToLocationInformations < ActiveRecord::Migration
  def change
    add_column :location_informations, :date_range, :string
  end
end
