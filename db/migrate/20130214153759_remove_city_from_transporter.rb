class RemoveCityFromTransporter < ActiveRecord::Migration
  def up
    remove_column :transporters, :city
  end

  def down
    add_column :transporters, :city, :string
  end
end
