class AddBudgetIdToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :budget_id, :integer
  end
end
