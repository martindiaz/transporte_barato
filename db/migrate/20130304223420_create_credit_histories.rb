class CreateCreditHistories < ActiveRecord::Migration
  def change
    create_table :credit_histories do |t|
      t.integer :company_id
      t.integer :movement_type_id
      t.integer :amount
      t.integer :budget_id

      t.timestamps
    end
  end
end
