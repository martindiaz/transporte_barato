class AddLicenseAndInsuranceToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :license_and_insurance, :text
  end
end
