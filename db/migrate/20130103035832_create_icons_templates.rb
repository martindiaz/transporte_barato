class CreateIconsTemplates < ActiveRecord::Migration
  def change
    create_table :icons_templates do |t|
      t.integer :template_id
      t.integer :icon_id
      t.integer :amount

      t.timestamps
    end
  end
end
