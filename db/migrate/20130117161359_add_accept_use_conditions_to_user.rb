class AddAcceptUseConditionsToUser < ActiveRecord::Migration
  def change
    add_column :users, :accept_use_conditions, :boolean
  end
end
