class AddStatusToBudget < ActiveRecord::Migration
  def change
    add_column :budgets, :status, :string
  end
end
