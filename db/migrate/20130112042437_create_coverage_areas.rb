class CreateCoverageAreas < ActiveRecord::Migration
  def change
    create_table :coverage_areas do |t|
      t.integer :transporter_id

      t.timestamps
    end
  end
end
