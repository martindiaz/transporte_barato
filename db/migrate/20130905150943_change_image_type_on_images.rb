class ChangeImageTypeOnImages < ActiveRecord::Migration
  def change
    change_column :images, :image, :string
  end
end
