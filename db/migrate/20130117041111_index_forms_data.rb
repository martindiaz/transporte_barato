class IndexFormsData < ActiveRecord::Migration
  def up
    execute "CREATE INDEX forms_gin_data ON forms USING GIN(data)"
  end

  def down
    execute "DROP INDEX forms_gin_data"
  end
end
