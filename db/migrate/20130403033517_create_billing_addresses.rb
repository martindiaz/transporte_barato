class CreateBillingAddresses < ActiveRecord::Migration
  def change
    create_table :billing_addresses do |t|
      t.string :first_name
      t.string :last_name
      t.string :street_line
      t.string :city
      t.string :zip
      t.string :state
      t.string :country
      t.integer :user_id

      t.timestamps
    end
  end
end
