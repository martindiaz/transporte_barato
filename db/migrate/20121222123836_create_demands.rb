class CreateDemands < ActiveRecord::Migration
  def change
    create_table :demands do |t|
      t.datetime :delivery_date
      t.string :origin
      t.string :destiny
      t.text :description
      t.integer :category_id
      t.integer :client_id

      t.timestamps
    end
  end
end
