class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
      t.references :user
      t.string :movement_type_id
      t.references :budget
      t.integer    :amount

      t.timestamps
    end
  end
end
