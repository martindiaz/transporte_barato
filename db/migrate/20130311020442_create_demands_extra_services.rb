class CreateDemandsExtraServices < ActiveRecord::Migration
  def change
    create_table :demands_extra_services, :id => false do |t|
      t.integer :extra_service_id
      t.integer :demand_id
    end
  end
end
