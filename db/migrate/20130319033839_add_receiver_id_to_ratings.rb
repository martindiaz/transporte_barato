class AddReceiverIdToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :receiver_id, :integer
  end
end
