class AddForeingKeyToIconsTemplates < ActiveRecord::Migration
  def change
    add_index :icons_templates, :template_id
    add_index :icons_templates, :icon_id   
  end
end
