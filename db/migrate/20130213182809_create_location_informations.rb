class CreateLocationInformations < ActiveRecord::Migration
  def change
    create_table :location_informations do |t|
      t.boolean :has_place_to_park
      t.date :delivery_date_from
      t.date :delivery_date_to

      t.references :location_informable, :polymorphic => true

      t.timestamps
    end
  end
end
