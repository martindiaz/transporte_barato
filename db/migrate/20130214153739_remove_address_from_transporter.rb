class RemoveAddressFromTransporter < ActiveRecord::Migration
  def up
    remove_column :transporters, :address
  end

  def down
    add_column :transporters, :address, :string
  end
end
