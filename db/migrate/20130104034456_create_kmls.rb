class CreateKmls < ActiveRecord::Migration
  def change
    create_table :kmls do |t|
      t.string :name
      t.text :body

      t.timestamps
    end
  end
end
