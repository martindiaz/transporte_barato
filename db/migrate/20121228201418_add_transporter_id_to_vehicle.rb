class AddTransporterIdToVehicle < ActiveRecord::Migration
  def change
    add_column :vehicles, :transporter_id, :integer
  end
end
