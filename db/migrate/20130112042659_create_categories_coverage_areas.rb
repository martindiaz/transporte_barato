class CreateCategoriesCoverageAreas < ActiveRecord::Migration
  def self.up
    create_table :categories_coverage_areas, :id => false do |t|
        t.references :category
        t.references :coverage_area
    end
    add_index :categories_coverage_areas, [:category_id, :coverage_area_id], :name => 'cca_category_coverage'
    add_index :categories_coverage_areas, [:coverage_area_id, :category_id], :name => 'cca_coverage_category'
  end

  def self.down
    drop_table :categories_coverage_areas
  end
end
