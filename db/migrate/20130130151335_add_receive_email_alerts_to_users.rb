class AddReceiveEmailAlertsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :receive_email_alerts, :boolean
  end
end
