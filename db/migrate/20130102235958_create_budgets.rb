class CreateBudgets < ActiveRecord::Migration
  def change
    create_table :budgets do |t|
      t.string :price
      t.text :message
      t.integer :transporter_id
      t.integer :demand_id

      t.timestamps
    end
  end
end
