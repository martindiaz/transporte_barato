class AddAcceptsNotificationsFromAreaToStateToCoverageAreas < ActiveRecord::Migration
  def change
    add_column :coverage_areas, :accepts_notifications_from_area_to_state, :boolean
  end
end
