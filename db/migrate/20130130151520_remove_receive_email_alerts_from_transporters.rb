class RemoveReceiveEmailAlertsFromTransporters < ActiveRecord::Migration
  def up
    remove_column :transporters, :receive_email_alerts
  end

  def down
    add_column :transporters, :receive_email_alerts, :boolean
  end
end
