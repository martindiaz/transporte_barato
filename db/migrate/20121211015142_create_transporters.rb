class CreateTransporters < ActiveRecord::Migration
  def change
    create_table :transporters do |t|
      t.string :company_name
      t.string :address
      t.string :city
      t.boolean :receive_email_alerts

      t.timestamps
    end
  end
end
