# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140211185354) do

  add_extension "hstore"

  create_table "addresses", :force => true do |t|
    t.string   "apartment"
    t.string   "building"
    t.string   "floor"
    t.string   "street_line1"
    t.string   "street_line2"
    t.string   "zipcode"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "neighborhood"
    t.string   "administrative_area"
    t.integer  "zone_id"
  end

  create_table "billing_addresses", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "street_line"
    t.string   "city"
    t.string   "zip"
    t.string   "state"
    t.string   "country"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "budgets", :force => true do |t|
    t.integer  "transporter_id"
    t.integer  "demand_id"
    t.datetime "created_at",                                                  :null => false
    t.datetime "updated_at",                                                  :null => false
    t.decimal  "price"
    t.string   "status"
    t.date     "expiration_date"
    t.boolean  "accepts_notification_for_cheaper_budgets", :default => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.string   "ancestry"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.integer  "template_id"
    t.float    "base_price",  :default => 0.0
    t.integer  "order"
  end

  add_index "categories", ["ancestry"], :name => "index_categories_on_ancestry"

  create_table "categories_companies", :force => true do |t|
    t.integer "company_id"
    t.integer "category_id"
  end

  create_table "categories_coverage_areas", :id => false, :force => true do |t|
    t.integer "category_id"
    t.integer "coverage_area_id"
  end

  add_index "categories_coverage_areas", ["category_id", "coverage_area_id"], :name => "cca_category_coverage"
  add_index "categories_coverage_areas", ["coverage_area_id", "category_id"], :name => "cca_coverage_category"

  create_table "categories_forms", :id => false, :force => true do |t|
    t.integer "category_id"
    t.integer "form_id"
  end

  add_index "categories_forms", ["category_id", "form_id"], :name => "cf_category_form"
  add_index "categories_forms", ["form_id", "category_id"], :name => "cf_form_category"

  create_table "categories_templates", :id => false, :force => true do |t|
    t.integer "category_id"
    t.integer "template_id"
  end

  create_table "clients", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.text     "message"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.integer  "question_id"
    t.string   "status",           :default => "READ"
  end

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.integer  "year_of_creation"
    t.string   "transport_license_number"
    t.integer  "transporter_id"
    t.integer  "number_of_drivers"
    t.integer  "number_of_employees"
    t.string   "company_type"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "phone_number"
    t.string   "website"
    t.boolean  "vat_registered"
    t.text     "license_and_insurance"
    t.text     "additional_information"
  end

  create_table "companies_extra_services", :force => true do |t|
    t.integer "company_id"
    t.integer "extra_service_id"
  end

  create_table "companies_payment_conditions", :force => true do |t|
    t.integer "company_id"
    t.integer "payment_condition_id"
  end

  create_table "companies_payment_methods", :force => true do |t|
    t.integer "company_id"
    t.integer "payment_method_id"
  end

  create_table "coverage_areas", :force => true do |t|
    t.integer  "transporter_id"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "description"
    t.boolean  "accepts_notifications_from_state_to_area"
    t.boolean  "accepts_notifications_from_city_to_area"
    t.boolean  "accepts_notifications_from_area_to_city"
    t.boolean  "accepts_notifications_from_area_to_state"
  end

  create_table "coverage_areas_kmls", :id => false, :force => true do |t|
    t.integer "kml_id"
    t.integer "coverage_area_id"
  end

  add_index "coverage_areas_kmls", ["coverage_area_id", "kml_id"], :name => "cca_coverage_kml"
  add_index "coverage_areas_kmls", ["kml_id", "coverage_area_id"], :name => "cca_kml_coverage"

  create_table "coverage_areas_polygons", :id => false, :force => true do |t|
    t.integer "polygon_id"
    t.integer "coverage_area_id"
  end

  add_index "coverage_areas_polygons", ["coverage_area_id", "polygon_id"], :name => "cca_coverage_polygon"
  add_index "coverage_areas_polygons", ["polygon_id", "coverage_area_id"], :name => "cca_polygon_coverage"

  create_table "credits", :force => true do |t|
    t.integer  "user_id"
    t.string   "movement_type_id"
    t.integer  "budget_id"
    t.integer  "amount"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "payment_type"
    t.string   "status"
  end

  create_table "demands", :force => true do |t|
    t.datetime "delivery_date"
    t.text     "description"
    t.integer  "category_id"
    t.integer  "client_id"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.integer  "origin_id"
    t.integer  "destiny_id"
    t.string   "status"
    t.date     "expiration_date"
    t.hstore   "data"
    t.integer  "destiny_information_id"
    t.integer  "origin_information_id"
    t.hstore   "icons"
    t.string   "distance"
    t.text     "detail"
    t.float    "height"
    t.string   "height_unit"
    t.float    "width"
    t.string   "width_unit"
    t.float    "depth"
    t.string   "depth_unit"
    t.float    "weight"
    t.string   "weight_unit"
    t.date     "budget_hosting_expiration_date"
    t.string   "email"
    t.string   "provider"
  end

  add_index "demands", ["data"], :name => "demands_data", :using => :gin
  add_index "demands", ["icons"], :name => "demands_icons", :using => :gin

  create_table "demands_extra_services", :id => false, :force => true do |t|
    t.integer "extra_service_id"
    t.integer "demand_id"
  end

  create_table "demands_transporters", :id => false, :force => true do |t|
    t.integer "demand_id"
    t.integer "transporter_id"
  end

  add_index "demands_transporters", ["demand_id", "transporter_id"], :name => "dt_demand_transporter"
  add_index "demands_transporters", ["transporter_id", "demand_id"], :name => "dt_transporter_demand"

  create_table "extra_services", :force => true do |t|
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "forms", :force => true do |t|
    t.hstore   "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "forms", ["data"], :name => "forms_gin_data", :using => :gin

  create_table "groups", :force => true do |t|
    t.string   "description"
    t.integer  "order"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "home_banners", :force => true do |t|
    t.integer  "transporter_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "icons", :force => true do |t|
    t.integer  "group_id"
    t.string   "description"
    t.integer  "order"
    t.float    "volume"
    t.integer  "unit"
    t.float    "high"
    t.float    "width"
    t.float    "depth"
    t.float    "weight"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "icons", ["group_id"], :name => "index_icons_on_group_id"

  create_table "icons_templates", :force => true do |t|
    t.integer  "template_id"
    t.integer  "icon_id"
    t.integer  "amount"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "icons_templates", ["icon_id"], :name => "index_icons_templates_on_icon_id"
  add_index "icons_templates", ["template_id"], :name => "index_icons_templates_on_template_id"

  create_table "images", :force => true do |t|
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "image"
  end

  create_table "kmls", :force => true do |t|
    t.string   "name"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "location_informations", :force => true do |t|
    t.boolean  "has_place_to_park"
    t.date     "delivery_date_from"
    t.date     "delivery_date_to"
    t.integer  "location_informable_id"
    t.string   "location_informable_type"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "date_range"
  end

  create_table "movement_types", :id => false, :force => true do |t|
    t.string "name",        :null => false
    t.string "description"
  end

  create_table "notification_types", :id => false, :force => true do |t|
    t.string "name",        :null => false
    t.string "description"
    t.string "partial"
  end

  create_table "notifications", :force => true do |t|
    t.string   "status"
    t.integer  "user_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "sender_id"
    t.boolean  "prominent"
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.string   "notification_type_id"
  end

  create_table "page_contents", :force => true do |t|
    t.string   "name"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "payment_conditions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "payment_methods", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "points", :force => true do |t|
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.integer  "polygon_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "polygons", :force => true do |t|
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "ratings", :force => true do |t|
    t.integer  "points"
    t.text     "comment"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "budget_id"
    t.integer  "owner_id"
    t.integer  "receiver_id"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "templates", :force => true do |t|
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "transporters", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "role_id"
    t.string   "role_type"
    t.string   "name"
    t.string   "surname"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "provider"
    t.string   "uid"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "phone_number"
    t.boolean  "accept_use_conditions"
    t.string   "username"
    t.boolean  "receive_email_alerts"
    t.boolean  "must_change_password",   :default => false
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "vehicle_caracteristics", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "vehicles", :force => true do |t|
    t.string   "description"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "name"
    t.integer  "transporter_id"
  end

  create_table "zones", :force => true do |t|
    t.string   "name"
    t.string   "administrative_areas", :null => false, :array => true
    t.string   "cities",               :null => false, :array => true
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

end
