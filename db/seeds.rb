# encoding: UTF-8
params = {
  name: 'Transporte',
  surname: 'Barato',
  username: 'Admin',
  email: 'administrador@transportebarato.com',
  password: 'transportebarato'
}

user = User.create(params)
user.confirm!
user.add_role :admin

client = Client.new
client.user = user
client.save

Rake::Task['db:fixtures:load'].invoke
Rake::Task['data:category_icons'].invoke
Rake::Task['data:icons'].invoke
Rake::Task['data:kmls'].invoke
Rake::Task['data:notification_types'].invoke
