module Status
  module Demand
    ACTIVE       = "ACTIVE"
    ACCOMPLISHED = "ACCOMPLISHED"
    DISCARDED    = "DISCARDED"
    EXPIRED      = "EXPIRED"
    FIRST_STEP   = "FIRST_STEP"
    SECOND_STEP  = "SECOND_STEP"
    THIRD_STEP   = "THIRD_STEP"
  end

  module Budget
    ACTIVE    = "ACTIVE"
    ACCEPTED  = "ACCEPTED"
    DISCARDED = "DISCARDED"
    SENT      = "SENT"
    RECEIVED  = "RECEIVED"
    UPDATED   = "UPDATED"
    TO_BE_CONFIRMED = "TO_BE_CONFIRMED"
    CONFIRMED = "CONFIRMED"
    NOT_SENT  = "NOT_SENT"
    SENT_WITHOUT_CREDIT = "SENT_WITHOUT_CREDIT"
    UPDATED_WITHOUT_CREDIT = "UPDATED_WITHOUT_CREDIT"
    TO_BE_CONFIRMED_WITHOUT_CREDIT = "TO_BE_CONFIRMED_WITHOUT_CREDIT"
    CONFIRMED_WITHOUT_CREDIT = "CONFIRMED_WITHOUT_CREDIT"
    NOT_SENT_TO_BE_CONFIRMED = "NOT_SENT_TO_BE_CONFIRMED"
    CANCELLED = "CANCELLED"
    ANOTHER_WAS_ACCEPTED = "ANOTHER_WAS_ACCEPTED"
    EXPIRED = "EXPIRED"
    CANCELLED_DEMAND = "CANCELLED_DEMAND"
    EXPIRED_DEMAND = "EXPIRED_DEMAND"
    NOT_SENT_AND_CANCELLED = "NOT_SENT_AND_CANCELLED"
    NOT_SENT_END_OF_PERIOD = "NOT_SENT_END_OF_PERIOD"
  end

  module Notification
    READ      = "READ"
    UNREAD    = "UNREAD"
    DELETED   = "DELETED"
  end

  module Comment
    READ   = "READ"
    UNREAD = "UNREAD"
  end
end
