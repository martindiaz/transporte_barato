class Meli
  include HTTParty

  base_uri 'https://api.mercadolibre.com'

  SITE_ID = 'MLA'

  def item(id)
    self.class.get("/items/#{SITE_ID}#{id}").parsed_response
  end
end
