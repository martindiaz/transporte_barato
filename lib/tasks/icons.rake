namespace :data do
  desc "Load icons from files"
  task :icons => :environment do

    Dir.open("#{Rails.root}/lib/tasks/iconos").each do |file|

      next if file =~ /^\.|^\.\./

      group_name, icon_name = file.split('_')

      icon_name = icon_name.gsub(/_|-/, ' ')
      icon_name = icon_name.gsub!(/\.jpg|\.png/, '')

      group_name = group_name.titlecase

      if group_name == "Sala"
        group_name = "Living"
      end

      group = Group.find_or_create_by_description(group_name)
      image = Image.new
      image.image = File.open("#{Rails.root}/lib/tasks/iconos/#{file}")
      image.save

      icon = Icon.new(description: icon_name, group_id: group.id)
      icon.image = image
      icon.save

      IconsTemplate.create(template_id: Template.first.id, icon_id: icon.id)

      puts "#{group_name} - #{icon_name} #{icon.id}"

    end

  end

  desc "Load category icons"
  task :category_icons => :environment do
    Category.roots.order(:name).delete_if{|a| a.name =~ /otros/i }.each do |category|

      image = Image.new
      image.image = File.open("#{Rails.root}/lib/tasks/categorias/#{category.name.downcase}.png")
      image.save

      category.image = image
      category.save

    end

  end
end
