namespace :data do

  desc "Insert notification types"
  task :notification_types => :environment do
    {
      CANCELLED_ACCOUNT:               ["Cancelaste tu cuenta", "cancel_account"],
      NEW_MESSAGE_RECEIVED:            ["Nuevo mensaje recibido", "new_message_received"],
      NEW_BUDGET_RECEIVED:             ["Nuevo presupuesto", "new_budget_received"],
      DEMAND_ABOUT_TO_EXPIRE:          ["Tu envío esta pronto a caducar", "demand_about_to_expire"],
      REMEMBER_TO_VALUATE_TRANSPORTER: ["Tienes una valoración pendiente", "remember_to_valuate_transporter"],
      REMEMBER_TO_VALUATE_CLIENT:      ["Tienes una valoración pendiente", "remember_to_valuate_client"],
      DEMAND_CHANGED:                  ["Envío actualizado", "demand_changed"],
      BUDGET_CHANGED:                  ["Presupuesto actualizado", "budget_changed"],
      BUDGET_ACCEPTED:                 ["Presupuesto aceptado", "budget_accepted"],
      NO_CREDIT:                       ["Tienes presupuestos pendientes de entrega", "no_credit"],
      NEW_DEMAND_TO_BUDGET:            ["Nuevo envío para presupuestar", "new_demand_to_budget"],
      NEW_RATING:                      ["Has recibido una valoración", "new_rating_received"],
      ANOTHER_BUDGET_WAS_ACCEPTED:     ["Otro presupuesto fue aceptado", "another_budget_was_accepted"],
      CONFIRM_YOUR_BUDGET:             ["Confirma tu presupuesto", "confirm_your_budget"],
      BUDGET_CANCELLED:                ["Han cancelado un presupuesto", "budget_cancelled"],
      BUDGET_CONFIRMED:                ["Han confirmado un presupuesto", "budget_confirmed"],
      BUDGET_DISCARDED:                ["Han descartado tu presupuesto", "budget_discarded"],
      DEMAND_CANCELLED:                ["Han cancelado un envío", "demand_cancelled"],
      DEMAND_EXPIRED:                  ["Envío caducado", "demand_expired"],
      BUDGET_EXPIRED:                  ["Tienes presupuestos que caducan hoy", "budget_expired"],
      CHEAPER_BUDGET:                  ["Han presupuestado más barato que tu", "cheaper_budget"],
      END_OF_RECEPTION_PERIOD:         ["Ya puedes elegir la mejor oferta para tu envío", "end_of_reception_period"]
    }.each do |k, v|
      NotificationType.create(name: k.to_s, description: v.first, partial: v.last)
    end
  end

end
