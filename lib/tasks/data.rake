namespace :data do
  desc "Add credits to transporters"
  task :credits => :environment do
    CSV.foreach("#{Rails.root}/lib/tasks/transporters.csv", {headers: true}) do |row|
      user = User.where(email: row['mail']).first

      break unless user

      credit = Credit.new

      credit.amount = 100
      credit.movement_type = MovementType.find('BUY_CREDITS')
      credit.user = user
      credit.status = 'Entregado'
      begin
        credit.save
      rescue
        puts "No se pudo cargar credito para el usuario: #{user.id}"
      end
    end
  end

  desc "Load transporters from csv file"
  task :transporters => :environment do
    counter = 0

    STATES = {
      'BA' => 'Buenos Aires',
      'ER' => 'Entre Ríos',
      'CBA' => 'Córdoba',
      'CF' => 'Capital Federal',
      'MZA' => 'Mendoza',
      'SF' => 'Santa Fe',
      'SAL' => 'Salta',
      'SJ' => 'San Juan',
      'CHU' => 'Chubut',
      'TDF' => 'Tierra del Fuego',
      'SC' => 'Santa Cruz',
      'TUC' => 'Tucuman',
      'RN' => 'Río Negro',
      'NEU' => 'Neuquén',
      'SE' => 'Santiago del Estero',
      'SL' => 'San Luis',
      'MIS' => 'Misiones',
      'LP' => 'La Pampa',
      'FOR' => 'Formosa',
      'CHA' => 'Chaco',
      'JUJ' => 'Jujuy',
      'CTES' => 'Corrientes',
      'CA' => 'Catamarca'
    }

    telefonos = {}
    CSV.foreach("#{Rails.root}/lib/tasks/telefonos.csv", {headers: true}) do |row|

      telefonos[row['id transportista']] = "#{row['DDn']} #{row['Tel']}"

    end

    categories = {}
    CSV.foreach("#{Rails.root}/lib/tasks/categorias.csv", {headers: true}) do |row|

      categories[row['ID Subcategoria']] = row['id_tb']

    end

    tranporter_categories = {}
    CSV.foreach("#{Rails.root}/lib/tasks/tranportortista_categorias.csv", {headers: true}) do |row|

      tranporter_categories[row['id transportista']] ||= []
      tranporter_categories[row['id transportista']] << categories[row['id subcategoria']]

    end

    counter = 0
  	CSV.foreach("#{Rails.root}/lib/tasks/transporters.csv", {headers: true}) do |row|
      user = User.where(email: row['mail']).first

      user.destroy if user

      names = row['Contacto'].split

      names.collect!{|word| word.capitalize }
      name = names[0..-2].join
      surname = names[-1]

      password = Devise.friendly_token[0,8]

      puts "Creando transportista: #{surname}..."

      # User
      user = User.new({
          email: row['mail'],
          name: name,
          surname: surname,
          username: "#{row['mail'].split("@").first}_#{row['id transportista']}",
          password: password,
          password_confirmation: password,
          receive_email_alerts: true
        })

      user.skip_confirmation!

      user.accept_use_conditions = true

      user.role = Transporter.new

      if match = row['Nombre comercial'].titlecase.match(/(S{1}.?A{1}|S{1}.?R{1}.?L{1})/)
        company_type = match[0].gsub('.', '')
      end

      company_type ||= "Resp.Inscripto"

      # Company
      company = Company.new({
          name: row['Nombre comercial'].titlecase,
          phone_number: telefonos[row['id transportista']],
          company_type: company_type
        })

      # Address
      company.build_address(country: "Argentina",
                            state: STATES[row['Provincia']],
                            city: row['Localidad'],
                            country: "Argentina",
                            street_line1: row['domilicio'],
                            zipcode: row['Cod Post'],
                            floor: row['Piso'],
                            apartment: row['Dto'])

      if tranporter_categories[row['id transportista']]
        #Coverage Area
        coverage_area = user.role.coverage_areas.build( description: 'Ambito inicial',
                                                        accepts_notifications_from_state_to_area: true,
                                                        accepts_notifications_from_area_to_state: true)
        coverage_area.category_ids = tranporter_categories[row['id transportista']]
        coverage_area.addresses.build(country: "Argentina", state: STATES[row['Provincia']])

        tranporter_categories[row['id transportista']].each do |category_id|
          if category_id
            parent = Category.find(category_id).parent
            unless company.categories.include? parent
              company.categories << parent
            end
          end
        end
      end

      user.role.company = company

      user.save

      counter += 1

      break if counter == 50
    end

  end

end
