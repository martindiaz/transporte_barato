namespace :data do

  desc "Insert KML data"
  task :kmls => :environment do
    %w(gba_zones ba_cities provinces).each do |zone|
      Kml.create(
        name: zone,
        body: File.read("#{Rails.root}/db/kmls/#{zone}.kml")
      )
    end
  end

end
