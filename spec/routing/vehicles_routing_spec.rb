require "spec_helper"

describe VehiclesController do
  describe "routing" do

    it "routes to #index" do
      get("transporters/1/vehicles").should route_to(
        controller: "vehicles",
        action: "index",
        transporter_id: "1")
    end

    it "routes to #new" do
      get("transporters/1/vehicles/new").should route_to(
        controller: "vehicles",
        action: "new",
        transporter_id: "1")
    end

    it "routes to #show" do
      get("transporters/1/vehicles/1").should route_to(
        controller: "vehicles",
        action: "show",
        transporter_id: "1",
        id: "1")
    end

    it "routes to #edit" do
      get("transporters/1/vehicles/1/edit").should route_to(
        controller: "vehicles",
        action: "edit",
        transporter_id: "1",
        id: "1")
    end

    it "routes to #create" do
      post("transporters/1/vehicles").should route_to(
        controller: "vehicles",
        action: "create",
        transporter_id: "1")
    end

    it "routes to #update" do
      put("transporters/1/vehicles/1").should route_to(
        controller: "vehicles",
        action: "update",
        transporter_id: "1",
        id: "1")
    end

    it "routes to #destroy" do
      delete("transporters/1/vehicles/1").should route_to(
        controller: "vehicles",
        action: "destroy",
        transporter_id: "1",
        id: "1")
    end
  end
end