require "spec_helper"

describe HomeBannersController do
  describe "routing" do

    it "routes to #index" do
      get("/home_banners").should route_to("home_banners#index")
    end

    it "routes to #new" do
      get("/home_banners/new").should route_to("home_banners#new")
    end

    it "routes to #show" do
      get("/home_banners/1").should route_to("home_banners#show", :id => "1")
    end

    it "routes to #edit" do
      get("/home_banners/1/edit").should route_to("home_banners#edit", :id => "1")
    end

    it "routes to #create" do
      post("/home_banners").should route_to("home_banners#create")
    end

    it "routes to #update" do
      put("/home_banners/1").should route_to("home_banners#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/home_banners/1").should route_to("home_banners#destroy", :id => "1")
    end

  end
end
