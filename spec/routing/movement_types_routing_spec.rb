require "spec_helper"

describe MovementTypesController do
  describe "routing" do

    it "routes to #index" do
      get("/movement_types").should route_to("movement_types#index")
    end

    it "routes to #new" do
      get("/movement_types/new").should route_to("movement_types#new")
    end

    it "routes to #show" do
      get("/movement_types/1").should route_to("movement_types#show", :id => "1")
    end

    it "routes to #edit" do
      get("/movement_types/1/edit").should route_to("movement_types#edit", :id => "1")
    end

    it "routes to #create" do
      post("/movement_types").should route_to("movement_types#create")
    end

    it "routes to #update" do
      put("/movement_types/1").should route_to("movement_types#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/movement_types/1").should route_to("movement_types#destroy", :id => "1")
    end

  end
end
