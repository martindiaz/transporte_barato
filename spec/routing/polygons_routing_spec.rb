require "spec_helper"

describe PolygonsController do
  describe "routing" do

    it "routes to #index" do
      get("/polygons").should route_to("polygons#index")
    end

    it "routes to #new" do
      get("/polygons/new").should route_to("polygons#new")
    end

    it "routes to #show" do
      get("/polygons/1").should route_to("polygons#show", :id => "1")
    end

    it "routes to #edit" do
      get("/polygons/1/edit").should route_to("polygons#edit", :id => "1")
    end

    it "routes to #create" do
      post("/polygons").should route_to("polygons#create")
    end

    it "routes to #update" do
      put("/polygons/1").should route_to("polygons#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/polygons/1").should route_to("polygons#destroy", :id => "1")
    end

  end
end
