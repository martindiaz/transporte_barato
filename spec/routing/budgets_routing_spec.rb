require "spec_helper"

describe BudgetsController do
  describe "routing" do

    it "routes to #index" do
      get("transporters/1/budgets").should route_to(
        controller: "budgets",
        action: "index",
        transporter_id: "1")
    end

    it "routes to #new" do
      get("transporters/1/budgets/new").should route_to(
        controller: "budgets",
        action: "new",
        transporter_id: "1")
    end

    it "routes to #show" do
      get("transporters/1/budgets/1").should route_to(
        controller: "budgets",
        action: "show",
        transporter_id: "1",
        id: "1")
    end

    it "routes to #edit" do
      get("transporters/1/budgets/1/edit").should route_to(
        controller: "budgets",
        action: "edit",
        transporter_id: "1",
        id: "1")
    end

    it "routes to #create" do
      post("transporters/1/budgets").should route_to(
        controller: "budgets",
        action: "create",
        transporter_id: "1")
    end

    it "routes to #update" do
      put("transporters/1/budgets/1").should route_to(
        controller: "budgets",
        action: "update",
        transporter_id: "1",
        id: "1")
    end

    it "routes to #destroy" do
      delete("transporters/1/budgets/1").should route_to(
        controller: "budgets",
        action: "destroy",
        transporter_id: "1",
        id: "1")
    end
  end
end