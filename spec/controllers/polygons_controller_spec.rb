require 'spec_helper'

describe PolygonsController do

  # This should return the minimal set of attributes required to create a valid
  # Polygon. As you add validations to Polygon, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { 
      "description" => "MyString",
      "points"      => [
        {
          "latitude"  => 1.1,
          "longitude" => 2.2
        }
      ]
    }
  end
  
  before(:each) do
    @polygon = FactoryGirl.create(:polygon_with_points)
  end
  
  after(:each) do
    @polygon.destroy if @polygon
  end


  describe "GET index" do
    it "exposes all polygons" do
      get :index
      controller.polygons.should eq([@polygon])
    end
  end

  describe "GET show" do
    it "exposes the requested polygon" do
      get :show, {:id => @polygon.to_param}
      controller.polygon.should eq(@polygon)
    end
  end

  describe "GET new" do
    it "exposes a new polygon" do
      get :new
      controller.polygon.should be_a_new(Polygon)
    end
  end

  describe "GET edit" do
    it "exposes the requested polygon" do
      get :edit, {:id => @polygon.to_param}
      controller.polygon.should eq(@polygon)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Polygon" do
        expect {
          post :create, {:polygon => valid_attributes}
        }.to change(Polygon, :count).by(1)
      end

      it "exposes a newly created polygon" do
        post :create, {:polygon => valid_attributes}
        controller.polygon.should be_a(Polygon)
      end

      it "redirects to the created polygon" do
        post :create, {:polygon => valid_attributes}
        response.should redirect_to(Polygon.last)
      end
    end

    describe "with invalid params" do
      it "exposes a newly created but unsaved polygon" do
        # Trigger the behavior that occurs when invalid params are submitted
        Polygon.any_instance.stub(:save).and_return(false)
        post :create, {:polygon => { "description" => "invalid value" }}
        controller.polygon.should be_a_new(Polygon)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Polygon.any_instance.stub(:save).and_return(false)
        post :create, {:polygon => { "description" => "invalid value" }}
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested polygon" do
        # Assuming there are no other polygons in the database, this
        # specifies that the Polygon created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Polygon.any_instance.should_receive(:update_attributes).with({ "description" => "MyString" })
        put :update, {:id => @polygon.to_param, :polygon => { "description" => "MyString" }}
      end

      it "exposes the requested polygon" do
        put :update, {:id => @polygon.to_param, :polygon => valid_attributes}
        controller.polygon.should eq(@polygon)
      end

      it "redirects to the polygon" do
        put :update, {:id => @polygon.to_param, :polygon => valid_attributes}
        response.should redirect_to(@polygon)
      end
    end

    describe "with invalid params" do
      it "expect the polygon" do
        Polygon.any_instance.stub(:save).and_return(false)
        put :update, {:id => @polygon.to_param, :polygon => { "description" => "invalid value" }}
        controller.polygon.should eq(@polygon)
      end

      it "re-renders the 'edit' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Polygon.any_instance.stub(:save).and_return(false)
        put :update, {:id => @polygon.to_param, :polygon => { "description" => "invalid value" }}
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested polygon" do
      expect {
        delete :destroy, {:id => @polygon.to_param}
      }.to change(Polygon, :count).by(-1)
    end

    it "redirects to the polygons list" do
      delete :destroy, {:id => @polygon.to_param}
      response.should redirect_to(polygons_url)
    end
  end

end
