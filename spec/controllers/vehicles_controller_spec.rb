require 'spec_helper'                              

describe VehiclesController do

  # This should return the minimal set of attributes required to create a valid
  # Vehicle. As you add validations to Vehicle, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "description" => "MyString" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # VehiclesController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  before(:each) do
    @vehicle = FactoryGirl.create(:vehicle_with_transporter)
    sign_in @vehicle.transporter.user
  end
  
  after(:each) do
    @vehicle.destroy if @vehicle
  end

  describe "GET index" do
    it "exposes all vehicles" do
      get :index, transporter_id: @vehicle.transporter
      controller.vehicles.should eq([@vehicle])
    end
  end

  describe "GET show" do
    it "exposes the requested vehicle" do
      get :show, { id: @vehicle.to_param, transporter_id: @vehicle.transporter }
      controller.vehicle.should eq(@vehicle)
    end
  end

  describe "GET new" do
    it "exposes a new vehicle" do
      get :new, transporter_id: @vehicle.transporter
      controller.vehicle.should be_a_new(Vehicle)
    end
  end

  describe "GET edit" do
    it "exposes the requested vehicle" do
      get :edit, { id: @vehicle.to_param, transporter_id: @vehicle.transporter }
      controller.vehicle.should eq(@vehicle)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Vehicle" do
        expect {
          post :create, { vehicle: valid_attributes, transporter_id: @vehicle.transporter }
        }.to change(Vehicle, :count).by(1)
      end

      it "exposes a newly created vehicle" do
        post :create, { vehicle: valid_attributes, transporter_id: @vehicle.transporter }
        controller.vehicle.should be_a(Vehicle)
      end

      it "redirects to the created vehicle" do
        post :create, { vehicle: valid_attributes, transporter_id: @vehicle.transporter }
        response.should redirect_to(@vehicle.transporter)
      end
    end

    describe "with invalid params" do
      it "exposes a newly created but unsaved vehicle" do
        # Trigger the behavior that occurs when invalid params are submitted
        Vehicle.any_instance.stub(:save).and_return(false)
        post :create, {
          vehicle: { "description" => "invalid value" },
          transporter_id: @vehicle.transporter
        }
        controller.vehicle.should be_a_new(Vehicle)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Vehicle.any_instance.stub(:save).and_return(false)
        post :create, {
          vehicle: { "description" => "invalid value" },
          transporter_id: @vehicle.transporter
        }
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested vehicle" do
        Vehicle.any_instance.should_receive(:update_attributes).with({ "description" => "MyString" })
        put :update, {
          id: @vehicle.to_param,
          vehicle: { "description" => "MyString" },
          transporter_id: @vehicle.transporter
        }
      end

      it "exposes the requested vehicle" do
        put :update, { 
          id: @vehicle.to_param,
          vehicle: valid_attributes,
          transporter_id: @vehicle.transporter
        }
        controller.vehicle.should eq(@vehicle)
      end

      it "redirects to the vehicle" do
        put :update, {
          id: @vehicle.to_param,
          vehicle: valid_attributes,
          transporter_id: @vehicle.transporter
        }
        response.should redirect_to(@vehicle.transporter)
      end
    end

    describe "with invalid params" do
      it "exposes the vehicle" do
        # Trigger the behavior that occurs when invalid params are submitted
        Vehicle.any_instance.stub(:save).and_return(false)
        put :update, {
          id: @vehicle.to_param,
          vehicle: { "description" => "invalid value" },
          transporter_id: @vehicle.transporter
        }
        controller.vehicle.should eq(@vehicle)
      end

      it "re-renders the 'edit' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Vehicle.any_instance.stub(:save).and_return(false)
        put :update, {
          id: @vehicle.to_param,
          transporter_id: @vehicle.transporter,
          vehicle: { "description" => "invalid value" }
        }
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested vehicle" do
      expect {
        delete :destroy, {
          id: @vehicle.to_param,
          transporter_id: @vehicle.transporter
        }
      }.to change(Vehicle, :count).by(-1)
    end

    it "redirects to the vehicles owner" do
      delete :destroy, {
        id: @vehicle.to_param,
        transporter_id: @vehicle.transporter
      }
      response.should redirect_to(@vehicle.transporter)
    end
  end

end
