require 'spec_helper'

describe LandingsController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'places'" do
    it "returns http success" do
      get 'places'
      response.should be_success
    end
  end

  describe "GET 'dates'" do
    it "returns http success" do
      get 'dates'
      response.should be_success
    end
  end

  describe "GET 'thanks'" do
    it "returns http success" do
      get 'thanks'
      response.should be_success
    end
  end

end
