require 'spec_helper'

describe DemandsController do

  # This should return the minimal set of attributes required to create a valid
  # Demand. As you add validations to Demand, be sure to
  # update the return value of this method accordingly.
  def valid_attributes

      attributes = FactoryGirl.attributes_for(:demand)
      attributes.merge!({:category_id => FactoryGirl.create(:category).id})

  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # DemandsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  before(:each) do
    @demand = FactoryGirl.create(:demand)
    sign_in @demand.client.user
  end

  after(:each) do
    @demand.destroy if @demand
  end

  describe "GET index" do
    it "exposes all demands" do
      get :index
      controller.demands.should eq([@demand])
    end
  end

  describe "GET show" do
    it "exposes the requested demand" do
      get :show, {:id => @demand.to_param}
      controller.demand.should eq(@demand)
    end
  end

  describe "GET new" do
    it "exposes a new demand" do
      get :new
      controller.demand.should be_a_new(Demand)
    end
  end

  describe "GET edit" do
    it "exposes the requested demand" do
      get :edit, {:id => @demand.to_param}
      controller.demand.should eq(@demand)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Demand" do
        expect {
          post :create, {:demand => valid_attributes}
        }.to change(Demand, :count).by(1)
      end

      it "exposes a newly created demand" do
        post :create, {:demand => valid_attributes}
        controller.demand.should be_a(Demand)
      end

      it "redirects to the created demand" do
        post :create, {:demand => valid_attributes}
        response.should redirect_to(Demand.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved demand as @demand" do
        # Trigger the behavior that occurs when invalid params are submitted
        Demand.any_instance.stub(:save).and_return(false)
        post :create, {:demand => { "delivery_date" => "invalid value" }}
        controller.demand.should be_a_new(Demand)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Demand.any_instance.stub(:save).and_return(false)
        post :create, {:demand => { "delivery_date" => "invalid value" }}
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested demand" do
        Demand.any_instance.should_receive(:update_attributes).with({
          "delivery_date" => "2012-12-22 09:38:36",
          "category"      => nil
        })
        put :update, {:id => @demand.to_param, :demand => {
          "delivery_date" => "2012-12-22 09:38:36",
          "category"      => nil
          }
        }
      end

      it "assigns the requested demand as @demand" do
        put :update, {:id => @demand.to_param, :demand => valid_attributes}
        controller.demand.should eq(@demand)
      end

      it "redirects to the demand" do
        put :update, {:id => @demand.to_param, :demand => valid_attributes}
        response.should redirect_to(@demand)
      end
    end

    describe "with invalid params" do
      it "assigns the demand as @demand" do
        # Trigger the behavior that occurs when invalid params are submitted
        Demand.any_instance.stub(:save).and_return(false)
        put :update, {:id => @demand.to_param, :demand => {
          "delivery_date" => "invalid value",
          "category"      => "test"
          }
        }
        controller.demand.should eq(@demand)
      end

      it "re-renders the 'edit' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Demand.any_instance.stub(:save).and_return(false)
        put :update, {:id => @demand.to_param, :demand => {
          "delivery_date" => "invalid value",
          "category"      => "test"
          }
        }
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested demand" do
      expect {
        delete :destroy, {:id => @demand.to_param}
      }.to change(Demand, :count).by(-1)
    end

    it "redirects to the demands list" do
      delete :destroy, {:id => @demand.to_param}
      response.should redirect_to(demands_url)
    end
  end

end
