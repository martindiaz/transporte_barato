require 'spec_helper'

describe TransportersController do

  # This should return the minimal set of attributes required to create a valid
  # Transporter. As you add validations to Transporter, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { 
      "company_name"         => "MyString",
      "city"                 => "Buenos Aires",
      "address"              => "Address",
      "receive_email_alerts" => true
    }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # TransportersController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  before(:each) do
    @transporter = FactoryGirl.create(:transporter)
  end

  after(:each) do
    @transporter.destroy if @transporter
  end

  describe "GET index" do
    it "exposes all transporters" do
      get :index, {}, valid_session
      controller.transporters.should eq([@transporter])
    end
  end

  describe "GET show" do
    it "exposes the requested transporter" do
      get :show, {:id => @transporter.to_param}, valid_session
      controller.transporter.should eq(@transporter)
    end
  end

  describe "GET new" do
    it "exposes a new transporter" do
      get :new, {}, valid_session
      controller.transporter.should be_a_new(Transporter)
    end
  end

  describe "GET edit" do
    it "exposes the requested transporter" do
      get :edit, {:id => @transporter.to_param}, valid_session
      controller.transporter.should eq(@transporter)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Transporter" do
        expect {
          post :create, {
            transporter: valid_attributes,
            user: valid_user_attributes
          }, valid_session
        }.to change(Transporter, :count).by(1)
      end

      it "exposes a newly created transporter" do
        post :create, {
          transporter: valid_attributes,
          user: valid_user_attributes
        }, valid_session
        controller.transporter.should be_a(Transporter)
        controller.transporter.should be_persisted
      end

      it "redirects to the created transporter" do
        post :create, {
          transporter: valid_attributes,
          user: valid_user_attributes
        }, valid_session
        response.should redirect_to(Transporter.last)
      end
    end

    describe "with invalid params" do
      it "exposes a newly created but unsaved transporter" do
        # Trigger the behavior that occurs when invalid params are submitted
        Transporter.any_instance.stub(:save).and_return(false)
        post :create, {
          transporter: { "company_name" => nil }
        }, valid_session
        controller.transporter.should be_a_new(Transporter)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Transporter.any_instance.stub(:save).and_return(false)
        post :create, {
          transporter: { "company_name" => nil }
        }, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested transporter" do
        Transporter.any_instance.should_receive(:update_attributes).with(valid_attributes)
        put :update, {
          id: @transporter.to_param, 
          transporter: valid_attributes,
          user: valid_user_attributes
        }, valid_session
      end

      it "exposes the requested transporter" do
        put :update, {
          id: @transporter.to_param,
          transporter: valid_attributes,
          user: valid_user_attributes
        }, valid_session
        controller.transporter.should eq(@transporter)
      end

      it "redirects to the transporter" do
        put :update, {
          id: @transporter.to_param,
          transporter: valid_attributes,
          user: valid_user_attributes
        }, valid_session
        response.should redirect_to(@transporter)
      end
    end

    describe "with invalid params" do
      it "exposes the transporter" do
        # Trigger the behavior that occurs when invalid params are submitted
        Transporter.any_instance.stub(:save).and_return(false)
        put :update, {:id => @transporter.to_param, :transporter => { "company_name" => nil }}, valid_session
        controller.transporter.should eq(@transporter)
      end

      it "re-renders the 'edit' template" do
        transporter = Transporter.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Transporter.any_instance.stub(:save).and_return(false)
        put :update, {:id => @transporter.to_param, :transporter => { "company_name" => nil }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested transporter" do
      expect {
        delete :destroy, {:id => @transporter.to_param}, valid_session
      }.to change(Transporter, :count).by(-1)
    end

    it "redirects to the transporters list" do
      delete :destroy, {:id => @transporter.to_param}, valid_session
      response.should redirect_to(transporters_url)
    end
  end

end
