require 'spec_helper'

describe BudgetsController do

  # This should return the minimal set of attributes required to create a valid
  # Budget. As you add validations to Budget, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "price" => "MyString" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # BudgetsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  before(:each) do
    @budget = FactoryGirl.create(:budget_with_transporter_and_demand)
    sign_in @budget.transporter.user
  end
  
  after(:each) do
    @budget.destroy if @budget
  end


  describe "GET index" do
    it "exposes all budgets as budgets" do
      get :index, transporter_id: @budget.transporter
      controller.budgets.should eq([@budget])
    end
  end

  describe "GET show" do
    it "exposes the requested budget" do
      get :show, { id: @budget.to_param, transporter_id: @budget.transporter }
      controller.budget.should eq(@budget)
    end
  end

  describe "GET new" do
    it "exposes a new budget" do
      get :new, demand_id: @budget.demand
      controller.budget.should be_a_new(Budget)
    end
  end

  describe "GET edit" do
    it "assigns the requested budget as @budget" do
      get :edit, { id: @budget.to_param, transporter_id: @budget.transporter }
      controller.budget.should eq(@budget)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Budget" do
        expect {
          post :create, {
            budget: valid_attributes,
            transporter_id: @budget.transporter,
            demand_id: @budget.demand
          }
        }.to change(Budget, :count).by(1)
      end

      it "exposes a newly created budget" do
        post :create, {
          budget: valid_attributes,
          transporter_id: @budget.transporter,
          demand_id: @budget.demand
        }
        controller.budget.should be_a(Budget)
      end

      it "redirects to the transporter index" do
        post :create, {
          budget: valid_attributes,
          transporter_id: @budget.transporter,
          demand_id: @budget.demand
        }
        response.should redirect_to(@budget.transporter)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved budget as @budget" do
        # Trigger the behavior that occurs when invalid params are submitted
        Budget.any_instance.stub(:save).and_return(false)
        post :create, { 
          budget: { "price" => "invalid value" },
          transporter_id: @budget.transporter,
          demand_id: @budget.demand
        }
        controller.budget.should be_a_new(Budget)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Budget.any_instance.stub(:save).and_return(false)
        post :create, {
          budget: { "price" => "invalid value" },
          transporter_id: @budget.transporter,
          demand_id: @budget.demand
        }
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested budget" do
        Budget.any_instance.should_receive(:update_attributes).with({ "price" => "MyString" })
        put :update, {
          id: @budget.to_param,
          budget: { "price" => "MyString" },
          transporter_id: @budget.transporter
        }
      end

      it "exposes the requested budget" do
        put :update, {
          id: @budget.to_param,
          budget: valid_attributes,
          transporter_id: @budget.transporter
        }
        controller.budget.should eq(@budget)
      end

      it "redirects to the transporter" do
        put :update, {
          id: @budget.to_param,
          budget: valid_attributes,
          transporter_id: @budget.transporter
        }
        response.should redirect_to(@budget.transporter)
      end
    end

    describe "with invalid params" do
      it "exposes the budget" do
        Budget.any_instance.stub(:save).and_return(false)
        put :update, {
          id: @budget.to_param,
          budget: { "price" => "invalid value" },
          transporter_id: @budget.transporter
        }
        controller.budget.should eq(@budget)
      end

      it "re-renders the 'edit' template" do
        Budget.any_instance.stub(:save).and_return(false)
        put :update, {
          id: @budget.to_param,
          budget: { "price" => "invalid value" },
          transporter_id: @budget.transporter
        }
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested budget" do
      expect {
        delete :destroy, {
          id: @budget.to_param,
          transporter_id: @budget.transporter
        }
      }.to change(Budget, :count).by(-1)
    end

    it "redirects to the transporter budget list" do
      delete :destroy, {
        id: @budget.to_param,
        transporter_id: @budget.transporter
      }
      response.should redirect_to(@budget.transporter)
    end
  end

end
