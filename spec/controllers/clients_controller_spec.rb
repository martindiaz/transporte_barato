require 'spec_helper'

describe ClientsController do

  # This should return the minimal set of attributes required to create a valid
  # Client. As you add validations to Client, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    {}
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # ClientsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  before(:each) do
    @valid_client = FactoryGirl.create(:client)
  end

  after(:each) do
    @valid_client.destroy if @valid_client
  end

  describe "GET index" do
    it "exposes clients" do
      get :index, {}, valid_session
      controller.clients.should eq([@valid_client])
    end
  end

  describe "GET show" do
    it "exposes a given client" do
      get :show, {:id => @valid_client.to_param}, valid_session
      controller.client.should eq(@valid_client)
    end
  end

  describe "GET new" do
    it "exposes a new client" do
      get :new, {}, valid_session
      controller.client.should be_a_new(Client)
    end
  end

  describe "GET edit" do
    it "exposes the requested client" do
      get :edit, {:id => @valid_client.to_param}, valid_session
      controller.client.should eq(@valid_client)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Client" do
        expect {
          post :create, {:client => valid_attributes, :user => valid_user_attributes}, valid_session
        }.to change(Client, :count).by(1)
      end

      it "exposes a newly created client" do
        post :create, {:client => valid_attributes, :user => valid_user_attributes}, valid_session
        controller.client.should be_a(Client)
        controller.client.should be_persisted
      end

      it "redirects to the created client" do
        post :create, {:client => valid_attributes, :user => valid_user_attributes}, valid_session
        response.should redirect_to(Client.last)
      end
    end

    describe "with invalid params" do
      it "exposes a newly created but unsaved client" do
        # Trigger the behavior that occurs when invalid params are submitted
        post :create, {}, valid_session
        controller.client.should be_a_new(Client)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        post :create, {}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested client" do
        Client.create! valid_attributes
        # Assuming there are no other clients in the database, this
        # specifies that the Client created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Client.any_instance.should_receive(:update_attributes).with(valid_attributes)
        put :update, {:id => @valid_client.to_param, :client => valid_attributes, :user => valid_user_attributes}, valid_session
      end

      it "exposes the requested client" do
        put :update, {:id => @valid_client.to_param, :client => valid_attributes, :user => valid_user_attributes}, valid_session
        controller.client.should eq(@valid_client)
      end

      it "redirects to the client" do
        put :update, {:id => @valid_client.to_param, :client => valid_attributes, :user => valid_user_attributes}, valid_session
        response.should redirect_to(@valid_client)
      end
    end

    describe "with invalid params" do
      it "exposes the client" do
        # Trigger the behavior that occurs when invalid params are submitted
        Client.any_instance.stub(:save).and_return(false)
        put :update, {:id => @valid_client.to_param, :client => {  }}, valid_session
        controller.client.should eq(@valid_client)
      end

      it "re-renders the 'edit' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Client.any_instance.stub(:save).and_return(false)
        put :update, {:id => @valid_client.to_param, :client => {  }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested client" do
      expect {
        delete :destroy, {:id => @valid_client.to_param}, valid_session
      }.to change(Client, :count).by(-1)
    end

    it "redirects to the clients list" do
      delete :destroy, {:id => @valid_client.to_param}, valid_session
      response.should redirect_to(clients_url)
    end
  end

end
