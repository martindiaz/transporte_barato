require 'spec_helper'

describe Transporter do

  it "has a valid factory" do
    FactoryGirl.create(:transporter).should be_valid
  end

  it "has a complete profile" do
    FactoryGirl.create(:transporter).has_a_complete_profile?.should eq true
  end

end
