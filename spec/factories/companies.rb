# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company do
    name "MyString"
    year_of_creation 1
    transport_license_number "MyString"
    transporter_id 1
    number_of_drivers 1
    number_of_employees 1
    company_type "MyString"
  end
end
