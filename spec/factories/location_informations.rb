# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location_information do
    has_place_to_park false
    delivery_date_from "2013-02-13"
    delivery_date_to "2013-02-13"
  end
end
