# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :billing_address do
    first_name "MyString"
    last_name "MyString"
    street_line "MyString"
    city "MyString"
    zip "MyString"
    state "MyString"
    country "MyString"
    user_id 1
  end
end
