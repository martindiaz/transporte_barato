FactoryGirl.define do
  sequence(:random_email) {|n| "test#{n}@email.com" }

  factory :user do
    email                 { generate(:random_email) }
    password              "password"
    password_confirmation "password"
    remember_me            true
  end

  factory :client_user, class: User do
    email                 { generate(:random_email) }
    password              "password"
    password_confirmation "password"
    remember_me            true
    association :role, factory: :client
  end
end
