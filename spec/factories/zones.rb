# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :zone do
    name "MyString"
    administrative_areas "MyString"
    cities "MyString"
  end
end
