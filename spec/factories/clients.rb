FactoryGirl.define do
  factory :client do
    user

    factory :user_with_demands do
      demands { FactoryGirl.create_list(:demand, 2) }
    end
  end
end
