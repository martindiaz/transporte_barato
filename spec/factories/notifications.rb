# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :notification do
    description "MyString"
    status "MyString"
    user_id 1
  end
end
