# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :demand do
    delivery_date "2012-12-22 09:38:36"
    origin "MyString"
    destiny "MyString"
    description "MyText"
    category
    client
  end
end
