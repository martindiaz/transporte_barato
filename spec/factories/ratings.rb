# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :rating do
    client_id 1
    transporter_id 1
    points 1
    comment "MyText"
  end
end
