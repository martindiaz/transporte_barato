# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :credit_history do
    company_id 1
    movement_type_id 1
    amount 1
    budget_id 1
  end
end
