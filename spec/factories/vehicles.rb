# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :vehicle do
    description "Traffic"

    factory :vehicle_with_transporter do
      association :transporter
    end
  end
end
