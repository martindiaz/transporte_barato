# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :budget do
    message "budget message"
    price   "the price"

    factory :budget_with_transporter_and_demand do
      association :transporter
      association :demand
    end
  end
end
