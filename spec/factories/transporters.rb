FactoryGirl.define do
  factory :transporter do
    company_name        "TransporteBarato"
    city                "Springfield"
    address             "742 Evergreen Terrace"
    receive_email_alerts true
    user

    factory :transporter_with_vehicles do
      vehicles { FactoryGirl.create_list(:vehicle, 2) }
    end
  end
end
