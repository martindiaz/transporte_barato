# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :polygon do
    description "MyString"
    
    factory :polygon_with_points do
      points { FactoryGirl.create_list(:point, 2) }
    end
  end
end
